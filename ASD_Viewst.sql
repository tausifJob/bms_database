USE [BMS_ASD]
GO
/****** Object:  View [sims].[sims_student_parent_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--select * FROM         [sims].[sims_student_parent_list]  where sims_student_enroll_number='1410'
--select * FROM         [sims].[sims_student]  where sims_student_enroll_number='1410'
--select * from sims.sims_sibling where sims_sibling_student_enroll_number='1410'
--select * from sims.sims_parent where sims_parent_number='P02707'

CREATE VIEW [sims].[sims_student_parent_list]
AS
SELECT        st.sims_student_enroll_number, 
                         CAST(st.sims_student_passport_first_name_en + ' ' + st.sims_student_passport_middle_name_en + ' ' + st.sims_student_passport_last_name_en
						+ ' ' + st.sims_student_family_name_en
						  AS nvarchar(120))
                          AS stu_name, p.sims_sibling_parent_number,
                             (SELECT       iif(st.sims_student_primary_contact_pref='F',(CAST(isnull(sims_parent_father_first_name,'') + ' ' + isnull(sims_parent_father_middle_name,'') + ' ' + isnull(sims_parent_father_last_name,'') AS nvarchar(45))),
											(CAST(isnull(sims_parent_mother_first_name,'') + ' ' + isnull(sims_parent_mother_middle_name,'') + ' ' + isnull(sims_parent_mother_last_name,'') AS nvarchar(45))))
                                                         AS Expr1
                               FROM            sims.sims_parent
                               WHERE        (sims_parent_number = p.sims_sibling_parent_number)) AS parent_name, ss.sims_academic_year, ss.sims_cur_code, 
                         ss.sims_allocation_status, ss.sims_grade_code, ss.sims_section_code,
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade AS gr
                               WHERE        (sims_grade_code = ss.sims_grade_code) AND (sims_cur_code = ss.sims_cur_code) AND (sims_academic_year = ss.sims_academic_year)) 
                         AS grade_name,
                             (SELECT        sims_section_name_en
                               FROM            sims.sims_section AS se
                               WHERE        (sims_section_code = ss.sims_section_code) AND (sims_cur_code = ss.sims_cur_code) AND 
                                                         (sims_academic_year = ss.sims_academic_year)) AS sec_name,
                             (SELECT        iif(st.sims_student_primary_contact_pref='F',sims_parent_father_mobile,sims_parent_mother_mobile) AS Expr1
                               FROM            sims.sims_parent AS sims_parent_1
                               WHERE        (sims_parent_number = p.sims_sibling_parent_number)) AS parent_cont
FROM            sims.sims_student AS st LEFT OUTER JOIN
                         sims.sims_sibling AS p ON st.sims_student_enroll_number = p.sims_sibling_student_enroll_number LEFT OUTER JOIN
                         sims.sims_student_section AS ss ON ss.sims_enroll_number = st.sims_student_enroll_number
			








GO
/****** Object:  View [sims].[student_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [sims].[student_list]
AS
SELECT        sims.sims_student.sims_student_enroll_number, sims.sims_student_section.sims_academic_year, sims.sims_student_section.sims_grade_code,
 --sims.sims_student.sims_student_date,
 SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_date, 
                         --sims.sims_student.sims_student_commence_date,
SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_commence_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_commence_date, 
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade
                               WHERE        (sims_grade_code = sims.sims_student_section.sims_grade_code) AND (sims_academic_year = sims.sims_student_section.sims_academic_year) AND (sims_cur_code=sims.sims_student_section.sims_cur_code)) AS grade_name, 
                         sims.sims_student_section.sims_section_code,
                             (SELECT        sims_section_name_en
                               FROM            sims.sims_section
                               WHERE        (sims_section_code = sims.sims_student_section.sims_section_code) AND (sims_academic_year = sims.sims_student_section.sims_academic_year) AND (sims_grade_code=sims.sims_student_section.sims_grade_code) AND (sims_cur_code=sims.sims_student_section.sims_cur_code)) AS section_name, 
                         sims.sims_student.sims_student_cur_code,
                             (SELECT        sims_cur_short_name_en
                               FROM            sims.sims_cur
                               WHERE        (sims_cur_code=sims.sims_student_section.sims_cur_code)) AS cur_shrt_name, 
                         isnull(sims.sims_student.sims_student_passport_first_name_en,'') + ' ' + isnull(sims.sims_student.sims_student_passport_middle_name_en,'') + ' ' + isnull(sims.sims_student.sims_student_passport_last_name_en,'') AS student_name,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_appl_code = 'Sim010') AND (sims_appl_form_field = 'Gender') AND (sims_appl_parameter = sims.sims_student.sims_student_gender)) AS gender, sims.sims_student.sims_student_gender, 
                         sims.sims_student.sims_student_religion_code,
                             (SELECT        sims_religion_name_en
                               FROM            sims.sims_religion
                               WHERE        (sims_religion_code = sims.sims_student.sims_student_religion_code)) AS std_religion, sims.sims_student.sims_student_dob, sims.sims_student.sims_student_birth_country_code,
                             (SELECT        sims_country_name_en
                               FROM            sims.sims_country
                               WHERE        (sims_country_code = sims.sims_student.sims_student_birth_country_code)) AS country_name, sims.sims_student.sims_student_nationality_code,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality
                               WHERE        (sims_nationality_code = sims.sims_student.sims_student_nationality_code)) AS nationality_name, sims.sims_student.sims_student_img, sims.sims_student.sims_student_academic_status, 
                         sims.sims_student.sims_student_remark, sims.sims_student.sims_student_family_name_en, sims.sims_student.sims_student_admission_grade_code,
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade AS sims_grade_1
                               WHERE        (sims_cur_code=sims.sims_student_section.sims_cur_code) AND (sims_academic_year = sims.sims_student_section.sims_academic_year) AND 
                                                         (sims_grade_code = sims.sims_student.sims_student_admission_grade_code)) AS sims_student_admission_grade_name,
														  sims.sims_student.sims_student_admission_academic_year, 
                         sims.sims_student.sims_student_prev_school, sims.sims_student.sims_student_visa_number,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_3
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Visa Type') AND (sims_appl_parameter = sims.sims_student.sims_student_visa_type)) 
                         AS sims_student_visa_type, sims.sims_student.sims_student_visa_issuing_place, sims.sims_student.sims_student_visa_issuing_authority, 
						 --sims.sims_student.sims_student_visa_issue_date, 
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_visa_issue_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_visa_issue_date, 
                         --sims.sims_student.sims_student_visa_expiry_date,
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_visa_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_visa_expiry_date, 
						 sims.sims_student.sims_student_national_id,
						 --sims.sims_student.sims_student_national_id_issue_date, 
                         SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_national_id_issue_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_national_id_issue_date, 
						 --sims.sims_student.sims_student_national_id_expiry_date,
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_national_id_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_national_id_expiry_date, 
						 sims.sims_student.sims_student_ea_number,
						 --sims.sims_student.sims_student_ea_registration_date,
                         SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_ea_registration_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_ea_registration_date, 
						     (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_2
                               WHERE        (sims_mod_code = '005') AND (sims_appl_code = 'Sim042') AND (sims_appl_form_field = 'Register Status') AND (sims_appl_parameter = sims.sims_student.sims_student_ea_status)) 
                         AS sims_student_ea_status,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_1
                               WHERE        (sims_mod_code = '005') AND (sims_appl_code = 'Sim042') AND (sims_appl_form_field = 'Transfer Status') AND (sims_appl_parameter = sims.sims_student.sims_student_ea_transfer)) 
                         AS sims_student_ea_transfer_status, sims.sims_student.sims_student_passport_number, sims.sims_student.sims_student_passport_issue_place, 
                         sims.sims_student.sims_student_passport_issuing_authority,
						 --sims.sims_student.sims_student_passport_issue_date,
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_passport_issue_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_passport_issue_date, 
						 --sims.sims_student.sims_student_passport_expiry_date, 
                         SWITCHOFFSET(TodateTimeoffset(sims.sims_student.sims_student_passport_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_student_passport_expiry_date, 

						 CASE WHEN sims.sims_student.sims_student_dob = NULL THEN 'Date of birth not found' ELSE dbo.fnCalculateAge(sims.sims_student.sims_student_dob, SWITCHOFFSET(TodateTimeoffset(GETDATE(),DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details))) END AS sims_student_age, 
                         sims.sims_student_parent_list.sims_sibling_parent_number, 
                         COALESCE (isnull(sims.sims_parent.sims_parent_father_first_name,' ') + ' ' + isnull(sims.sims_parent.sims_parent_father_middle_name,'') + ' ' + isnull(sims.sims_parent.sims_parent_father_last_name,''), N'') AS father_name, 
                         sims.sims_parent.sims_parent_father_family_name, sims.sims_parent.sims_parent_father_name_ot,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality AS sims_nationality_6
                               WHERE        (sims_nationality_code = sims.sims_parent.sims_parent_father_nationality1_code)) AS father_nationality1,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality AS sims_nationality_5
                               WHERE        (sims_nationality_code = sims.sims_parent.sims_parent_father_nationality2_code)) AS father_nationality2, sims.sims_parent.sims_parent_father_summary_address, 
                         sims.sims_parent.sims_parent_father_summary_address_local_language, sims.sims_parent.sims_parent_father_country_code, sims.sims_parent.sims_parent_father_phone, 
                         sims.sims_parent.sims_parent_father_mobile, sims.sims_parent.sims_parent_father_email, sims.sims_parent.sims_parent_father_fax, sims.sims_parent.sims_parent_father_po_box, 
                         sims.sims_parent.sims_parent_father_occupation, sims.sims_parent.sims_parent_father_occupation_local_language, sims.sims_parent.sims_parent_father_occupation_location_local_language, 
                         sims.sims_parent.sims_parent_father_company, sims.sims_parent.sims_parent_father_passport_number, 
                         COALESCE (isnull(sims.sims_parent.sims_parent_mother_first_name,' ') + ' ' + isnull(sims.sims_parent.sims_parent_mother_middle_name,' ') + ' ' + isnull(sims.sims_parent.sims_parent_mother_last_name,''), N'') AS mother_name, 
                         sims.sims_parent.sims_parent_mother_family_name, sims.sims_parent.sims_parent_mother_name_ot,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality AS sims_nationality_4
                               WHERE        (sims_nationality_code = sims.sims_parent.sims_parent_mother_nationality1_code)) AS mother_nationality1,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality AS sims_nationality_3
                               WHERE        (sims_nationality_code = sims.sims_parent.sims_parent_mother_nationality2_code)) AS mother_nationality2, sims.sims_parent.sims_parent_mother_summary_address, 
                         sims.sims_parent.sims_parent_mother_country_code, sims.sims_parent.sims_parent_mother_phone, sims.sims_parent.sims_parent_mother_mobile, sims.sims_parent.sims_parent_mother_email, 
                         sims.sims_parent.sims_parent_mother_fax, sims.sims_parent.sims_parent_mother_po_box, sims.sims_parent.sims_parent_mother_occupation, 
                         sims.sims_parent.sims_parent_mother_occupation_local_language, sims.sims_parent.sims_parent_mother_company, sims.sims_parent.sims_parent_mother_passport_number, 
                         COALESCE (sims.sims_parent.sims_parent_guardian_first_name + ' ' + sims.sims_parent.sims_parent_guardian_middle_name + ' ' + sims.sims_parent.sims_parent_guardian_last_name, N'') 
                         AS guardian_name, sims.sims_parent.sims_parent_guardian_family_name, sims.sims_parent.sims_parent_guardian_name_ot,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality AS sims_nationality_2
                               WHERE        (sims_nationality_code = sims.sims_parent.sims_parent_guardian_nationality1_code)) AS guardian_nationality1,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality AS sims_nationality_1
                               WHERE        (sims_nationality_code = sims.sims_parent.sims_parent_guardian_nationality2_code)) AS guardian_nationality2, sims.sims_parent.sims_parent_guardian_summary_address, 
                         sims.sims_parent.sims_parent_guardian_summary_address_local_language, sims.sims_parent.sims_parent_guardian_country_code, sims.sims_parent.sims_parent_guardian_phone, 
                         sims.sims_parent.sims_parent_guardian_mobile, sims.sims_parent.sims_parent_guardian_email, sims.sims_parent.sims_parent_guardian_fax, sims.sims_parent.sims_parent_guardian_po_box, 
                         sims.sims_parent.sims_parent_guardian_occupation, sims.sims_parent.sims_parent_guardian_occupation_local_language, sims.sims_parent.sims_parent_guardian_occupation_location_local_language, 
                         sims.sims_parent.sims_parent_guardian_company, sims.sims_parent.sims_parent_guardian_relationship_code, sims.sims_parent.sims_parent_guardian_passport_number, 
                         sims.sims_parent.sims_parent_is_employment_number,
						  --sims.sims_parent.sims_parent_father_passport_expiry_date,
						  SWITCHOFFSET(TodateTimeoffset(sims.sims_parent.sims_parent_father_passport_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_parent_father_passport_expiry_date, 
						   sims.sims_parent.sims_parent_father_national_id, 
                         --sims.sims_parent.sims_parent_father_national_id__expiry_date,
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_parent.sims_parent_father_national_id__expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_parent_father_national_id__expiry_date, 
						  --sims.sims_parent.sims_parent_mother_passport_expiry_date,
						  SWITCHOFFSET(TodateTimeoffset(sims.sims_parent.sims_parent_mother_passport_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_parent_mother_passport_expiry_date, 
						   sims.sims_parent.sims_parent_mother_national_id, 
                         --sims.sims_parent.sims_parent_mother_national_id_expiry_date,
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_parent.sims_parent_mother_national_id_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_parent_mother_national_id_expiry_date, 
						  --sims.sims_parent.sims_parent_guardian_passport_expiry_date2,
						  SWITCHOFFSET(TodateTimeoffset(sims.sims_parent.sims_parent_guardian_passport_expiry_date2,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_parent_guardian_passport_expiry_date2, 
						   sims.sims_parent.sims_parent_guardian_national_id, 
                         --sims.sims_parent.sims_parent_guardian_national_id_expiry_date,
						 SWITCHOFFSET(TodateTimeoffset(sims.sims_parent.sims_parent_guardian_national_id_expiry_date,DATEPART(TZ,sysdatetimeoffset())),(select lic_time_zone from mogra.mogra_license_details)) as sims_parent_guardian_national_id_expiry_date, 
						 --sims.sims_student.sims_student_attribute1,
						 COALESCE(STUFF((select ', '+ x.sims_transport_vehicle_model_name
							from sims.sims_transport_route_student y inner join sims.sims_transport_route z on
								y.sims_transport_academic_year = z.sims_academic_year and
								y.sims_transport_route_code = z.sims_transport_route_code and
								y.sims_transport_route_direction = z.sims_transport_route_direction inner join sims.sims_transport_vehicle x on
								z.sims_transport_route_vehicle_code = x.sims_transport_vehicle_code
							where y.sims_transport_academic_year = sims.sims_student_section.sims_academic_year 
									and y.sims_transport_enroll_number = sims.sims_student_section.sims_enroll_number
									and sims_transport_route_student_status = 'A'
							FOR XML PATH('')),1,1,''),'Private') as sims_student_attribute1,
						 sims.sims_student.sims_student_attribute2,
						 '971507014692' AS parent_cont--select top 100 *
						 
						  
FROM                     sims.sims_student LEFT OUTER JOIN
                         sims.sims_student_section ON sims.sims_student_section.sims_enroll_number = sims.sims_student.sims_student_enroll_number and 
						 sims.sims_student_section.sims_cur_code = sims.sims_student.sims_student_cur_code
						 LEFT OUTER JOIN
                         sims.sims_student_parent_list ON 
						 sims.sims_student_parent_list.sims_cur_code = sims.sims_student_section.sims_cur_code and 
						 sims.sims_student_parent_list.sims_academic_year = sims.sims_student_section.sims_academic_year and 
						 sims.sims_student_parent_list.sims_grade_code = sims.sims_student_section.sims_grade_code and 
						 sims.sims_student_parent_list.sims_section_code = sims.sims_student_section.sims_section_code and 
						 sims.sims_student.sims_student_enroll_number = sims.sims_student_parent_list.sims_student_enroll_number 
						 LEFT OUTER JOIN
                         sims.sims_parent ON sims.sims_student_parent_list.sims_sibling_parent_number = sims.sims_parent.sims_parent_number
where					 sims.sims_student_section.sims_allocation_status in ('A','1')



GO
/****** Object:  View [sims].[sims_gradebook_category_assignment_student_marks_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [sims].[sims_gradebook_category_assignment_student_marks_list]
AS
SELECT        a.sims_cur_code,
			a.sims_academic_year,
			a.sims_grade_code,
			a.sims_section_code,
			a.sims_gb_number, a.sims_gb_cat_code, a.sims_gb_cat_assign_number, a.sims_gb_cat_assign_enroll_number, 
			CASE WHEN a.sims_gb_cat_assign_mark IS NULL THEN a.sims_gb_cat_assign_mark 
					WHEN a.sims_gb_cat_assign_mark = '' THEN NULL ELSE  
			convert(numeric(6,1), a.sims_gb_cat_assign_mark) END AS final_mark, a.sims_gb_cat_assign_final_grade, 
                         ISNULL(NULLIF (f.sims_mark_grade_name, N''), N'UM') AS final_grade, f.sims_mark_grade_color_code, 
						 a.sims_gb_cat_assign_status, a.sims_gb_cat_assign_comment, d.sims_gb_cat_assign_name, e.student_name,
						 d.sims_gb_cat_assign_max_score,
						 sims_gb_cat_assign_max_score_correct,
                             (SELECT        sims_gb_cat_name
                               FROM            sims.sims_gradebook_category
                               WHERE        (sims_cur_code = a.sims_cur_code) AND (sims_academic_year = a.sims_academic_year) AND (sims_grade_code = a.sims_grade_code) AND (sims_section_code = a.sims_section_code) AND 
                                                         (sims_gb_number = a.sims_gb_number) AND (sims_gb_cat_code = a.sims_gb_cat_code)) AS category_name,
							 (SELECT		sims_gb_cat_weightage_per
                               FROM            sims.sims_gradebook_category
                               WHERE        (sims_cur_code = a.sims_cur_code) AND (sims_academic_year = a.sims_academic_year) AND (sims_grade_code = a.sims_grade_code) AND (sims_section_code = a.sims_section_code) AND 
                                                         (sims_gb_number = a.sims_gb_number) AND (sims_gb_cat_code = a.sims_gb_cat_code)) AS category_weight,
								 (SELECT		sims_gb_cat_points_possible
                               FROM            sims.sims_gradebook_category
                               WHERE        (sims_cur_code = a.sims_cur_code) AND (sims_academic_year = a.sims_academic_year) AND (sims_grade_code = a.sims_grade_code) AND (sims_section_code = a.sims_section_code) AND 
                                                         (sims_gb_number = a.sims_gb_number) AND (sims_gb_cat_code = a.sims_gb_cat_code)) AS sims_gb_cat_points_possible,
							 b.sims_gb_name, c.narr_grade_group_name,
														 b.sims_gb_term_code , b.sims_gb_subject_code,
														 d.sims_gb_cat_assign_include_in_final_grade, sims_gb_cat_assign_weightage, d.sims_gb_cat_assign_grade_completed_status, b.sims_gb_grade_scale
														 ,d.sims_gb_cat_assign_date
															,ss.sims_subject_type,st.sims_subject_type_name_en
FROM            sims.sims_gradebook_category_assignment_student AS a LEFT OUTER JOIN
                         sims.sims_gradebook AS b ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = b.sims_academic_year AND a.sims_grade_code = b.sims_grade_code AND 
                         a.sims_section_code = b.sims_section_code AND a.sims_gb_number = b.sims_gb_number LEFT OUTER JOIN
                         sims.sims_gradebook_narrative_grade AS c ON b.sims_gb_grade_scale = c.narr_grade_group_code LEFT OUTER JOIN
                         sims.sims_gradebook_category_assignment AS d ON a.sims_cur_code = d.sims_cur_code AND a.sims_academic_year = d.sims_academic_year AND a.sims_grade_code = d.sims_grade_code AND 
                         a.sims_section_code = d.sims_section_code AND a.sims_gb_number = d.sims_gb_number AND a.sims_gb_cat_code = d.sims_gb_cat_code AND 
                         a.sims_gb_cat_assign_number = d.sims_gb_cat_assign_number LEFT OUTER JOIN
                         sims.student_list AS e ON a.sims_cur_code = e.sims_student_cur_code AND a.sims_academic_year = e.sims_academic_year AND a.sims_grade_code = e.sims_grade_code AND 
                         a.sims_section_code = e.sims_section_code AND a.sims_gb_cat_assign_enroll_number = e.sims_student_enroll_number LEFT OUTER JOIN
                         sims.sims_gradebook_mark_grade_scale AS f ON a.sims_cur_code = f.sims_cur_code AND a.sims_academic_year = f.sims_academic_year AND f.sims_mark_grade = b.sims_gb_grade_scale AND 
                         a.sims_gb_cat_assign_final_grade = f.sims_mark_grade_code
						  Left Outer JOIN sims.sims_subject AS ss ON a.sims_cur_code = ss.sims_cur_code AND 
						 b.sims_gb_subject_code = ss.sims_subject_code Left Outer JOIN sims.sims_subject_type AS st ON 
						 a.sims_cur_code = st.sims_cur_code AND ss.sims_subject_type = st.sims_subject_type_code 










GO
/****** Object:  View [sims].[sims_report_card_category_details]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [sims].[sims_report_card_category_details]
AS
SELECT        a.sims_cur_code, a.sims_academic_year, a.sims_grade_code, a.sims_section_code, a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number, a.sims_gb_cat_code, a.category_weight, 
                         CONVERT(numeric(6, 2), SUM(CAST(a.sims_gb_cat_assign_weightage AS numeric(6, 2)) * ISNULL(a.final_mark, 0) / 100)) AS final_marks, SUM(CONVERT(numeric(6, 2), ISNULL(a.final_mark, 0))) 
                         AS cum_marks_obtained, SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score)) AS final_max_marks, 
						   SUM(CAST(a.sims_gb_cat_points_possible AS numeric(6,2)) * ISNULL(a.final_mark, 0) / 100) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))AS final_marks_percentage,
						 --CONVERT(int, CONVERT(numeric(6, 2), 
       --                  SUM(CAST(a.sims_gb_cat_points_possible AS numeric(6,2)) * ISNULL(a.final_mark, 0) / 100)) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))) AS final_marks_percentage,
                             (SELECT        sims_mark_grade_name
                               FROM            sims.sims_gradebook_mark_grade_scale AS gs
                               WHERE        (sims_cur_code = a.sims_cur_code) AND (sims_academic_year = a.sims_academic_year) AND (sims_mark_grade = a.sims_gb_grade_scale) AND (CONVERT(int, CONVERT(numeric(6, 2), 
                                                         SUM(CAST(a.sims_gb_cat_assign_weightage AS numeric(6, 2)) * ISNULL(a.final_mark, 0) / 100)) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))) BETWEEN 
                                                         sims_mark_grade_low AND sims_mark_grade_high)) AS final_cum_grade, b.sims_subject_name_en, c.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_gb_term_code, e.sims_level_code, 
                         e.sims_report_card_code, e.sims_report_card_term, a.sims_gb_subject_code, f.sims_display_order, g.sims_grade_name_en, h.sims_section_name_en, i.sims_comment1_code, i.sims_user1_code, 
                         i.sims_comment2_code, i.sims_user2_code, j.sims_comment_desc AS comment_1, k.sims_comment_desc AS comment_2
FROM            sims.sims_gradebook_category_assignment_student_marks_list AS a LEFT OUTER JOIN
                         sims.sims_subject AS b ON a.sims_cur_code = b.sims_cur_code AND a.sims_gb_subject_code = b.sims_subject_code LEFT OUTER JOIN
                         sims.sims_term AS c ON a.sims_cur_code = c.sims_cur_code AND a.sims_academic_year = c.sims_academic_year AND a.sims_gb_term_code = c.sims_term_code LEFT OUTER JOIN
                         sims.sims_report_card AS e ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = e.sims_academic_year AND a.sims_grade_code = e.sims_grade_code AND 
                         a.sims_section_code = e.sims_section_code AND a.sims_gb_term_code = e.sims_report_card_term LEFT OUTER JOIN
                         sims.sims_section_subject AS f ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = f.sims_academic_year AND a.sims_grade_code = f.sims_grade_code AND 
                         a.sims_section_code = f.sims_section_code AND a.sims_gb_subject_code = f.sims_subject_code LEFT OUTER JOIN
                         sims.sims_grade AS g ON a.sims_cur_code = g.sims_cur_code AND a.sims_academic_year = g.sims_academic_year AND a.sims_grade_code = g.sims_grade_code LEFT OUTER JOIN
                         sims.sims_section AS h ON a.sims_cur_code = h.sims_cur_code AND a.sims_academic_year = h.sims_academic_year AND a.sims_grade_code = h.sims_grade_code AND 
                         a.sims_section_code = h.sims_section_code LEFT OUTER JOIN
                         sims.sims_report_card_student_subject_comment AS i ON a.sims_cur_code = i.sims_cur_code AND a.sims_academic_year = i.sims_academic_year AND a.sims_grade_code = i.sims_grade_code AND 
                         a.sims_section_code = i.sims_section_code AND e.sims_level_code = i.sims_level_code AND e.sims_report_card_code = i.sims_report_card_code AND a.sims_gb_subject_code = i.sims_subject_code AND 
                         a.sims_gb_cat_assign_enroll_number = i.sims_enroll_number LEFT OUTER JOIN
                         sims.sims_report_card_comment AS j ON a.sims_cur_code = j.sims_cur_code AND a.sims_academic_year = j.sims_academic_year AND e.sims_level_code = j.sims_level_code AND 
                         e.sims_report_card_code = j.sims_report_card_code AND i.sims_comment1_code = j.sims_comment_code LEFT OUTER JOIN
                         sims.sims_report_card_comment AS k ON a.sims_cur_code = k.sims_cur_code AND a.sims_academic_year = k.sims_academic_year AND e.sims_level_code = k.sims_level_code AND 
                         e.sims_report_card_code = k.sims_report_card_code AND i.sims_comment2_code = k.sims_comment_code
WHERE        (a.sims_gb_cat_assign_include_in_final_grade = 'T') AND (a.sims_gb_cat_assign_grade_completed_status = 'T')
GROUP BY a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number, b.sims_subject_name_en, c.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_cur_code, a.sims_academic_year, 
                         a.sims_grade_code, a.sims_section_code, a.sims_gb_term_code, a.sims_gb_cat_code, a.category_weight, e.sims_level_code, e.sims_report_card_code, e.sims_report_card_term, a.sims_gb_subject_code, 
                         f.sims_display_order, g.sims_grade_name_en, h.sims_section_name_en, i.sims_comment1_code, i.sims_user1_code, i.sims_comment2_code, i.sims_user2_code, j.sims_comment_desc, 
                         k.sims_comment_desc



GO
/****** Object:  View [sims].[sims_report_card_details]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE View [sims].[sims_report_card_details]
AS
select	a.sims_cur_code,
			a.sims_academic_year,
			a.sims_grade_code,
			a.sims_section_code,
		a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number , 
		CONVERT(numeric(6,2), Sum(CAST(a.category_weight AS numeric(6,2))*ISNULL(a.final_marks,0)/100)) AS final_marks,
		SUM(CONVERT(numeric(6,2),ISNULL(a.final_marks,0))) AS cum_marks_obtained, SUM(CONVERT(numeric(6,2), a.final_max_marks)) AS final_max_marks,
		CONVERT(int, (CONVERT(numeric(6,2), sum(CAST(a.category_weight AS numeric(6,2))*ISNULL(a.final_marks,0))))
		/(SUM(CONVERT(numeric(6,2), a.final_max_marks)))) AS final_marks_percentage,
		(SELECT gs.sims_mark_grade_name
		 FROM sims.sims_gradebook_mark_grade_scale AS gs
		 WHERE gs.sims_cur_code = a.sims_cur_code AND 
				gs.sims_academic_year = a.sims_academic_year AND 
				gs.sims_mark_grade = a.sims_gb_grade_scale AND 
				CONVERT(int, (CONVERT(numeric(6,2), sum(CAST(a.category_weight AS numeric(6,2))*ISNULL(a.final_marks,0))))
		/(SUM(CONVERT(numeric(6,2), a.final_max_marks)))) BETWEEN gs.sims_mark_grade_low AND gs.sims_mark_grade_high) AS final_cum_grade,
		a.sims_subject_name_en, 
		a.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_gb_term_code, 
		a.sims_level_code, a.sims_report_card_code, a.sims_report_card_term, a.sims_gb_subject_code, a.sims_display_order,a.sims_grade_name_en,a.sims_section_name_en,
		a.sims_comment1_code, a.sims_user1_code, 
		a.sims_comment2_code, a.sims_user2_code, a.comment_1, a.comment_2
from	sims.sims_report_card_category_details AS a 
GROUP BY a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number ,
		a.sims_subject_name_en,	a.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_cur_code,
			a.sims_academic_year,
			a.sims_grade_code,
			a.sims_section_code, a.sims_gb_term_code,
			a.sims_level_code, a.sims_report_card_code, a.sims_report_card_term, a.sims_gb_subject_code,
			a.sims_display_order,a.sims_grade_name_en,a.sims_section_name_en, 
			a.sims_comment1_code, a.sims_user1_code, 
		a.sims_comment2_code, a.sims_user2_code, a.comment_1, a.comment_2 





GO
/****** Object:  View [fins].[fins_pdc_status_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [fins].[fins_pdc_status_list]
AS
SELECT        a.pc_comp_code, a.pc_doc_type, a.pc_our_doc_no, a.pc_sl_ldgr_code, a.pc_sl_acno, c.coad_pty_full_name AS pty_name, a.pc_cheque_no,
                             (SELECT        pb_bank_name
                               FROM            fins.fins_bank
                               WHERE        pb_bank_code = a.pc_bank_from) AS pc_bank_name, pc_bank_from, a.pc_due_date, a.pc_submission_date, a.pc_resub_date, 
                         a.pc_cancel_date, a.pc_resub_ref, a.pc_narrative, a.pc_amount, ISNULL(a.pc_discd, 'O') AS pdc_status
FROM            fins.fins_pdc_cheques AS a INNER JOIN
                         fins.fins_sblgr_masters AS b ON a.pc_comp_code = b.slma_comp_code AND a.pc_sl_ldgr_code = b.slma_ldgrctl_code AND 
                         a.pc_sl_acno = b.slma_acno INNER JOIN
                         fins.fins_pty_address AS c ON a.pc_comp_code = c.coad_comp_code AND b.slma_addr_id = c.coad_addr_id
WHERE        (a.pc_sl_acno != '03' OR
                         a.pc_sl_ldgr_code != '04')
UNION
SELECT        a.pc_comp_code, a.pc_doc_type, a.pc_our_doc_no, a.pc_sl_ldgr_code, a.pc_sl_acno, c.student_name AS pty_name, a.pc_cheque_no,
                             (SELECT        pb_bank_name
                               FROM            fins.fins_bank
                               WHERE        pb_bank_code = a.pc_bank_from) AS pc_bank_name, pc_bank_from, a.pc_due_date, a.pc_submission_date, a.pc_resub_date, 
                         a.pc_cancel_date, a.pc_resub_ref, a.pc_narrative, a.pc_amount, ISNULL(a.pc_discd, 'O') AS pdc_status
FROM            fins.fins_pdc_cheques AS a INNER JOIN
                         fins.fins_sblgr_masters AS b ON a.pc_comp_code = b.slma_comp_code AND a.pc_sl_ldgr_code = b.slma_ldgrctl_code AND 
                         a.pc_sl_acno = b.slma_acno INNER JOIN
                         sims.student_list AS c ON b.slma_acno = c.sims_student_enroll_number
WHERE        a.pc_sl_ldgr_code = '04'
UNION
SELECT        a.pc_comp_code, a.pc_doc_type, a.pc_our_doc_no, a.pc_sl_ldgr_code, a.pc_sl_acno, 
                         c.em_first_name + ' ' + c.em_middle_name + ' ' + c.em_last_name AS pty_name, a.pc_cheque_no,
                             (SELECT        pb_bank_name
                               FROM            fins.fins_bank
                               WHERE        pb_bank_code = a.pc_bank_from) AS pc_bank_name, pc_bank_from, a.pc_due_date, a.pc_submission_date, a.pc_resub_date, 
                         a.pc_cancel_date, a.pc_resub_ref, a.pc_narrative, a.pc_amount, ISNULL(a.pc_discd, 'O') AS pdc_status
FROM            fins.fins_pdc_cheques AS a INNER JOIN
                         fins.fins_sblgr_masters AS b ON a.pc_comp_code = b.slma_comp_code AND a.pc_sl_ldgr_code = b.slma_ldgrctl_code AND 
                         a.pc_sl_acno = b.slma_acno INNER JOIN
                         pays.pays_employee AS c ON b.slma_acno = c.em_login_code
WHERE        a.pc_sl_ldgr_code = '03'



GO
/****** Object:  View [sims].[sims_report_card_details_siso]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- View

CREATE VIEW [sims].[sims_report_card_details_siso]
AS
--SELECT        a.sims_cur_code, a.sims_academic_year, a.sims_grade_code, a.sims_section_code, a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number, a.sims_gb_cat_code, a.category_weight, 
--              CONVERT(numeric(6, 2), SUM(a.final_mark)) AS final_marks, 
--			  SUM(CONVERT(numeric(6, 2), ISNULL(a.final_mark, 0))) 
--                         AS cum_marks_obtained, 

--			CONVERT(numeric(6,2),SUM(a.sims_gb_cat_assign_max_score)) AS final_max_marks, 


--			CONVERT(int, CONVERT(numeric(6, 2), 
--                         SUM(CAST(a.sims_gb_cat_assign_weightage AS numeric(6, 2)) * ISNULL(a.final_mark, 0) / 100)) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))) AS final_marks_percentage,
--                             (SELECT        sims_mark_grade_name
--                               FROM            sims.sims_gradebook_mark_grade_scale AS gs
--                               WHERE        (sims_cur_code = a.sims_cur_code) AND (sims_academic_year = a.sims_academic_year) AND (sims_mark_grade = a.sims_gb_grade_scale) AND (CONVERT(int, CONVERT(numeric(6, 2), 
--                                                         SUM(CAST(a.sims_gb_cat_assign_weightage AS numeric(6, 2)) * ISNULL(a.final_mark, 0) / 100)) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))) BETWEEN 
--                                                         sims_mark_grade_low AND sims_mark_grade_high)) AS final_cum_grade, b.sims_subject_name_en, c.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_gb_term_code, e.sims_level_code, 
--                         e.sims_report_card_code, e.sims_report_card_term, a.sims_gb_subject_code, f.sims_display_order, g.sims_grade_name_en, h.sims_section_name_en, i.sims_comment1_code, i.sims_user1_code, 
--                         i.sims_comment2_code, i.sims_user2_code, j.sims_comment_desc AS comment_1, k.sims_comment_desc AS comment_2
--FROM           sims.sims_gradebook_category_assignment_student_marks_list a LEFT OUTER JOIN
--                         sims.sims_subject AS b ON a.sims_cur_code = b.sims_cur_code AND a.sims_gb_subject_code = b.sims_subject_code LEFT OUTER JOIN
--                         sims.sims_term AS c ON a.sims_cur_code = c.sims_cur_code AND a.sims_academic_year = c.sims_academic_year AND a.sims_gb_term_code = c.sims_term_code LEFT OUTER JOIN
--						 sims.sims_report_card_allocation p ON a.sims_cur_code = p.sims_report_card_cur_code AND a.sims_academic_year = p.sims_report_card_academic_year AND 
--						 a.sims_grade_code = p.sims_report_card_grade_code AND a.sims_section_code = p.sims_report_card_section_code AND 
--						 a.sims_gb_number = p.sims_gb_number and a.sims_gb_cat_code = p.sims_gb_cat_code and a.sims_gb_cat_assign_number = p.sims_gb_cat_assign_number LEFT OUTER JOIN
--                         sims.sims_report_card AS e ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = e.sims_academic_year AND a.sims_grade_code = e.sims_grade_code AND 
--                         a.sims_section_code = e.sims_section_code AND a.sims_gb_term_code = e.sims_report_card_term AND 
--						 p.sims_report_card_code = e.sims_report_card_code AND p.sims_report_card_level_code = e.sims_level_code LEFT OUTER JOIN
--                         sims.sims_section_subject AS f ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = f.sims_academic_year AND a.sims_grade_code = f.sims_grade_code AND 
--                         a.sims_section_code = f.sims_section_code AND a.sims_gb_subject_code = f.sims_subject_code LEFT OUTER JOIN
--                         sims.sims_grade AS g ON a.sims_cur_code = g.sims_cur_code AND a.sims_academic_year = g.sims_academic_year AND a.sims_grade_code = g.sims_grade_code LEFT OUTER JOIN
--                         sims.sims_section AS h ON a.sims_cur_code = h.sims_cur_code AND a.sims_academic_year = h.sims_academic_year AND a.sims_grade_code = h.sims_grade_code AND 
--                         a.sims_section_code = h.sims_section_code LEFT OUTER JOIN
--                         sims.sims_report_card_student_subject_comment AS i ON a.sims_cur_code = i.sims_cur_code AND a.sims_academic_year = i.sims_academic_year AND a.sims_grade_code = i.sims_grade_code AND 
--                         a.sims_section_code = i.sims_section_code AND e.sims_level_code = i.sims_level_code AND e.sims_report_card_code = i.sims_report_card_code AND a.sims_gb_subject_code = i.sims_subject_code AND 
--                         a.sims_gb_cat_assign_enroll_number = i.sims_enroll_number LEFT OUTER JOIN
--                         sims.sims_report_card_comment AS j ON a.sims_cur_code = j.sims_cur_code AND a.sims_academic_year = j.sims_academic_year AND e.sims_level_code = j.sims_level_code AND 
--                         e.sims_report_card_code = j.sims_report_card_code AND i.sims_comment1_code = j.sims_comment_code LEFT OUTER JOIN
--                         sims.sims_report_card_comment AS k ON a.sims_cur_code = k.sims_cur_code AND a.sims_academic_year = k.sims_academic_year AND e.sims_level_code = k.sims_level_code AND 
--                         e.sims_report_card_code = k.sims_report_card_code AND i.sims_comment2_code = k.sims_comment_code
--WHERE        (a.sims_gb_cat_assign_include_in_final_grade = 'T') AND (a.sims_gb_cat_assign_grade_completed_status = 'T') AND 
--				a.final_mark is not NULL 

--GROUP BY a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number, b.sims_subject_name_en, c.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_cur_code, a.sims_academic_year, 
--                         a.sims_grade_code, a.sims_section_code, a.sims_gb_term_code, a.sims_gb_cat_code, a.category_weight, e.sims_level_code, e.sims_report_card_code, e.sims_report_card_term, a.sims_gb_subject_code, 
--                         f.sims_display_order, g.sims_grade_name_en, h.sims_section_name_en, i.sims_comment1_code, i.sims_user1_code, i.sims_comment2_code, i.sims_user2_code, j.sims_comment_desc, 
--                         k.sims_comment_desc


SELECT        a.sims_cur_code, a.sims_academic_year, a.sims_grade_code, a.sims_section_code, a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number, a.sims_gb_cat_code, a.category_weight, 
              CONVERT(numeric(6, 2), SUM(a.final_mark)) AS final_marks1, 
			  CASE WHEN b.sims_subject_type = '01' THEN 
									convert(numeric(6,2),IIF(a.sims_gb_cat_code = '1',SUM(final_mark)/2,IIF(a.sims_gb_cat_code = 2,
									(ISNULL((select max(final_mark) from sims.sims_gradebook_category_assignment_student_marks_list m 
									where m.sims_cur_code = a.sims_cur_code and m.sims_academic_year = a.sims_academic_year and 
									m.sims_grade_code = a.sims_grade_code and m.sims_section_code = a.sims_section_code and 
									m.sims_gb_number = a.sims_gb_number and m.sims_gb_cat_code = a.sims_gb_cat_code and 
									m.sims_gb_cat_assign_enroll_number = a.sims_gb_cat_assign_enroll_number and sims_gb_cat_assign_number in ('1','2')),0)+
									ISNULL((select max(final_mark) from sims.sims_gradebook_category_assignment_student_marks_list m 
									where m.sims_cur_code = a.sims_cur_code and m.sims_academic_year = a.sims_academic_year and 
									m.sims_grade_code = a.sims_grade_code and m.sims_section_code = a.sims_section_code and 
									m.sims_gb_number = a.sims_gb_number and m.sims_gb_cat_code = a.sims_gb_cat_code and 
									m.sims_gb_cat_assign_enroll_number = a.sims_gb_cat_assign_enroll_number and sims_gb_cat_assign_number in ('3')),0))/2,
									IIF(a.sims_grade_code in ('11','12'),SUM(final_mark)/3,IIF(a.sims_grade_code in ('03','04','05','06','07'),SUM(final_mark),SUM(final_mark)/2)))))	
				ELSE CONVERT(numeric(6, 2),SUM(a.final_mark)) 
									END AS final_marks,
			  SUM(CONVERT(numeric(6, 2), ISNULL(a.final_mark, 0))) 
                         AS cum_marks_obtained, 

			CONVERT(numeric(6,2),SUM(a.sims_gb_cat_assign_max_score)) AS final_max_marks1, 
			CASE WHEN b.sims_subject_type = '01' THEN 
									convert(numeric(6,2),IIF(a.sims_gb_cat_code = '1',SUM(sims_gb_cat_assign_max_score)/2,IIF(a.sims_gb_cat_code = 2,
									(ISNULL((select max(sims_gb_cat_assign_max_score) from sims.sims_gradebook_category_assignment_student_marks_list m 
									where m.sims_cur_code = a.sims_cur_code and m.sims_academic_year = a.sims_academic_year and 
									m.sims_grade_code = a.sims_grade_code and m.sims_section_code = a.sims_section_code and 
									m.sims_gb_number = a.sims_gb_number and m.sims_gb_cat_code = a.sims_gb_cat_code and 
									m.sims_gb_cat_assign_enroll_number = a.sims_gb_cat_assign_enroll_number and sims_gb_cat_assign_number in ('1','2')),0)+
									ISNULL((select max(sims_gb_cat_assign_max_score) from sims.sims_gradebook_category_assignment_student_marks_list m 
									where m.sims_cur_code = a.sims_cur_code and m.sims_academic_year = a.sims_academic_year and 
									m.sims_grade_code = a.sims_grade_code and m.sims_section_code = a.sims_section_code and 
									m.sims_gb_number = a.sims_gb_number and m.sims_gb_cat_code = a.sims_gb_cat_code and 
									m.sims_gb_cat_assign_enroll_number = a.sims_gb_cat_assign_enroll_number and sims_gb_cat_assign_number in ('3')),0))/2,
									IIF(a.sims_grade_code in ('11','12'),SUM(sims_gb_cat_assign_max_score)/3,IIF(a.sims_grade_code  in ('03','04','05','06','07'),SUM(sims_gb_cat_assign_max_score),SUM(sims_gb_cat_assign_max_score)/2)))))	
				ELSE CONVERT(numeric(6, 2),SUM(a.sims_gb_cat_assign_max_score)) 
									END AS final_max_marks,

			CONVERT(int, CONVERT(numeric(6, 2), 
                         SUM(CAST(a.sims_gb_cat_assign_weightage AS numeric(6, 2)) * ISNULL(a.final_mark, 0) / 100)) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))) AS final_marks_percentage,
                             (SELECT        sims_mark_grade_name
                               FROM            sims.sims_gradebook_mark_grade_scale AS gs
                               WHERE        (sims_cur_code = a.sims_cur_code) AND (sims_academic_year = a.sims_academic_year) AND (sims_mark_grade = a.sims_gb_grade_scale) AND (CONVERT(int, CONVERT(numeric(6, 2), 
                                                         SUM(CAST(a.sims_gb_cat_assign_weightage AS numeric(6, 2)) * ISNULL(a.final_mark, 0) / 100)) * 100 / SUM(CONVERT(numeric(6, 2), a.sims_gb_cat_assign_max_score))) BETWEEN 
                                                         sims_mark_grade_low AND sims_mark_grade_high)) AS final_cum_grade, b.sims_subject_name_en, c.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_gb_term_code, NULL AS sims_level_code, 
                         NULL AS sims_report_card_code, NULL AS sims_report_card_term, a.sims_gb_subject_code, f.sims_display_order, g.sims_grade_name_en, h.sims_section_name_en, i.sims_comment1_code, i.sims_user1_code, 
                         i.sims_comment2_code, i.sims_user2_code, j.sims_comment_desc AS comment_1, k.sims_comment_desc AS comment_2
						 ,sims_gb_cat_points_possible
FROM           sims.sims_gradebook_category_assignment_student_marks_list a LEFT OUTER JOIN
                         sims.sims_subject AS b ON a.sims_cur_code = b.sims_cur_code AND a.sims_gb_subject_code = b.sims_subject_code LEFT OUTER JOIN
                         sims.sims_term AS c ON a.sims_cur_code = c.sims_cur_code AND a.sims_academic_year = c.sims_academic_year AND a.sims_gb_term_code = c.sims_term_code LEFT OUTER JOIN
						 sims.sims_report_card_allocation p ON a.sims_cur_code = p.sims_report_card_cur_code AND a.sims_academic_year = p.sims_report_card_academic_year AND 
						 a.sims_grade_code = p.sims_report_card_grade_code AND a.sims_section_code = p.sims_report_card_section_code AND 
						 a.sims_gb_number = p.sims_gb_number and a.sims_gb_cat_code = p.sims_gb_cat_code and a.sims_gb_cat_assign_number = p.sims_gb_cat_assign_number LEFT OUTER JOIN
                         sims.sims_report_card AS e ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = e.sims_academic_year AND a.sims_grade_code = e.sims_grade_code AND 
                         a.sims_section_code = e.sims_section_code AND a.sims_gb_term_code = e.sims_report_card_term AND 
						 p.sims_report_card_code = e.sims_report_card_code AND p.sims_report_card_level_code = e.sims_level_code LEFT OUTER JOIN
                         sims.sims_section_subject AS f ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = f.sims_academic_year AND a.sims_grade_code = f.sims_grade_code AND 
                         a.sims_section_code = f.sims_section_code AND a.sims_gb_subject_code = f.sims_subject_code LEFT OUTER JOIN
                         sims.sims_grade AS g ON a.sims_cur_code = g.sims_cur_code AND a.sims_academic_year = g.sims_academic_year AND a.sims_grade_code = g.sims_grade_code LEFT OUTER JOIN
                         sims.sims_section AS h ON a.sims_cur_code = h.sims_cur_code AND a.sims_academic_year = h.sims_academic_year AND a.sims_grade_code = h.sims_grade_code AND 
                         a.sims_section_code = h.sims_section_code LEFT OUTER JOIN
                         sims.sims_report_card_student_subject_comment AS i ON a.sims_cur_code = i.sims_cur_code AND a.sims_academic_year = i.sims_academic_year AND a.sims_grade_code = i.sims_grade_code AND 
                         a.sims_section_code = i.sims_section_code AND e.sims_level_code = i.sims_level_code AND e.sims_report_card_code = i.sims_report_card_code AND a.sims_gb_subject_code = i.sims_subject_code AND 
                         a.sims_gb_cat_assign_enroll_number = i.sims_enroll_number LEFT OUTER JOIN
                         sims.sims_report_card_comment AS j ON a.sims_cur_code = j.sims_cur_code AND a.sims_academic_year = j.sims_academic_year AND e.sims_level_code = j.sims_level_code AND 
                         e.sims_report_card_code = j.sims_report_card_code AND i.sims_comment1_code = j.sims_comment_code LEFT OUTER JOIN
                         sims.sims_report_card_comment AS k ON a.sims_cur_code = k.sims_cur_code AND a.sims_academic_year = k.sims_academic_year AND e.sims_level_code = k.sims_level_code AND 
                         e.sims_report_card_code = k.sims_report_card_code AND i.sims_comment2_code = k.sims_comment_code
WHERE        --(a.sims_gb_cat_assign_include_in_final_grade = 'T') AND (a.sims_gb_cat_assign_grade_completed_status = 'T') AND 
				a.final_mark is not NULL 	
GROUP BY a.student_name, a.sims_gb_cat_assign_enroll_number, a.sims_gb_number, b.sims_subject_name_en, c.sims_term_desc_en, a.sims_gb_grade_scale, a.sims_cur_code, a.sims_academic_year, 
                         a.sims_grade_code, a.sims_section_code, a.sims_gb_term_code, a.sims_gb_cat_code, a.category_weight, a.sims_gb_subject_code, 
                         f.sims_display_order, g.sims_grade_name_en, h.sims_section_name_en, i.sims_comment1_code, i.sims_user1_code, i.sims_comment2_code, i.sims_user2_code, j.sims_comment_desc, 
                         k.sims_comment_desc,b.sims_subject_type,sims_gb_cat_points_possible















GO
/****** Object:  View [sims].[sims_report_card_category_details_sjs]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

Create VIEW [sims].[sims_report_card_category_details_sjs]
AS
SELECT	sims_cur_code, sims_academic_year, sims_grade_code,sims_section_code,sims_gb_cat_assign_enroll_number,
		student_name, sims_gb_number, sims_subject_name_en, sims_term_desc_en, sims_gb_grade_scale,	sims_gb_term_code,
		sims_level_code, sims_report_card_code,sims_report_card_term, sims_gb_subject_code, sims_display_order,
		sims_grade_name_en, sims_section_name_en, 
		sims_comment1_code, sims_user1_code, comment_1,
		sims_comment2_code, sims_user2_code, comment_2,
		Max(catCode_1) AS cat_1,SUM(finalMark_1) AS finalMark_1,MAX(catWeightage_1) AS cat_weightage_1,
		MAX(maxMarks_1) AS maxMarks_1, 
		Max(catCode_2) AS cat_2,SUM(finalMark_2) AS finalMark_2,MAX(catWeightage_2) AS cat_weightage_2,
		MAX(maxMarks_2) AS maxMarks_2, 
		Max(catCode_3) AS cat_3,SUM(finalMark_3) AS finalMark_3,MAX(catWeightage_3) AS cat_weightage_3,
		MAX(maxMarks_3) AS maxMarks_3, 
		Max(catCode_4) AS cat_4,SUM(finalMark_4) AS finalMark_4,MAX(catWeightage_4) AS cat_weightage_4,
		MAX(maxMarks_4) AS maxMarks_4, 
		Max(catCode_5) AS cat_5,SUM(finalMark_5) AS finalMark_5,MAX(catWeightage_5) AS cat_weightage_5,
		MAX(maxMarks_5) AS maxMarks_5, 
		Max(catCode_6) AS cat_6,SUM(finalMark_6) AS finalMark_6,MAX(catWeightage_6) AS cat_weightage_6,
		MAX(maxMarks_6) AS maxMarks_6, 
		Max(catCode_7) AS cat_7,SUM(finalMark_7) AS finalMark_7,MAX(catWeightage_7) AS cat_weightage_7,
		MAX(maxMarks_7) AS maxMarks_7, 
		sims_subject_type,sims_subject_type_name_en
FROM	(
			select	b.*,
			c.col+'_'+CAST(b.seq AS nvarchar(2)) AS col,
			c.val
			FROM	(
			SELECT	ROW_NUMBER() OVER (Partition BY a.sims_cur_code, sims_academic_year,
											a.sims_grade_code,sims_section_code,
											sims_gb_cat_assign_enroll_number, a.sims_gb_number
								 ORDER BY a.sims_gb_cat_code) AS seq, a.* , ss.sims_subject_type,
								 st.sims_subject_type_name_en
			FROM	sims.sims_report_card_category_details AS a INNER JOIN sims.sims_subject AS ss ON 
					a.sims_cur_code = ss.sims_cur_code AND 
					a.sims_gb_subject_code = ss.sims_subject_code INNER JOIN sims.sims_subject_type AS st ON 
					a.sims_cur_code = st.sims_cur_code AND 
					ss.sims_subject_type = st.sims_subject_type_code
			) b
			CROSS APPLY
			(
				SELECT 'catCode',sims_gb_cat_code UNION ALL 
				SELECT 'finalMark',final_marks UNION ALL
				SELECT 'catWeightage', category_weight UNION ALL
				SELECT 'maxMarks', final_max_marks
			) c (col,val)
		) d
		PIVOT
		(
			max(val) for col in(catCode_1,finalMark_1,catWeightage_1,maxMarks_1,
								catCode_2,finalMark_2,catWeightage_2,maxMarks_2,
								catCode_3,finalMark_3,catWeightage_3,maxMarks_3,
								catCode_4,finalMark_4,catWeightage_4,maxMarks_4,
								catCode_5,finalMark_5,catWeightage_5,maxMarks_5,
								catCode_6,finalMark_6,catWeightage_6,maxMarks_6,
								catCode_7,finalMark_7,catWeightage_7,maxMarks_7)
		) AS pvtTbl 
GROUP BY	sims_cur_code, sims_academic_year, sims_grade_code,sims_section_code,sims_gb_cat_assign_enroll_number,
		sims_gb_number, student_name, sims_subject_name_en, sims_term_desc_en, sims_gb_grade_scale,	sims_gb_term_code,
		sims_level_code, sims_report_card_code,sims_report_card_term, sims_gb_subject_code, sims_display_order,
		sims_grade_name_en, sims_section_name_en, 
		sims_comment1_code, sims_user1_code, comment_1,
		sims_comment2_code, sims_user2_code, comment_2,
		sims_subject_type,sims_subject_type_name_en





GO
/****** Object:  View [sims].[sims_fee_ledger_with_invoice ]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

-- View

CREATE VIEW [sims].[sims_fee_ledger_with_invoice ] 

AS
(SELECT        a.sims_fee_cur_code, a.sims_fee_academic_year, a.sims_fee_grade_code, a.sims_fee_section_code, a.sims_enroll_number, m.sims_sibling_parent_number, a.sims_fee_number, a.sims_fee_frequency, 
                         sims_fee_period, sims_expected_fee, 0 AS sims_paid_fee,0 AS dd_fee_amount_discounted, c.sims_fee_code_description, UPPER(SUBSTRING(dbo.GetMonthName(Substring(a.sims_fee_period, 16, 2)), 1, 3)) AS sims_fee_period_name, 
                         CASE WHEN a.sims_fee_frequency IN ('O', 'Y') THEN
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS ay
                               WHERE        ay.sims_cur_code = a.sims_fee_cur_code AND ay.sims_academic_year = a.sims_fee_academic_year) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, Substring(a.sims_fee_period, 
                         16, 2) + '-' + '01' + '-' + a.sims_fee_academic_year, 110) >
                             (SELECT        sims_academic_year_end_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, Substring(a.sims_fee_period, 16, 2) 
                         + '-' + '01' + '-' + CAST(a.sims_fee_academic_year - 1 AS nvarchar), 110) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, Substring(a.sims_fee_period, 16, 2) 
                         + '-' + '01' + '-' + a.sims_fee_academic_year, 110) <
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, Substring(a.sims_fee_period, 16, 2) 
                         + '-' + '1' + '-' + CAST(a.sims_fee_academic_year + 1 AS nvarchar), 110) ELSE CONVERT(date, Substring(a.sims_fee_period, 16, 2) + '-' + '1' + '-' + a.sims_fee_academic_year, 110) END AS sims_date, NULL 
                         AS doc_no,NULL AS dd_fee_payment_mode, NULL AS dd_fee_cheque_number, NULL AS dd_chq_status, NULL AS dd_fee_credit_card_code 
FROM            (SELECT        sims_fee_cur_code, sims_fee_academic_year, sims_fee_grade_code, sims_fee_section_code, sims_enroll_number, sims_fee_number, sims_fee_frequency, sims_fee_amount, sims_fee_period, 
                                                    sims_expected_fee
                          FROM            sims.sims_student_fee AS sf UNPIVOT (sims_expected_fee FOR sims_fee_period IN (sims_fee_period1, sims_fee_period2, sims_fee_period3, sims_fee_period4, sims_fee_period5, 
                                                    sims_fee_period6, sims_fee_period7, sims_fee_period8, sims_fee_period9, sims_fee_period10, sims_fee_period11, sims_fee_period12)) AS x
                          WHERE        sims_expected_fee != 0) AS a INNER JOIN
                         /*select * from */ sims.sims_section_fee AS b ON a.sims_fee_cur_code = b.sims_fee_cur_code AND a.sims_fee_academic_year = b.sims_fee_academic_year AND 
                         a.sims_fee_grade_code = b.sims_fee_grade_code AND a.sims_fee_section_code = b.sims_fee_section_code AND a.sims_fee_number = b.sims_fee_number INNER JOIN
                         sims.sims_fee_type AS c ON b.sims_fee_code = c.sims_fee_code INNER JOIN
                         sims.student_list AS m ON a.sims_fee_cur_code = m.sims_student_cur_code AND a.sims_fee_academic_year = m.sims_academic_year AND a.sims_fee_grade_code = m.sims_grade_code AND 
                         a.sims_fee_section_code = m.sims_section_code AND a.sims_enroll_number = m.sims_student_enroll_number)
UNION
(SELECT        a.sims_fee_cur_code, a.sims_fee_academic_year, a.sims_fee_grade_code, a.sims_fee_section_code, a.sims_enroll_number, m.sims_sibling_parent_number, a.sims_fee_number, a.sims_fee_frequency, 
                         sims_fee_period, 0 AS sims_expected_fee, sims_paid_fee,0 AS dd_fee_amount_discounted, c.sims_fee_code_description, UPPER(SUBSTRING(dbo.GetMonthName(LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1)), 1, 3)) AS sims_fee_period_name, 
                         CASE WHEN a.sims_fee_frequency IN ('O', 'Y') THEN
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS ay
                               WHERE        ay.sims_cur_code = a.sims_fee_cur_code AND ay.sims_academic_year = a.sims_fee_academic_year) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) + '-' + '01' + '-' + a.sims_fee_academic_year, 110) >
                             (SELECT        sims_academic_year_end_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) 
                         + '-' + '01' + '-' + CAST(a.sims_fee_academic_year - 1 AS nvarchar), 110) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1)
                         + '-' + '01' + '-' + a.sims_fee_academic_year, 110) <
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) 
                         + '-' + '1' + '-' + CAST(a.sims_fee_academic_year + 1 AS nvarchar), 110) ELSE CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) + '-' + '1' + '-' + a.sims_fee_academic_year, 110) END AS sims_date, NULL 
                         AS doc_no,'Concession' AS dd_fee_payment_mode, NULL AS dd_fee_cheque_number, NULL AS dd_chq_status, NULL AS dd_fee_credit_card_code 
FROM            (SELECT        sims_fee_cur_code, sims_fee_academic_year, sims_fee_grade_code, sims_fee_section_code, sims_enroll_number, sims_fee_number, sims_fee_frequency, sims_fee_amount, sims_fee_period, 
                                                    sims_paid_fee
                          FROM            sims.sims_student_fee AS sf UNPIVOT (sims_paid_fee FOR sims_fee_period IN (sims_fee_period1_concession, sims_fee_period2_concession,sims_fee_period3_concession, 
						  sims_fee_period4_concession,
						  sims_fee_period5_concession,
						  sims_fee_period6_concession,
						  sims_fee_period7_concession,
						  sims_fee_period8_concession,
						  sims_fee_period9_concession,
						  sims_fee_period10_concession,
						  sims_fee_period11_concession,
						  sims_fee_period12_concession)) AS x
                          WHERE        sims_paid_fee != 0) AS a INNER JOIN
                         sims.sims_section_fee AS b ON a.sims_fee_cur_code = b.sims_fee_cur_code AND a.sims_fee_academic_year = b.sims_fee_academic_year AND 
                         a.sims_fee_grade_code = b.sims_fee_grade_code AND a.sims_fee_section_code = b.sims_fee_section_code AND a.sims_fee_number = b.sims_fee_number INNER JOIN
                         sims.sims_fee_type AS c ON b.sims_fee_code = c.sims_fee_code INNER JOIN
                         sims.student_list AS m ON a.sims_fee_cur_code = m.sims_student_cur_code AND a.sims_fee_academic_year = m.sims_academic_year AND a.sims_fee_grade_code = m.sims_grade_code AND 
                         a.sims_fee_section_code = m.sims_section_code AND a.sims_enroll_number = m.sims_student_enroll_number
)
UNION
(SELECT        h.sims_student_cur_code AS sims_fee_cur_code, h.sims_academic_year AS sims_fee_academic_year, h.sims_grade_code AS sims_fee_grade_code, h.sims_section_code AS sims_fee_section_code, 
                          g.enroll_number AS sims_enroll_number, h.sims_sibling_parent_number, f.dd_fee_number AS sims_fee_number, NULL AS sims_fee_frequency, NULL AS sims_fee_period, 0 AS sims_expected_fee, 
                          f.dd_fee_amount AS sims_paid_fee, 
						  f.dd_fee_amount_discounted,j.sims_fee_code_description, UPPER(SUBSTRING(dbo.GetMonthName(f.dd_fee_period_code), 1, 3)), g.doc_date AS sims_date, f.doc_no,
						  						  f.dd_fee_payment_mode, f.dd_fee_cheque_number,
						  (SELECT pc_discd FROM fins.fins_pdc_cheques WHERE pc_sl_ldgr_code = '04' AND
						  pc_sl_acno = h.sims_student_enroll_number AND 
						  pc_cheque_no = f.dd_fee_cheque_number AND 
						  pc_due_date = f.dd_fee_cheque_date AND
						  pc_bank_from = f.dd_fee_cheque_bank_code) AS dd_chq_status, f.dd_fee_credit_card_code
 FROM            sims.sims_fee_document_details AS f INNER JOIN
                         sims.sims_fee_document AS g ON f.doc_no = g.doc_no INNER JOIN
                          sims.student_list AS h ON g.enroll_number = h.sims_student_enroll_number INNER JOIN
                          sims.sims_section_fee AS i ON h.sims_student_cur_code = i.sims_fee_cur_code AND h.sims_academic_year = i.sims_fee_academic_year AND h.sims_grade_code = i.sims_fee_grade_code AND 
                          h.sims_section_code = i.sims_fee_section_code AND f.dd_fee_number = i.sims_fee_number INNER JOIN
                          sims.sims_fee_type AS j ON j.sims_fee_code = i.sims_fee_code
						  where  LEN(LTRIM(g.doc_ref_code))<=0 and g.doc_status='1')

UNION
(select   h.sims_student_cur_code AS sims_fee_cur_code, h.sims_academic_year AS sims_fee_academic_year, h.sims_grade_code AS sims_fee_grade_code, h.sims_section_code AS sims_fee_section_code, 
							   g.enroll_number AS sims_enroll_number, h.sims_sibling_parent_number, f.dd_fee_number AS sims_fee_number, NULL AS sims_fee_frequency, NULL AS sims_fee_period, 0 AS sims_expected_fee
								,idd.id_fee_amount As sims_paid_fee,idd.id_fee_amount_discounted,sims_fee_code_description,UPPER(SUBSTRING(dbo.GetMonthName(idd.id_fee_period_code), 1, 3)),id.in_date As sims_date,
								id.in_ref_code as in_no, f.dd_fee_payment_mode, f.dd_fee_cheque_number,
						  (SELECT pc_discd FROM fins.fins_pdc_cheques WHERE pc_sl_ldgr_code = '04' AND
						  pc_sl_acno = h.sims_student_enroll_number AND 
						  pc_cheque_no = f.dd_fee_cheque_number AND 
						  pc_due_date = f.dd_fee_cheque_date AND
						  pc_bank_from = f.dd_fee_cheque_bank_code) AS dd_chq_status, f.dd_fee_credit_card_code
						  from sims.sims_invoice_document_details  As idd 
						   INNER JOIN  [sims].[sims_invoice_document] As id on idd.in_no=id.in_no INNER JOIN  sims.sims_fee_document_details AS f ON
						  idd.in_paying_agent_transaction_no=f.doc_paying_agent_transaction_no and --idd.in_paying_agent_transaction_lineno=f.dd_line_no and
						  id.[in_ref_code]=f.doc_no
						   INNER JOIN
                          sims.sims_fee_document AS g ON f.doc_no = g.doc_no INNER JOIN
                          sims.student_list AS h ON g.enroll_number = h.sims_student_enroll_number INNER JOIN
                          sims.sims_section_fee AS i ON h.sims_student_cur_code = i.sims_fee_cur_code AND h.sims_academic_year = i.sims_fee_academic_year AND h.sims_grade_code = i.sims_fee_grade_code AND 
                          h.sims_section_code = i.sims_fee_section_code AND f.dd_fee_number = i.sims_fee_number INNER JOIN
                          sims.sims_fee_type AS j ON j.sims_fee_code = i.sims_fee_code and id.in_status IN ('4'))









GO
/****** Object:  View [sims].[sims_report_card_category_details_sjs_updated]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

--select * from [sims].[sims_report_card_category_details_sjs]
Create VIEW [sims].[sims_report_card_category_details_sjs_updated]
AS
SELECT	sims_cur_code, sims_academic_year, sims_grade_code,sims_section_code,sims_gb_cat_assign_enroll_number,
		student_name, sims_gb_number, sims_subject_name_en, sims_term_desc_en, sims_gb_grade_scale,	sims_gb_term_code,
		--sims_level_code, sims_report_card_code,
		sims_report_card_term, sims_gb_subject_code, sims_display_order,
		sims_grade_name_en, sims_section_name_en, 
		sims_comment1_code, sims_user1_code, comment_1,
		sims_comment2_code, sims_user2_code, comment_2,
		Max(catCode_1) AS cat_1,SUM(finalMark_1) AS finalMark_1,MAX(catWeightage_1) AS cat_weightage_1,
		MAX(maxMarks_1) AS maxMarks_1, 
		Max(catCode_2) AS cat_2,SUM(finalMark_2) AS finalMark_2,MAX(catWeightage_2) AS cat_weightage_2,
		MAX(maxMarks_2) AS maxMarks_2, 
		Max(catCode_3) AS cat_3,SUM(finalMark_3) AS finalMark_3,MAX(catWeightage_3) AS cat_weightage_3,
		MAX(maxMarks_3) AS maxMarks_3, 
		Max(catCode_4) AS cat_4,SUM(finalMark_4) AS finalMark_4,MAX(catWeightage_4) AS cat_weightage_4,
		MAX(maxMarks_4) AS maxMarks_4, 
		Max(catCode_5) AS cat_5,SUM(finalMark_5) AS finalMark_5,MAX(catWeightage_5) AS cat_weightage_5,
		MAX(maxMarks_5) AS maxMarks_5, 
		Max(catCode_6) AS cat_6,SUM(finalMark_6) AS finalMark_6,MAX(catWeightage_6) AS cat_weightage_6,
		MAX(maxMarks_6) AS maxMarks_6, 
		Max(catCode_7) AS cat_7,SUM(finalMark_7) AS finalMark_7,MAX(catWeightage_7) AS cat_weightage_7,
		MAX(maxMarks_7) AS maxMarks_7, 
		sims_subject_type,sims_subject_type_name_en
FROM	(
			select	b.*,
			c.col+'_'+CAST(b.seq AS nvarchar(2)) AS col,
			c.val
			FROM	(
			SELECT	ROW_NUMBER() OVER (Partition BY a.sims_cur_code, sims_academic_year,
											a.sims_grade_code,sims_section_code,
											sims_gb_cat_assign_enroll_number, a.sims_gb_number
								 ORDER BY a.sims_gb_cat_code) AS seq, a.* , ss.sims_subject_type,
								 st.sims_subject_type_name_en
			FROM	sims.sims_report_card_category_details AS a INNER JOIN sims.sims_subject AS ss ON 
					a.sims_cur_code = ss.sims_cur_code AND 
					a.sims_gb_subject_code = ss.sims_subject_code INNER JOIN sims.sims_subject_type AS st ON 
					a.sims_cur_code = st.sims_cur_code AND 
					ss.sims_subject_type = st.sims_subject_type_code
			) b
			CROSS APPLY
			(
				SELECT 'catCode',sims_gb_cat_code UNION ALL 
				SELECT 'finalMark',final_marks UNION ALL
				SELECT 'catWeightage', category_weight UNION ALL
				SELECT 'maxMarks', final_max_marks
			) c (col,val)
		) d
		PIVOT
		(
			max(val) for col in(catCode_1,finalMark_1,catWeightage_1,maxMarks_1,
								catCode_2,finalMark_2,catWeightage_2,maxMarks_2,
								catCode_3,finalMark_3,catWeightage_3,maxMarks_3,
								catCode_4,finalMark_4,catWeightage_4,maxMarks_4,
								catCode_5,finalMark_5,catWeightage_5,maxMarks_5,
								catCode_6,finalMark_6,catWeightage_6,maxMarks_6,
								catCode_7,finalMark_7,catWeightage_7,maxMarks_7)
		) AS pvtTbl 
GROUP BY	sims_cur_code, sims_academic_year, sims_grade_code,sims_section_code,sims_gb_cat_assign_enroll_number,
		sims_gb_number, student_name, sims_subject_name_en, sims_term_desc_en, sims_gb_grade_scale,	sims_gb_term_code,
		--sims_level_code, sims_report_card_code,
		sims_report_card_term, sims_gb_subject_code, sims_display_order,
		sims_grade_name_en, sims_section_name_en, 
		sims_comment1_code, sims_user1_code, comment_1,
		sims_comment2_code, sims_user2_code, comment_2,
		sims_subject_type,sims_subject_type_name_en





GO
/****** Object:  View [dbo].[scoresheet_by_shrutika]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

CREATE VIEW [dbo].[scoresheet_by_shrutika]
As
select a.sims_student_enroll_number+'-'+a.[student_name] as sname,a.grade_name,a.section_name,d.sims_subject_name_en,f.sims_gb_term_code,
f.sims_gb_number,f.sims_gb_name,g.sims_gb_cat_code,g.sims_gb_cat_name,
h.sims_gb_cat_assign_number,h.sims_gb_cat_assign_name,h.sims_gb_cat_assign_type,
i.sims_attribute_group_name,j.sims_subject_attribute_name,
case 
when (h.sims_gb_cat_assign_type='A') then
(select coalesce([sims_gb_cat_assign_mark],'UM') from [sims].[sims_gradebook_category_assignment_student] where sims_academic_year=a.sims_academic_year 
and sims_cur_code=a.sims_student_cur_code and  sims_grade_code=a.sims_grade_code and sims_section_code=a.sims_section_code and sims_gb_number=f.sims_gb_number
and sims_gb_cat_code=g.sims_gb_cat_code and sims_gb_cat_assign_number=h.sims_gb_cat_assign_number and sims_gb_cat_assign_enroll_number=a.sims_student_enroll_number--and [sims_gb_cat_assign_status]='A'
)
when (h.sims_gb_cat_assign_type='S') then
(select coalesce([sims_gb_cat_assign_subject_attribute_mark],'UM') from [sims].[sims_report_card_subject_attribute_assignment_student] where sims_academic_year=a.sims_academic_year 
and sims_cur_code=a.sims_student_cur_code and  sims_grade_code=a.sims_grade_code and sims_section_code=a.sims_section_code and sims_gb_number=f.sims_gb_number
and sims_gb_cat_code=g.sims_gb_cat_code and sims_gb_cat_assign_number=h.sims_gb_cat_assign_number and [sims_subject_attribute_enroll_number]=a.sims_student_enroll_number
 and [sims_subject_attribute_group_code]=i.sims_attribute_group_code and [sims_subject_attribute_code]=j.sims_subject_attribute_code  and [sims_gb_cat_assign_subject_attribute_status]='A'
)
end as marks,
case 
when (h.sims_gb_cat_assign_type='A') then
(select isnull([sims_gb_cat_assign_final_grade],'UM') from [sims].[sims_gradebook_category_assignment_student] where sims_academic_year=a.sims_academic_year 
and sims_cur_code=a.sims_student_cur_code and  sims_grade_code=a.sims_grade_code and sims_section_code=a.sims_section_code and sims_gb_number=f.sims_gb_number
and sims_gb_cat_code=g.sims_gb_cat_code and sims_gb_cat_assign_number=h.sims_gb_cat_assign_number and sims_gb_cat_assign_enroll_number=a.sims_student_enroll_number --and [sims_gb_cat_assign_status]='A'
)
when (h.sims_gb_cat_assign_type='S') then
(select isnull([sims_gb_cat_assign_subject_attribute_final_grade],'UM') from [sims].[sims_report_card_subject_attribute_assignment_student] where sims_academic_year=a.sims_academic_year 
and sims_cur_code=a.sims_student_cur_code and  sims_grade_code=a.sims_grade_code and sims_section_code=a.sims_section_code and sims_gb_number=f.sims_gb_number
and sims_gb_cat_code=g.sims_gb_cat_code and sims_gb_cat_assign_number=h.sims_gb_cat_assign_number and [sims_subject_attribute_enroll_number]=a.sims_student_enroll_number
 and [sims_subject_attribute_group_code]=i.sims_attribute_group_code and [sims_subject_attribute_code]=j.sims_subject_attribute_code  and [sims_gb_cat_assign_subject_attribute_status]='A'
)
end as grade

    from sims.student_list AS a 
   -- INNER JOIN sims.student_list As b ON a.sims_enroll_number=b.[sims_student_enroll_number]
	INNER JOIN sims.sims_section_subject As c ON c.sims_cur_code=a.sims_student_cur_code
												and c.sims_academic_year=a.sims_academic_year
											    and c.sims_grade_code=a.sims_grade_code
												and c.sims_section_code=a.sims_section_code
	INNER JOIN sims.sims_subject As d ON d.sims_cur_code=c.sims_cur_code and d.sims_subject_code=c.sims_subject_code 
	INNER JOIN sims.sims_gradebook As f ON f.sims_academic_year=a.sims_academic_year
											and f.sims_cur_code=a.sims_student_cur_code
											and f.sims_grade_code=a.sims_grade_code
											and f.sims_section_code=a.sims_section_code 																					
	 INNER JOIN [sims].[sims_gradebook_category] As g ON g.sims_academic_year=a.sims_academic_year
											and g.sims_cur_code=a.sims_student_cur_code
											and g.sims_grade_code=a.sims_grade_code
											and g.sims_section_code=a.sims_section_code
											and g.sims_gb_number=f.sims_gb_number
    INNER JOIN [sims].[sims_gradebook_category_assignment] As h ON h.sims_academic_year=a.sims_academic_year
											and h.sims_cur_code=a.sims_student_cur_code
											and h.sims_grade_code=a.sims_grade_code
											and h.sims_section_code=a.sims_section_code
											and h.sims_gb_number=f.sims_gb_number
											and h.sims_gb_cat_code=g.sims_gb_cat_code
    Left Outer JOIN [sims].[sims_gradebook_category_assignment_student] As cas ON   cas.sims_academic_year=a.sims_academic_year 
											and cas.sims_cur_code=a.sims_student_cur_code
											and cas.sims_grade_code=a.sims_grade_code 
											and cas.sims_section_code=a.sims_section_code 
											and cas.sims_gb_number=f.sims_gb_number
											and cas.sims_gb_cat_code=g.sims_gb_cat_code 
											and cas.sims_gb_cat_assign_number=h.sims_gb_cat_assign_number 
											and cas.sims_gb_cat_assign_enroll_number=a.sims_student_enroll_number

	Left Outer Join [sims].[sims_report_card_subject_attribute_assignment_student]  As sas ON h.sims_academic_year=sas.sims_academic_year
											and h.sims_cur_code=sas.sims_cur_code
											and h.sims_grade_code=sas.sims_grade_code
											and h.sims_section_code=sas.sims_section_code
											and h.sims_gb_number=sas.sims_gb_number
											and h.sims_gb_cat_code=sas.sims_gb_cat_code
											and h.sims_gb_cat_assign_number=sas.sims_gb_cat_assign_number
										    and sas.sims_subject_attribute_enroll_number=a.sims_student_enroll_number
	Left Outer Join [sims].[sims_report_card_attribute_group] As i ON sas.sims_academic_year=i.sims_academic_year 
											and sas.sims_cur_code=i.sims_cur_code 
											and  sas.sims_grade_code=i.sims_grade_code 
										    and sas.sims_section_code=i.sims_section_code
											and sas.sims_subject_attribute_group_code=i.sims_attribute_group_code

   Left Outer Join [sims].[sims_report_card_subject_attribute] As j ON sas.sims_academic_year=j.sims_academic_year
											and sas.sims_cur_code=j.sims_cur_code
											and sas.sims_grade_code=j.sims_grade_code
											and sas.sims_section_code=j.sims_section_code
											and sas.sims_subject_attribute_group_code=j.sims_subject_attribute_group_code
											and sas.sims_subject_attribute_code=j.sims_subject_attribute_code
											and j.sims_subject_code=d.sims_subject_code



--where  a.sims_academic_year='2015' and --@acad_year and
--       a.sims_student_cur_code='01' and --@cur_code and 
--	   a.sims_grade_code='04' and --@grade_code and 
--	   a.sims_section_code='21' and --@section_code and 
--	  -- a.sims_allocation_status IN ('A','1') and
--	   d.sims_subject_code='01' and --@sub_code and
--	   f.sims_gb_term_code='01'--@term 
--	  and a.sims_student_enroll_number='S00346'







GO
/****** Object:  View [sims].[sims_fee_ledger]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE View [sims].[sims_fee_ledger]
AS
(SELECT        a.sims_fee_cur_code, a.sims_fee_academic_year, a.sims_fee_grade_code, a.sims_fee_section_code, a.sims_enroll_number, m.sims_sibling_parent_number, a.sims_fee_number, a.sims_fee_frequency, 
                         sims_fee_period, sims_expected_fee, 0 AS sims_paid_fee,0 AS dd_fee_amount_discounted, c.sims_fee_code_description, UPPER(SUBSTRING(dbo.GetMonthName(Substring(a.sims_fee_period, 16, 2)), 1, 3)) AS sims_fee_period_name, 
                         CASE WHEN a.sims_fee_frequency IN ('O', 'Y') THEN
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS ay
                               WHERE        ay.sims_cur_code = a.sims_fee_cur_code AND ay.sims_academic_year = a.sims_fee_academic_year) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, Substring(a.sims_fee_period, 
                         16, 2) + '-' + '01' + '-' + a.sims_fee_academic_year, 110) >
                             (SELECT        sims_academic_year_end_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, Substring(a.sims_fee_period, 16, 2) 
                         + '-' + '01' + '-' + CAST(a.sims_fee_academic_year - 1 AS nvarchar), 110) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, Substring(a.sims_fee_period, 16, 2) 
                         + '-' + '01' + '-' + a.sims_fee_academic_year, 110) <
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, Substring(a.sims_fee_period, 16, 2) 
                         + '-' + '1' + '-' + CAST(a.sims_fee_academic_year + 1 AS nvarchar), 110) ELSE CONVERT(date, Substring(a.sims_fee_period, 16, 2) + '-' + '1' + '-' + a.sims_fee_academic_year, 110) END AS sims_date, NULL 
                         AS doc_no,NULL AS dd_fee_payment_mode, NULL AS dd_fee_cheque_number, NULL AS dd_chq_status, NULL AS dd_fee_credit_card_code 
FROM            (SELECT        sims_fee_cur_code, sims_fee_academic_year, sims_fee_grade_code, sims_fee_section_code, sims_enroll_number, sims_fee_number, sims_fee_frequency, sims_fee_amount, sims_fee_period, 
                                                    sims_expected_fee
                          FROM            sims.sims_student_fee AS sf UNPIVOT (sims_expected_fee FOR sims_fee_period IN (sims_fee_period1, sims_fee_period2, sims_fee_period3, sims_fee_period4, sims_fee_period5, 
                                                    sims_fee_period6, sims_fee_period7, sims_fee_period8, sims_fee_period9, sims_fee_period10, sims_fee_period11, sims_fee_period12)) AS x
                          WHERE        sims_expected_fee != 0) AS a INNER JOIN
                         /*select * from */ sims.sims_section_fee AS b ON a.sims_fee_cur_code = b.sims_fee_cur_code AND a.sims_fee_academic_year = b.sims_fee_academic_year AND 
                         a.sims_fee_grade_code = b.sims_fee_grade_code AND a.sims_fee_section_code = b.sims_fee_section_code AND a.sims_fee_number = b.sims_fee_number INNER JOIN
                         sims.sims_fee_type AS c ON b.sims_fee_code = c.sims_fee_code INNER JOIN
                         sims.student_list AS m ON a.sims_fee_cur_code = m.sims_student_cur_code AND a.sims_fee_academic_year = m.sims_academic_year AND a.sims_fee_grade_code = m.sims_grade_code AND 
                         a.sims_fee_section_code = m.sims_section_code AND a.sims_enroll_number = m.sims_student_enroll_number)
UNION
(SELECT        a.sims_fee_cur_code, a.sims_fee_academic_year, a.sims_fee_grade_code, a.sims_fee_section_code, a.sims_enroll_number, m.sims_sibling_parent_number, a.sims_fee_number, a.sims_fee_frequency, 
                         sims_fee_period, 0 AS sims_expected_fee, sims_paid_fee,0 AS dd_fee_amount_discounted, c.sims_fee_code_description, UPPER(SUBSTRING(dbo.GetMonthName(LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1)), 1, 3)) AS sims_fee_period_name, 
                         CASE WHEN a.sims_fee_frequency IN ('O', 'Y') THEN
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS ay
                               WHERE        ay.sims_cur_code = a.sims_fee_cur_code AND ay.sims_academic_year = a.sims_fee_academic_year) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) + '-' + '01' + '-' + a.sims_fee_academic_year, 110) >
                             (SELECT        sims_academic_year_end_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) 
                         + '-' + '01' + '-' + CAST(a.sims_fee_academic_year - 1 AS nvarchar), 110) WHEN a.sims_fee_frequency IN ('M', 'T') AND CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1)
                         + '-' + '01' + '-' + a.sims_fee_academic_year, 110) <
                             (SELECT        sims_academic_year_start_date
                               FROM            sims.sims_academic_year AS e
                               WHERE        e.sims_cur_code = a.sims_fee_cur_code AND e.sims_academic_year = a.sims_fee_academic_year) THEN CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) 
                         + '-' + '1' + '-' + CAST(a.sims_fee_academic_year + 1 AS nvarchar), 110) ELSE CONVERT(date, LEFT(SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000),
           PATINDEX('%[^0-9.-]%', SUBSTRING(a.sims_fee_period, PATINDEX('%[0-9.-]%', a.sims_fee_period), 8000) + 'X') -1) + '-' + '1' + '-' + a.sims_fee_academic_year, 110) END AS sims_date, NULL 
                         AS doc_no,'Concession' AS dd_fee_payment_mode, NULL AS dd_fee_cheque_number, NULL AS dd_chq_status, NULL AS dd_fee_credit_card_code 
FROM            (SELECT        sims_fee_cur_code, sims_fee_academic_year, sims_fee_grade_code, sims_fee_section_code, sims_enroll_number, sims_fee_number, sims_fee_frequency, sims_fee_amount, sims_fee_period, 
                                                    sims_paid_fee
                          FROM            sims.sims_student_fee AS sf UNPIVOT (sims_paid_fee FOR sims_fee_period IN (sims_fee_period1_concession, sims_fee_period2_concession,sims_fee_period3_concession, 
						  sims_fee_period4_concession,
						  sims_fee_period5_concession,
						  sims_fee_period6_concession,
						  sims_fee_period7_concession,
						  sims_fee_period8_concession,
						  sims_fee_period9_concession,
						  sims_fee_period10_concession,
						  sims_fee_period11_concession,
						  sims_fee_period12_concession)) AS x
                          WHERE        sims_paid_fee != 0) AS a INNER JOIN
                         sims.sims_section_fee AS b ON a.sims_fee_cur_code = b.sims_fee_cur_code AND a.sims_fee_academic_year = b.sims_fee_academic_year AND 
                         a.sims_fee_grade_code = b.sims_fee_grade_code AND a.sims_fee_section_code = b.sims_fee_section_code AND a.sims_fee_number = b.sims_fee_number INNER JOIN
                         sims.sims_fee_type AS c ON b.sims_fee_code = c.sims_fee_code INNER JOIN
                         sims.student_list AS m ON a.sims_fee_cur_code = m.sims_student_cur_code AND a.sims_fee_academic_year = m.sims_academic_year AND a.sims_fee_grade_code = m.sims_grade_code AND 
                         a.sims_fee_section_code = m.sims_section_code AND a.sims_enroll_number = m.sims_student_enroll_number
)
UNION
(SELECT        h.sims_student_cur_code AS sims_fee_cur_code, h.sims_academic_year AS sims_fee_academic_year, h.sims_grade_code AS sims_fee_grade_code, h.sims_section_code AS sims_fee_section_code, 
                          g.enroll_number AS sims_enroll_number, h.sims_sibling_parent_number, f.dd_fee_number AS sims_fee_number, NULL AS sims_fee_frequency, NULL AS sims_fee_period, 0 AS sims_expected_fee, 
                          f.dd_fee_amount AS sims_paid_fee, 
						  f.dd_fee_amount_discounted,j.sims_fee_code_description, UPPER(SUBSTRING(dbo.GetMonthName(f.dd_fee_period_code), 1, 3)), g.doc_date AS sims_date, f.doc_no,
						  						  f.dd_fee_payment_mode, f.dd_fee_cheque_number,
						  (SELECT pc_discd FROM fins.fins_pdc_cheques WHERE pc_sl_ldgr_code = '04' AND
						  pc_sl_acno = h.sims_student_enroll_number AND 
						  pc_cheque_no = f.dd_fee_cheque_number AND 
						  pc_due_date = f.dd_fee_cheque_date AND
						  pc_bank_from = f.dd_fee_cheque_bank_code) AS dd_chq_status, f.dd_fee_credit_card_code
 FROM            sims.sims_fee_document_details AS f INNER JOIN
                          sims.sims_fee_document AS g ON f.doc_no = g.doc_no INNER JOIN
                          sims.student_list AS h ON g.enroll_number = h.sims_student_enroll_number INNER JOIN
                          sims.sims_section_fee AS i ON h.sims_student_cur_code = i.sims_fee_cur_code AND h.sims_academic_year = i.sims_fee_academic_year AND h.sims_grade_code = i.sims_fee_grade_code AND 
                          h.sims_section_code = i.sims_fee_section_code AND f.dd_fee_number = i.sims_fee_number INNER JOIN
                          sims.sims_fee_type AS j ON j.sims_fee_code = i.sims_fee_code)






GO
/****** Object:  View [sims].[sims_library_membership_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [sims].[sims_library_membership_list]
AS
SELECT        a.sims_library_user_number, CASE WHEN a.sims_enroll_number IS NOT NULL THEN a.sims_enroll_number WHEN a.sims_employee_code IS NOT NULL 
                         THEN a.sims_employee_code END AS sims_login_code, CASE WHEN a.sims_enroll_number IS NOT NULL THEN b.student_name WHEN a.sims_employee_code IS NOT NULL THEN ISNULL(e.em_first_name, '') 
                         + ' ' + ISNULL(e.em_middle_name, '') + ' ' + ISNULL(e.em_last_name, '') END AS sims_user_name, b.sims_student_cur_code, b.sims_academic_year, a.sims_library_user_group_type, 
                         c.comn_user_group_name, a.sims_library_membership_type, d.sims_library_privilege_name, b.sims_grade_code, b.grade_name, b.sims_section_code, b.section_name, a.sims_library_joining_date, 
                         a.sims_library_expiry_date, a.sims_library_renewed_date, a.sims_library_status
FROM            sims.sims_library_membership AS a LEFT OUTER JOIN
                         sims.student_list AS b ON a.sims_enroll_number = b.sims_student_enroll_number LEFT OUTER JOIN
                         comn.comn_user_group AS c ON a.sims_library_user_group_type = c.comn_user_group_code LEFT OUTER JOIN
                         sims.sims_library_privilege AS d ON a.sims_library_membership_type = d.sims_library_privilege_number LEFT OUTER JOIN
                         pays.pays_employee AS e ON a.sims_employee_code = e.em_login_code


GO
/****** Object:  View [sims].[student_attendance]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [sims].[student_attendance]
AS
SELECT        a.sims_cur_code, a.sims_academic_year, a.sims_grade_code, a.sims_section_code, a.sims_attendance_date, a.sims_enrollment_number, 
                         a.sims_attendance_day_code, a.sims_attendance_day_comment, a.sims_attendance_day_am_attendance_code, a.sims_attendance_day_am_comment, 
                         a.sims_attendance_day_pm_attendance_code, a.sims_attendance_day_pm_comment, a.sims_attendance_slot_1_subject_code, a.sims_attendance_slot_1, 
                         a.sims_attendance_day_teacher_code, a.sims_attendance_day_attendance_code, a.sims_attendance_day_attendance_comment, 
                         a.sims_attendance_slot_1_teacher_code, a.sims_attendance_slot_1_attendance_code, a.sims_attendance_slot_1_attendance_comment, 
                         a.sims_attendance_slot_2_subject_code, a.sims_attendance_slot_2, a.sims_attendance_slot_2_teacher_code, a.sims_attendance_slot_2_attendance_code, 
                         a.sims_attendance_slot_2_attendance_comment, a.sims_attendance_slot_3_subject_code, a.sims_attendance_slot_3, 
                         a.sims_attendance_slot_3_teacher_code, a.sims_attendance_slot_3_attendance_code, a.sims_attendance_slot_3_attendance_comment, 
                         a.sims_attendance_slot_4_subject_code, a.sims_attendance_slot_4, a.sims_attendance_slot_4_teacher_code, a.sims_attendance_slot_4_attendance_code, 
                         a.sims_attendance_slot_4_attendance_comment, a.sims_attendance_slot_5_subject_code, a.sims_attendance_slot_5, 
                         a.sims_attendance_slot_5_teacher_code, a.sims_attendance_slot_5_attendance_code, a.sims_attendance_slot_5_attendance_comment, 
                         a.sims_attendance_slot_6_subject_code, a.sims_attendance_slot_6, a.sims_attendance_slot_6_teacher_code, a.sims_attendance_slot_6_attendance_code, 
                         a.sims_attendance_slot_6_attendance_comment, a.sims_attendance_slot_7_subject_code, a.sims_attendance_slot_7, 
                         a.sims_attendance_slot_7_teacher_code, a.sims_attendance_slot_7_attendance_code, a.sims_attendance_slot_7_attendance_comment, 
                         a.sims_attendance_slot_8_subject_code, a.sims_attendance_slot_8, a.sims_attendance_slot_8_teacher_code, a.sims_attendance_slot_8_attendance_code, 
                         a.sims_attendance_slot_8_attendance_comment, a.sims_attendance_slot_9_subject_code, a.sims_attendance_slot_9, 
                         a.sims_attendance_slot_9_teacher_code, a.sims_attendance_slot_9_attendance_code, a.sims_attendance_slot_9_attendance_comment, 
                         a.sims_attendance_slot_10_subject_code, a.sims_attendance_slot_10, a.sims_attendance_slot_10_teacher_code, 
                         a.sims_attendance_slot_10_attendance_code, a.sims_attendance_slot_10_attendance_comment, a.sims_attendance_slot_11_subject_code, 
                         a.sims_attendance_slot_11, a.sims_attendance_slot_11_teacher_code, a.sims_attendance_slot_11_attendance_code, 
                         a.sims_attendance_slot_11_attendance_comment, a.sims_attendance_slot_12_subject_code, a.sims_attendance_slot_12, 
                         a.sims_attendance_slot_12_teacher_code, a.sims_attendance_slot_12_attendance_code, a.sims_attendance_slot_12_attendance_comment, 
                         a.sims_attendance_slot_13_subject_code, a.sims_attendance_slot_13, a.sims_attendance_slot_13_teacher_code, 
                         a.sims_attendance_slot_13_attendance_code, a.sims_attendance_slot_13_attendance_comment, a.sims_attendance_slot_14_subject_code, 
                         a.sims_attendance_slot_14, a.sims_attendance_slot_14_teacher_code, a.sims_attendance_slot_14_attendance_code, 
                         a.sims_attendance_slot_14_attendance_comment, a.sims_attendance_slot_15_subject_code, a.sims_attendance_slot_15, 
                         a.sims_attendance_slot_15_teacher_code, a.sims_attendance_slot_15_attendance_code, a.sims_attendance_slot_15_attendance_comment, 
                         CASE WHEN a.sims_attendance_day_attendance_code NOT IN
                             (SELECT        ac.sims_attendance_code
                               FROM            sims.sims_attendance_code AS ac
                               WHERE        ac.sims_cur_code = a.sims_cur_code) THEN
                             (SELECT        TOP (1) ce.sims_calendar_exception_description
                               FROM            sims.sims_calendar_exception AS ce
                               WHERE        a.sims_academic_year = ce.sims_academic_year AND a.sims_attendance_date BETWEEN ce.sims_from_date AND ce.sims_to_date) 
                         ELSE
                             (SELECT        ac.sims_attendance_description
                               FROM            sims.sims_attendance_code ac
                               WHERE        a.sims_cur_code = ac.sims_cur_code AND a.sims_attendance_day_attendance_code = ac.sims_attendance_code) 
                         END AS sims_day_attendance_code_description, CASE WHEN a.sims_attendance_day_attendance_code NOT IN
                             (SELECT        ac.sims_attendance_code
                               FROM            sims.sims_attendance_code AS ac
                               WHERE        ac.sims_cur_code = a.sims_cur_code) THEN
                             (SELECT        TOP (1) ce.sims_calendar_exception_color_code
                               FROM            sims.sims_calendar_exception AS ce
                               WHERE        a.sims_academic_year = ce.sims_academic_year AND a.sims_attendance_date BETWEEN ce.sims_from_date AND ce.sims_to_date) 
                         ELSE
                             (SELECT        ac.sims_attendance_color_code
                               FROM            sims.sims_attendance_code ac
                               WHERE        a.sims_cur_code = ac.sims_cur_code AND a.sims_attendance_day_attendance_code = ac.sims_attendance_code) 
                         END AS sims_attendance_color_code, b.student_name, b.grade_name, b.section_name, b.sims_student_academic_status, ISNULL(c.sims_bell_day_desc, 
                         N'Not Defined') AS sims_bell_day_desc, ISNULL(b.gender, N'Info Not available') AS gender
FROM            sims.sims_student_attendance AS a LEFT OUTER JOIN
                         sims.student_list AS b ON a.sims_cur_code = b.sims_student_cur_code AND a.sims_academic_year = b.sims_academic_year AND 
                         a.sims_grade_code = b.sims_grade_code AND a.sims_section_code = b.sims_section_code AND 
                         a.sims_enrollment_number = b.sims_student_enroll_number LEFT OUTER JOIN
                         sims.sims_bell_day AS c ON a.sims_academic_year = c.sims_bell_academic_year AND a.sims_attendance_day_code = c.sims_bell_day_code AND 
                         c.sims_bell_code =
                             (SELECT        sims_bell_code
                               FROM            sims.sims_bell_section AS bs
                               WHERE        (sims_bell_academic_year = a.sims_academic_year) AND (sims_bell_grade_code = a.sims_grade_code) AND 
                                                         (sims_bell_section_code = a.sims_section_code))


GO
/****** Object:  View [sims].[sims_admission_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--select * from [sims].[sims_admission]
	



CREATE VIEW [sims].[sims_admission_list]
AS
SELECT	--Admission no details--
		a.sims_admission_number, a.sims_admission_application_number, a.sims_admission_date, 
		a.sims_admission_pros_application_number, a.sims_admission_pros_number,n.sims_student_enroll_number,
		--Admission School Details--
		a.sims_admission_school_code, b.lic_school_name, 
		a.sims_admission_cur_code, c.sims_cur_short_name_en, 
		a.[sims_admission_commencement_date] as sims_admission_tentative_joining_date,
		
		
		 a.sims_admission_academic_year,
		IIF(a.sims_admission_status='C',o.sims_grade_code,a.sims_admission_grade_code) as sims_admission_grade_code, 
		(select sims_grade_name_en from sims.sims_grade where sims_cur_code+sims_academic_year+sims_grade_code=IIF(a.sims_admission_status='C',o.sims_cur_code+o.sims_academic_year+o.sims_grade_code,a.sims_admission_cur_code+a.sims_admission_academic_year+a.sims_admission_grade_code)) as sims_grade_name_en, 
		IIF(a.sims_admission_status='C',o.sims_section_code,a.sims_admission_section_code) as sims_admission_section_code, 
	    (select sims_section_name_en from sims.sims_section where sims_cur_code+sims_academic_year+sims_grade_code+sims_section_code=IIF(a.sims_admission_status='C',o.sims_cur_code+o.sims_academic_year+o.sims_grade_code+o.sims_section_code,a.sims_admission_cur_code+a.sims_admission_academic_year+a.sims_admission_grade_code+a.sims_admission_section_code)) as section_name,  
		a.sims_admission_term_code, f.sims_term_desc_en, 
		--Personal Details of student--
		CONCAT(a.sims_admission_passport_first_name_en, ' ',a.sims_admission_passport_middle_name_en, ' ',a.sims_admission_passport_last_name_en) AS student_name_en,
		CONCAT(a.sims_admission_passport_first_name_ot,' ', a.sims_admission_passport_middle_name_ot, ' ',a.sims_admission_passport_last_name_ot) AS student_name_ot,
		a.sims_admission_dob, sims_admission_commencement_date, 
		a.sims_admission_gender, (SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
								WHERE sims_mod_code = '003' AND sims_appl_form_field = 'Gender' AND 
								sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_gender) AS gender, 
		a.sims_admission_birth_country_code, g.sims_country_name_en,
		a.sims_admission_nationality_code, h.sims_nationality_name_en,
		a.sims_admission_ethnicity_code, i.sims_ethnicity_name_en,
		a.sims_admission_religion_code, j.sims_religion_name_en, 
		--Passport Details--
		a.sims_admission_passport_number, a.sims_admission_passport_issue_date, a.sims_admission_passport_expiry_date, 
		a.sims_admission_passport_issuing_authority, sims_admission_passport_issue_place, 
		--Visa/National ID Information--
		a.sims_admission_visa_type, (SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
									WHERE sims_mod_code = '004' AND 
									sims_appl_form_field = 'Visa Type' AND 
									sims_appl_code = 'Per099' AND 
									sims_appl_parameter = a.sims_admission_visa_type ) AS visa_type,a.sims_admission_visa_number,
		a.sims_admission_visa_issuing_place, a.sims_admission_visa_issuing_authority, a.sims_admission_visa_issue_date,
		a.sims_admission_visa_expiry_date, a.sims_admission_national_id, a.sims_admission_national_id_issue_date, 
		a.sims_admission_national_id_expiry_date,
		--Sibling Information--
		a.sims_admission_sibling_status, a.sims_admission_parent_id, a.sims_admission_sibling_enroll_number, 
		a.sims_admission_sibling_name, a.sims_admission_sibling_dob, sims_admission_sibling_school_code, 
		k.lic_school_name AS lic_sibling_school_name,
		(select grade_name from sims.student_list where sims_student_enroll_number=a.sims_admission_sibling_enroll_number and sims_academic_year=(select sims_academic_year from sims.sims_academic_year where sims_academic_year_status='C')) as sibling_grade_name,
		(select section_name from sims.student_list where sims_student_enroll_number=a.sims_admission_sibling_enroll_number and sims_academic_year=(select sims_academic_year from sims.sims_academic_year where sims_academic_year_status='C')) as sibling_section_name,

		--Father Employee of school--
		a.sims_admission_employee_code, 
		--Language details--
		a.sims_admission_main_language_code, l.sims_language_name_en, 
		a.sims_admission_main_language_m, a.sims_admission_main_language_r, 
		a.sims_admission_main_language_s, a.sims_admission_main_language_w, 
		--Parent Details--
	CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_father_salutation_code),' ',a.sims_admission_father_first_name,' ',a.sims_admission_father_middle_name,' ',a.sims_admission_father_last_name) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_father_salutation_code),' ',m.sims_parent_father_first_name,' ',m.sims_parent_father_middle_name, ' ',m.sims_parent_father_last_name) END AS father_name,
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_mother_salutation_code),' ',a.sims_admission_mother_first_name,' ',a.sims_admission_mother_middle_name,' ',a.sims_admission_mother_last_name) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_mother_salutation_code),' ',m.sims_parent_mother_first_name,' ',m.sims_parent_mother_middle_name, ' ',m.sims_parent_mother_last_name) END AS mother_name,
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_guardian_salutation_code),' ',a.sims_admission_guardian_first_name,' ',a.sims_admission_guardian_middle_name,' ',a.sims_admission_guardian_last_name) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_guardian_salutation_code),' ',m.sims_parent_guardian_first_name,' ',m.sims_parent_guardian_middle_name, ' ',m.sims_parent_guardian_last_name) END AS guardian_name, 
		a.sims_admission_primary_contact_pref, a.sims_admission_fee_payment_contact_pref, 
		a.sims_admission_transport_status, a.sims_admission_transport_desc, 
		--CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_father_mobile ELSE m.sims_parent_father_mobile END 
		a.sims_admission_father_mobile AS father_mobile, 
		--CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_mother_mobile ELSE m.sims_parent_mother_mobile END 
		a.sims_admission_mother_mobile AS mother_mobile, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_guardian_mobile ELSE m.sims_parent_guardian_mobile END AS guardian_mobile, 
		--CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_father_phone ELSE m.sims_parent_father_phone END 
		a.sims_admission_father_phone AS father_phone, 
		--CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_mother_phone ELSE m.sims_parent_mother_phone END 
		a.sims_admission_mother_phone AS mother_phone, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_guardian_phone ELSE m.sims_parent_guardian_phone END AS guardian_phone, 

		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_father_email ELSE m.sims_parent_father_email END AS father_email, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_mother_email ELSE m.sims_parent_mother_email END AS mother_email, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_guardian_email ELSE m.sims_parent_guardian_email END AS guardian_email, 


		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN 
		CONCAT(a.sims_admission_father_appartment_number,' ',a.sims_admission_father_building_number,' ',a.sims_admission_father_street_number,' ',
		a.sims_admission_father_area_number,' ',a.sims_admission_father_city,' ',a.sims_admission_father_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_admission_father_country_code))
		ELSE CONCAT(m.sims_parent_father_appartment_number,' ',m.sims_parent_father_building_number,' ',m.sims_parent_father_street_number,' ',
		m.sims_parent_father_area_number,' ',m.sims_parent_father_city,' ',m.sims_parent_father_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_father_country_code)) END AS father_address,
				CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN 
		CONCAT(a.sims_admission_mother_appartment_number,' ',a.sims_admission_mother_building_number,' ',a.sims_admission_mother_street_number,' ',
		a.sims_admission_mother_area_number,' ',a.sims_admission_mother_city,' ',a.sims_admission_mother_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_admission_mother_country_code))
		ELSE CONCAT(m.sims_parent_mother_appartment_number,' ',m.sims_parent_mother_building_number,' ',m.sims_parent_mother_street_number,' ',
		m.sims_parent_mother_area_number,' ',m.sims_parent_mother_city,' ',m.sims_parent_mother_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_mother_country_code)) END AS mother_address,
				CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN 
		CONCAT(a.sims_admission_guardian_appartment_number,' ',a.sims_admission_guardian_building_number,' ',a.sims_admission_guardian_street_number,' ',
		a.sims_admission_guardian_area_number,' ',a.sims_admission_guardian_city,' ',a.sims_admission_guardian_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_admission_guardian_country_code))
		ELSE CONCAT(m.sims_parent_guardian_appartment_number,' ',m.sims_parent_guardian_building_number,' ',m.sims_parent_guardian_street_number,' ',
		m.sims_parent_guardian_area_number,' ',m.sims_parent_guardian_city,' ',m.sims_parent_guardian_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_guardian_country_code)) END AS guardian_address, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_father_po_box ELSE m.sims_parent_father_po_box END AS father_po_box, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_mother_po_box ELSE m.sims_parent_mother_po_box END AS mother_po_box, 
		CASE WHEN NULLIF(a.sims_admission_parent_id,'') IS NULL THEN a.sims_admission_guardian_po_box ELSE m.sims_parent_guardian_po_box END AS guardian_po_box,
		--Previous School Details--
		a.sims_admission_current_school_name, a.sims_admission_current_school_enroll_number, 
		a.sims_admission_current_school_grade, a.sims_admission_current_school_cur, 
		a.sims_admission_current_school_from_date, a.sims_admission_current_school_to_date, 
		a.sims_admission_current_school_language, a.sims_admission_current_school_phone, 
		a.sims_admission_current_school_address, a.sims_admission_current_school_city,
		(SELECT sc.sims_country_name_en FROM sims.sims_country AS sc WHERE sc.sims_country_code = a.sims_admission_current_school_country_code) AS sims_admission_current_school_country_name, 
		--Legal Custoday Details--
		a.sims_admission_legal_custody, 
		--Health Details--
		a.sims_admission_health_card_number, a.sims_admission_health_card_issue_date, a.sims_admission_health_card_expiry_date, 
		a.sims_admission_blood_group_code, (select sims_appl_form_field_value1 from sims.sims_parameter where sims_mod_code = '003'  AND sims_appl_code = 'Sim010' AND
											sims_appl_form_field = 'BloodGroup' AND sims_appl_parameter = a.sims_admission_blood_group_code) AS blood_group, 
		a.sims_admission_health_restriction_status,a.sims_admission_health_restriction_desc, 
		a.sims_admission_disability_status, a.sims_admission_disability_desc, 
		a.sims_admission_medication_status, a.sims_admission_medication_desc, 
		a.sims_admission_health_other_status, a.sims_admission_health_other_desc, 
		a.sims_admission_health_hearing_status, a.sims_admission_health_hearing_desc, 
		a.sims_admission_health_vision_status, a.sims_admission_health_vision_desc, 
		--Other Details--
		a.sims_admission_gifted_status, a.sims_admission_gifted_desc, 
		a.sims_admission_music_status, a.sims_admission_music_desc, 
		a.sims_admission_sports_status, a.sims_admission_sports_desc, 
		a.sims_admission_language_support_status, a.sims_admission_language_support_desc,
		--Status--
		a.sims_admission_status,(SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
								 WHERE sims_mod_code = '003' AND sims_appl_code= 'Sim010' AND 
										sims_appl_form_field = 'Admission Status' 
										AND sims_appl_parameter = a.sims_admission_status)	AS sims_admission_status_value, 
		a.sims_student_health_respiratory_status,a.sims_student_health_respiratory_desc,a.sims_student_health_hay_fever_status,
		a.sims_student_health_hay_fever_desc, a.sims_student_health_epilepsy_status, a.sims_student_health_epilepsy_desc,
		a.sims_student_health_skin_status, a.sims_student_health_skin_desc, a.sims_student_health_diabetes_status, 
		a.sims_student_health_diabetes_desc,a.sims_student_health_surgery_status, a.sims_student_health_surgery_desc, 
		a.sims_student_attribute3, a.sims_student_attribute4, a.sims_admission_father_national_id, a.sims_admission_mother_national_id,
		a.sims_admission_guardian_national_id, a.sims_student_organizational_skills, a.sims_student_social_skills, a.sims_student_academic_standard, 
		a.sims_student_special_education, a.sims_student_attribute5 , a.sims_student_attribute6, a.sims_student_attribute7,
		a.sims_student_attribute8, a.sims_student_attribute9, a.sims_student_attribute10, a.sims_student_attribute11,
		a.sims_student_attribute12, a.sims_student_attribute1,a.sims_student_attribute2,
		ap.sims_admission_parent_REG_id,ap.sims_admission_parent_email as parent_login

FROM	sims.sims_admission AS a LEFT OUTER JOIN mogra.mogra_license_details AS b ON 
		a.sims_admission_school_code = b.lic_school_code LEFT OUTER JOIN sims.sims_cur AS c ON 
		a.sims_admission_cur_code = c.sims_cur_code LEFT OUTER JOIN sims.sims_grade AS d ON 
		a.sims_admission_cur_code = d.sims_cur_code AND a.sims_admission_academic_year = d.sims_academic_year AND
		a.sims_admission_grade_code = d.sims_grade_code LEFT OUTER JOIN sims.sims_section AS e ON 
		a.sims_admission_cur_code = e.sims_cur_code AND a.sims_admission_academic_year = e.sims_academic_year AND 
		a.sims_admission_grade_code = e.sims_grade_code AND 
		a.sims_admission_section_code = e.sims_section_code	LEFT OUTER JOIN sims.sims_term AS f ON 
		a.sims_admission_cur_code = f.sims_cur_code AND a.sims_admission_academic_year = f.sims_academic_year AND 
		a.sims_admission_term_code = f.sims_term_code LEFT OUTER JOIN sims.sims_country AS g ON 
		a.sims_admission_birth_country_code = g.sims_country_code LEFT OUTER JOIN sims.sims_nationality AS h ON 
		a.sims_admission_nationality_code = h.sims_nationality_code LEFT OUTER JOIN sims.sims_ethnicity AS i ON 
		a.sims_admission_ethnicity_code = i.sims_ethnicity_code LEFT OUTER JOIN sims.sims_religion AS j ON 
		a.sims_admission_religion_code = j.sims_religion_code LEFT OUTER JOIN mogra.mogra_license_details AS k ON 
		a.sims_admission_sibling_school_code = k.lic_school_code LEFT OUTER JOIN sims.sims_language AS l ON
		a.sims_admission_main_language_code = l.sims_language_code LEFT OUTER JOIN sims.sims_parent AS m ON 
		a.sims_admission_parent_id = m.sims_parent_number left outer join sims.sims_student as n on
		a.sims_admission_number=n.[sims_student_admission_number] left outer join  sims.sims_student_section as o on
		n.sims_student_enroll_number=o.sims_enroll_number and o.sims_cur_code=n.sims_student_cur_code and 
		o.sims_academic_year=a.sims_admission_academic_year and
		o.sims_allocation_status in ('1','A') left outer join sims.sims_admission_parent ap on
		a.sims_admission_parent_REG_id = ap.sims_admission_parent_REG_id

		






GO
/****** Object:  View [pays].[pays_employee_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [pays].[pays_employee_list]
AS
SELECT        em_number,em_login_code, em_dept_code, em_dept_effect_from, em_desg_code, 
							 (SELECT dg_desc 
							  FROM pays.pays_designation WHERE dg_code = a.em_desg_code) AS em_designation,
							 em_dest_code,
                             (SELECT        ds_name
                               FROM            pays.pays_destination
                               WHERE        (ds_code = a.em_dest_code) AND (ds_nation_code = a.em_nation_code)) AS destination_name, em_nation_code,
                             (SELECT        sims_country_name_en
                               FROM            sims.sims_country
                               WHERE        (sims_country_code = a.em_nation_code)) AS emp_country, em_grade_code,
                             (SELECT        gr_desc
                               FROM            pays.pays_grade
                               WHERE        (gr_code = a.em_grade_code)) AS grade_name, em_grade_effect_from, em_staff_type,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Staff Type') AND 
                                                         (sims_appl_parameter = a.em_staff_type)) AS emp_staff_type, em_salutation, 
                         isnull(em_first_name ,' ')+ ' ' + isnull(em_middle_name ,'')+ ' ' + isnull(em_last_name,'') AS emp_name, em_name_ot, em_date_of_birth, em_sex,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_2
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Gender') AND (sims_appl_parameter = a.em_sex)) 
                         AS em_gender,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_1
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Marital Status') AND 
                                                         (sims_appl_parameter = a.em_marital_status)) AS emp_marital_status, em_religion_code,
                             (SELECT        sims_religion_name_en
                               FROM            sims.sims_religion
                               WHERE        (sims_religion_code = a.em_religion_code)) AS religion_code, em_ethnicity_code,
                             (SELECT        sims_ethnicity_name_en
                               FROM            sims.sims_ethnicity
                               WHERE        (sims_ethnicity_code = a.em_ethnicity_code)) AS emp_ethnicity, em_appartment_number, em_building_number, em_street_number, 
                         em_area_number, em_summary_address, em_summary_address_local_language, em_country_code,
                             (SELECT        sims_country_name_en
                               FROM            sims.sims_country AS sims_country_1
                               WHERE        (sims_country_code = a.em_country_code)) AS em_address_country, em_state,
                             (SELECT        sims_state_name_en
                               FROM            sims.sims_state
                               WHERE        (sims_country_code = a.em_country_code) AND (sims_state_code = a.em_state)) AS em_address_state, em_city,
                             (SELECT        sims_city_name_en
                               FROM            sims.sims_city
                               WHERE        (sims_state_code = a.em_state) AND (sims_city_code = a.em_city)) AS em_add_city, em_phone, em_mobile, em_email, em_fax, em_po_box, 
                         em_img, em_emergency_contact_number1, em_emergency_contact_name1, em_emergency_contact_number2, em_emergency_contact_name2, 
                         em_date_of_join,
                             (SELECT        sims_financial_year
                               FROM            sims.sims_financial_year
                               WHERE        (a.em_date_of_join BETWEEN sims_financial_year_start_date AND sims_financial_year_end_date)) AS year_of_join, em_gpf_ac_no, 
                         em_gosi_start_date, em_pan_no, en_labour_card_no, em_status, em_service_status,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_3
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Service Status') AND 
                                                         (sims_appl_parameter = a.em_service_status)) AS service_status,
						em_passport_number, em_passport_issue_date, em_passport_expiry_date, 
						em_company_code, em_bank_ac_no,em_bank_code, 
						em_bank_cash_tag

--select *

FROM            pays.pays_employee AS a










GO
/****** Object:  View [pays].[pays_paysheet_details]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [pays].[pays_paysheet_details]
AS
SELECT        a.pe_company_code, a.pe_number, a.pe_year_month, SUBSTRING(CONVERT(nvarchar, a.pe_year_month), 1, 4) AS paysheet_year, dbo.GetMonthName(RIGHT('00' + SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 5, 2), 2)) AS paysheet_month_name, 
						 RIGHT('00' + SUBSTRING(CONVERT(nvarchar, a.pe_year_month), 5, 2), 2) AS paysheet_month, b.em_bank_cash_tag, b.em_bank_ac_no, b.em_desg_code, b.destination_name, b.em_grade_code, b.grade_name, b.emp_name, c.sd_pay_code, e.cp_desc, 
                         e.cp_change_desc, d.pc_earn_dedn_tag, c.sd_amount, e.cp_display_order,b.em_bank_code,
						 (select top 1 pb_bank_name from fins.fins_bank where pb_bank_code=b.em_bank_code and pb_comp_code=a.pe_company_code) as bank
						 ,b.em_login_code,f.em_iban_no,b.em_designation,f.em_service_status,
						 DATEADD(month, (RIGHT('00' + SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 5, 2), 2))-1,DATEADD(year,(SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 1, 4))-1900,0)) as pay_start_date,
						 DATEADD(day,-1,DATEADD(month,convert(int,RIGHT('00' + SUBSTRING(CONVERT(nvarchar, a.pe_year_month), 5, 2), 2)),DATEADD(year,convert(int,SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 1, 4))-1900,0)))  as pays_end_date,
						 day(DATEADD(day,-1,DATEADD(month,convert(int,RIGHT('00' + SUBSTRING(CONVERT(nvarchar, a.pe_year_month), 5, 2), 2)),DATEADD(year,convert(int,SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 1, 4))-1900,0)))) as no_of_working_days,
						 (select count(att_absent_flag) from pays.pays_attendance_daily where att_emp_id= f.em_login_code and  att_date between  DATEADD(month, (RIGHT('00' + SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 5, 2), 2))-1,DATEADD(year,(SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 1, 4))-1900,0)) and  DATEADD(day,-1,DATEADD(month,convert(int,RIGHT('00' + SUBSTRING(CONVERT(nvarchar, a.pe_year_month), 5, 2), 2)),DATEADD(year,convert(int,SUBSTRING(CONVERT(nvarchar,a.pe_year_month), 1, 4))-1900,0))) and att_absent_flag='A') as 'Days_of_Unpaid_Leave'
						,cast(b.em_date_of_join as date) as em_date_of_join
						,fd.codp_dept_name
						,(select sims_appl_form_field_value1 from sims.sims_parameter where sims_appl_code='Per099' and sims_appl_form_field='Staff Type'
						and sims_appl_parameter=f.em_staff_type) as em_staff_type
FROM            pays.pays_payroll_empl AS a INNER JOIN
						 pays.pays_employee As f ON a.pe_company_code = f.em_company_code AND a.pe_number = f.em_number INNER JOIN
                         pays.pays_employee_list AS b ON a.pe_company_code = b.em_company_code AND a.pe_number = b.em_number INNER JOIN
                         pays.pays_salary_details AS c ON a.pe_company_code = c.sd_company_code AND a.pe_number = c.sd_number and a.pe_year_month=c.sd_year_month 
						 INNER JOIN
                         pays.pays_pay_code AS d ON a.pe_company_code = d.pc_company_code AND c.sd_pay_code = d.pc_code 
						 INNER JOIN
                         pays.pays_common_pay_code AS e ON d.pc_code = e.cp_code
						 left outer JOIN fins.fins_departments  fd on f.em_dept_code=fd.codp_dept_no and 
						 fd.codp_year in (select sims_financial_year from sims.sims_financial_year where sims_financial_year_status='C')
						 





--select * from              pays.pays_employee_list







GO
/****** Object:  View [sims].[sims_library_transaction_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [sims].[sims_library_transaction_list]
AS
SELECT        sims.sims_library_transaction_detail.sims_library_transaction_number, sims.sims_library_transaction_detail.sims_library_transaction_line_number, 
                         sims.sims_library_transaction_detail.sims_library_item_number, sims.sims_library_transaction_detail.sims_library_item_expected_return_date, 
                         sims.sims_library_transaction_detail.sims_library_item_reserve_release_date, sims.sims_library_transaction_detail.sims_library_item_rate, sims.sims_library_transaction_detail.sims_library_item_line_total, 
                         sims.sims_library_transaction_detail.sims_library_item_transaction_status, sims.sims_library_transaction.sims_library_transaction_date, sims.sims_library_transaction.sims_library_transaction_remarks, 
                         sims.sims_library_transaction.sims_library_transaction_type,sims.sims_library_transaction_detail.sims_library_item_actual_return_date,
                             (SELECT sims_library_transaction_type_name FROM sims.sims_library_transaction_type 
							 WHERE sims_library_transaction_type_code = sims_library_transaction_type) AS trns_type_desc, sims.sims_library_transaction.sims_library_transaction_total, 
                         sims.sims_library_transaction.sims_library_transaction_user_code,
                             (SELECT        CAST(isnull(sims_library_user_first_name,'') + ' ' + isnull(sims_library_user_middle_name,'') + ' ' + isnull(sims_library_user_last_name,'') AS nvarchar(50)) AS Expr1
                               FROM            sims.sims_library_membership AS lm
                               WHERE        (sims_library_user_number = sims.sims_library_transaction.sims_library_transaction_user_code)) AS member_name, sims.sims_library_item_master.sims_library_item_name, 
                         sims.sims_library_item_master.sims_library_item_desc, sims.sims_library_item_master.sims_library_item_accession_number, sims.sims_library_item_master.sims_library_item_cur_code, 
                         sims.sims_library_item_master.sims_library_item_cur_level_code, sims.sims_library_item_master.sims_library_item_grade_code, sims.sims_library_item_master.sims_library_item_category,
                             (SELECT        sims_library_category_name
                               FROM            sims.sims_library_category
                               WHERE        (sims_library_category_code = sims.sims_library_item_master.sims_library_item_category)) AS cat_name, sims.sims_library_item_master.sims_library_item_subcategory,
                             (SELECT        sims_library_subcategory_name
                               FROM            sims.sims_library_subcategory
                               WHERE        (sims_library_subcategory_code = sims.sims_library_item_master.sims_library_item_subcategory)) AS subcat_name, sims.sims_library_item_master.sims_library_item_catalogue_code, 
                         sims.sims_library_item_master.sims_library_item_language_code, sims.sims_library_membership_list.sims_login_code, 
						 sims.sims_library_membership_list.sims_user_name, sims.sims_library_membership_list.sims_library_user_group_type,
						 CASE WHEN sims_library_item_transaction_status = 'I' THEN 'Issue' 
							  WHEN sims_library_item_transaction_status = 'R' THEN 'Return' 
							  WHEN sims_library_item_transaction_status = 'X' THEN 'Re-issue' ELSE 'Reserve' END AS transaction_status, 
						sims.sims_library_item_master.sims_library_isbn_number,
						sims.sims_library_membership_list.sims_grade_code ,sims.sims_library_membership_list.grade_name,sims.sims_library_membership_list.sims_section_code,sims.sims_library_membership_list.section_name	  						 
						, sims.sims_library_transaction.sims_library_transaction_status,sims_library_item_issue_date
FROM            sims.sims_library_transaction_detail LEFT OUTER JOIN
                         sims.sims_library_transaction ON sims.sims_library_transaction.sims_library_transaction_number = sims.sims_library_transaction_detail.sims_library_transaction_number LEFT OUTER JOIN
                         sims.sims_library_item_master ON sims.sims_library_item_master.sims_library_item_number = sims.sims_library_transaction_detail.sims_library_item_number LEFT OUTER JOIN
                         sims.sims_library_membership_list ON sims.sims_library_membership_list.sims_library_user_number = sims.sims_library_transaction.sims_library_transaction_user_code
					--	  AND sims.sims_library_membership_list.sims_academic_year=(select sims_academic_year from sims.sims_academic_year where sims_cur_code=sims.sims_library_item_master.sims_library_item_cur_code and sims_academic_year_status='C')


					













GO
/****** Object:  View [comn].[comn_user_audit_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [comn].[comn_user_audit_list]
AS
SELECT        a.comn_audit_user_code, b.comn_user_name AS comn_user_login_code, b.comn_user_group_code, e.comn_user_group_name, CASE WHEN b.comn_user_group_code = '03' THEN
                             (SELECT        CONCAT(em_first_name, ' ', em_middle_name, ' ', em_last_name)
                               FROM            pays.pays_employee
                               WHERE        em_login_code = comn_user_name) WHEN b.comn_user_group_code = '04' THEN
                             (SELECT        CONCAT(sims_student_passport_first_name_en, ' ', sims_student_passport_middle_name_en, ' ', sims_student_passport_last_name_en)
                               FROM            sims.sims_student
                               WHERE        sims_student_enroll_number = comn_user_name) WHEN b.comn_user_group_code = '04' THEN
                             (SELECT        CONCAT(sims_parent_father_first_name, ' ', sims_parent_father_middle_name, ' ', sims_parent_father_last_name)
                               FROM            sims.sims_parent
                               WHERE        sims_parent_number = comn_user_name) ELSE b.comn_user_alias END AS comn_user_name, a.comn_audit_user_appl_code, c.comn_appl_name_en, c.comn_appl_mod_code, 
                         d.comn_mod_name_en, a.comn_audit_ip, a.comn_audit_dns, a.comn_audit_start_time, a.comn_audit_end_time, a.comn_audit_remark
FROM            comn.comn_user_audit AS a INNER JOIN
                         comn.comn_user AS b ON a.comn_audit_user_code = b.comn_user_code INNER JOIN
                         comn.comn_application AS c ON a.comn_audit_user_appl_code = c.comn_appl_code INNER JOIN
                         comn.comn_module AS d ON c.comn_appl_mod_code = d.comn_mod_code INNER JOIN
                         comn.comn_user_group AS e ON b.comn_user_group_code = e.comn_user_group_code


GO
/****** Object:  View [comn].[comn_user_detail]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [comn].[comn_user_detail]
AS
select comn_user_name,comn_user_password,COALESCE(NULLIF(comn_user_email,''),'-') comn_user_email, case comn_user_group_code
When '01' Then comn.comn_user.comn_user_alias
When '03' Then (select COALESCE(NULLIF(em_first_name,''),'-') from pays.pays_employee where em_login_code =comn_user_name) 
When '04' Then (select COALESCE(NULLIF(sims_student_passport_first_name_en,''),'-') from sims.sims_student where sims_student_enroll_number =comn_user_enroll_number) 
When '05' Then (select COALESCE(NULLIF(sims_parent_father_first_name,''),'-') from sims.sims_parent where sims_parent_login_code =comn_user_name) 

END sims_student_passport_first_name_en,
case comn_user_group_code
When '01' Then '-'
When '03' Then (select COALESCE(NULLIF(em_last_name,''),'-') from pays.pays_employee where em_login_code =comn_user_name)
When '04' Then (select COALESCE(NULLIF(sims_student_passport_last_name_en,''),'-') from sims.sims_student where sims_student_enroll_number =comn_user_enroll_number) 
When '05' Then (select COALESCE(NULLIF(sims_parent_father_last_name,''),'-') from sims.sims_parent where sims_parent_login_code =comn_user_name)

END sims_student_passport_last_name_en

from comn.comn_user



GO
/****** Object:  View [dbo].[pays_employee_report]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

-- View

CREATE VIEW [dbo].[pays_employee_report]
AS
SELECT        em_login_code, em_number, em_company_code,
                             (SELECT        TOP (1) co_desc
                               FROM            pays.pays_company
                               WHERE        (co_company_code = pays.pays_employee.em_company_code)) AS company_name, em_dept_code,
                             (SELECT        TOP (1) codp_dept_name
                               FROM            fins.fins_departments
                               WHERE        (codp_comp_code = pays.pays_employee.em_company_code) AND (codp_dept_no = pays.pays_employee.em_dept_code)) AS dept_name, em_dept_effect_from, em_desg_code,
                             (SELECT        TOP (1) dg_desc
                               FROM            pays.pays_designation
                               WHERE        (dg_code = pays.pays_employee.em_desg_code) AND (dg_company_code = pays.pays_employee.em_company_code)) AS designation_name, em_dest_code,
                             (SELECT        TOP (1) ds_name
                               FROM            pays.pays_destination
                               WHERE        (ds_nation_code = pays.pays_employee.em_country_code) AND (ds_code = pays.pays_employee.em_dest_code)) AS destination_name, em_nation_code,
                             (SELECT        TOP (1) sims_nationality_name_en
                               FROM            sims.sims_nationality
                               WHERE        (sims_nationality_code = pays.pays_employee.em_nation_code)) AS nationality_name, em_grade_code,
                             (SELECT        TOP (1) eg_grade_desc
                               FROM            pays.pays_employee_grade
                               WHERE        (eg_company_code = pays.pays_employee.em_company_code) AND (eg_grade_code = pays.pays_employee.em_grade_code)) AS grade_name, em_grade_effect_from, em_staff_type, 
                         em_salutation, em_first_name, em_middle_name, em_last_name, em_salutation + '.' + em_first_name + ' ' + em_middle_name + ' ' + em_last_name AS 'em_full_name', em_family_name, em_name_ot, 
                         em_date_of_birth, em_sex, em_marital_status, em_religion_code,
                             (SELECT        TOP (1) sims_religion_name_en
                               FROM            sims.sims_religion
                               WHERE        (sims_religion_code = pays.pays_employee.em_religion_code)) AS religion_name, em_ethnicity_code,
                             (SELECT        TOP (1) sims_ethnicity_name_en
                               FROM            sims.sims_ethnicity
                               WHERE        (sims_ethnicity_code = pays.pays_employee.em_ethnicity_code)) AS ethnicity_name, em_appartment_number, em_building_number, em_street_number, em_area_number, em_summary_address, 
                         em_summary_address_local_language, em_city,
                             (SELECT        TOP (1) sims_city_name_en
                               FROM            sims.sims_city
                               WHERE        (sims_city_code = pays.pays_employee.em_city) AND (sims_state_code = pays.pays_employee.em_state)) AS city_name, em_state,
                             (SELECT        TOP (1) sims_state_name_en
                               FROM            sims.sims_state
                               WHERE        (sims_state_code = pays.pays_employee.em_state) AND (sims_country_code = pays.pays_employee.em_country_code)) AS state_name, em_country_code,
                             (SELECT        TOP (1) sims_country_name_en
                               FROM            sims.sims_country
                               WHERE        (sims_country_code = pays.pays_employee.em_country_code)) AS country_code, em_phone, em_mobile, em_email, em_fax, em_po_box, em_img, em_dependant_full, em_dependant_half, 
                         em_dependant_infant, em_emergency_contact_name1, em_emergency_contact_name2, em_emergency_contact_number1, em_emergency_contact_number2, em_date_of_join, em_service_status, 
                         em_bank_cash_tag, em_ledger_code, em_ledger_ac_no, em_gpf_ac_no, em_gosi_ac_ac_no, em_gosi_start_date, em_pan_no, en_labour_card_no, em_iban_no, em_route_code, em_status, 
                         em_stop_salary_indicator, em_leave_resume_date, em_leave_tag, em_citi_exp_tag, em_joining_ref, em_bank_code,
                             (SELECT        TOP (1) bk_name
                               FROM            invs.banks
                               WHERE        (bk_code = pays.pays_employee.em_bank_code)) AS bank_name, em_bank_ac_no, em_bank_swift_code, em_left_date, em_left_reason, em_habdicap_status, em_leave_start_date, 
                         em_leave_end_date, em_cl_resume_date, em_leave_resume_ref, em_over_stay_days, em_under_stay_days, em_stop_salary_from, em_unpaid_leave, em_punching_status, em_punching_id, 
                         em_passport_number, em_passport_issue_date, em_passport_remember_expiry_date, em_passport_expiry_date, em_passport_return_date, em_passport_issuing_authority, em_passport_issue_place, 
                         em_visa_number, em_visa_issue_date, em_visa_expiry_date, em_visa_remember_expiry_date, em_visa_issuing_place, em_visa_issuing_authority, em_visa_type, em_national_id, 
                         em_national_id_issue_date, em_national_id_expiry_date, em_modified_on, em_last_login, em_secret_question_code, em_secret_answer, em, em_agreement, em_agreement_start_date, 
                         em_agreement_exp_date, em_agreement_exp_rem_date, em_blood_group_code,
                             (SELECT        TOP (1) sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_appl_code = 'Sim010') AND (sims_appl_form_field = 'BloodGroup') AND (sims_appl_parameter = pays.pays_employee.em_blood_group_code)) AS 'em_blood_group_name'
FROM            pays.pays_employee

GO
/****** Object:  View [dbo].[RandomNewID]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[RandomNewID]
AS
SELECT NEWID() AS [NewID]


GO
/****** Object:  View [dbo].[sims_collab_details]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[sims_collab_details]
AS
    SELECT  sims_smtp_id ,
            sims_email_flag ,
            sims_sms_flag ,
            sims_alert_flag ,
            sims_mobile_alert_flag ,
            sims_email_receipient_member ,
            sims_sms_receipient_member ,
            [sims_mod_code] ,
            [sims_appl_code] ,
            [sims_form_field] ,
            ( SELECT    sims_smtp_from_email
              FROM      sims.sims_smtp
              WHERE     ( sims_smtp_code = a.sims_smtp_id )
            ) AS SMTPEMAILD ,
            ( SELECT    sims_smtp_username
              FROM      sims.sims_smtp AS sims_smtp_5
              WHERE     ( sims_smtp_code = a.sims_smtp_id )
            ) AS SMTPProfile ,
            ( SELECT    sims_msg_subject
              FROM      sims.sims_email_sms_msg
              WHERE     ( sims_msg_sr_no = a.sims_email_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS EmailSubject ,
            ( SELECT    sims_msg_body
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_8
              WHERE     ( sims_msg_sr_no = a.sims_email_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS EmailBody ,
            ( SELECT    sims_msg_signature
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_7
              WHERE     ( sims_msg_sr_no = a.sims_email_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS EmailSignature ,
            ( SELECT    sims_msg_subject
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_6
              WHERE     ( sims_msg_sr_no = a.sims_sms_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS SMSSubject ,
            ( SELECT    sims_msg_body
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_5
              WHERE     ( sims_msg_sr_no = a.sims_sms_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS SMSBody ,
            ( SELECT    sims_msg_signature
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_4
              WHERE     ( sims_msg_sr_no = a.sims_sms_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS SMSSignature ,
            ( SELECT    sims_msg_subject
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_3
              WHERE     ( sims_msg_sr_no = a.sims_alert_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS AlertSubject ,
            ( SELECT    sims_msg_body
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_2
              WHERE     ( sims_msg_sr_no = a.sims_alert_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS AlertBody ,
            ( SELECT    sims_msg_signature
              FROM      sims.sims_email_sms_msg AS sims_email_sms_msg_1
              WHERE     ( sims_msg_sr_no = a.sims_alert_templ_no )
                        AND ( sims_msg_status = 'A' )
            ) AS AlertSignature ,
            ( SELECT    COUNT(*) AS Expr1
              FROM      sims.sims_email_transaction
              WHERE     ( DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              ))) )
                        AND ( DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              AS mogra_license_details_6
                                                              ))) )
                        AND ( DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              AS mogra_license_details_5
                                                              ))) )
                        AND ( sims_sender_email_id = ( SELECT sims_smtp_from_email
                                                       FROM   sims.sims_smtp
                                                              AS sims_smtp_4
                                                       WHERE  ( sims_smtp_code = a.sims_smtp_id )
                                                     ) )
            ) AS TotalDBDayMailCount ,
            ( SELECT    sims_smtp_max_mail_day
              FROM      sims.sims_smtp AS sims_smtp_3
              WHERE     ( sims_smtp_code = a.sims_smtp_id )
            ) AS TotalSMTPDayMailCount ,
            ( SELECT    COUNT(*) AS Expr1
              FROM      sims.sims_email_transaction AS sims_email_transaction_1
              WHERE     ( DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              AS mogra_license_details_4
                                                              ))) )
                        AND ( DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              AS mogra_license_details_3
                                                              ))) )
                        AND ( DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              AS mogra_license_details_2
                                                              ))) )
                        AND ( DATEPART(hh, sims_email_date) = DATEPART(hh,
                                                              SWITCHOFFSET(TODATETIMEOFFSET(GETDATE(),
                                                              DATEPART(TZ,
                                                              SYSDATETIMEOFFSET())),
                                                              ( SELECT TOP ( 1 )
                                                              lic_time_zone
                                                              FROM
                                                              mogra.mogra_license_details
                                                              AS mogra_license_details_1
                                                              ))) )
                        AND ( sims_sender_email_id = ( SELECT sims_email_transaction_1.sims_sender_email_id
                                                       FROM   sims.sims_smtp
                                                              AS sims_smtp_2
                                                       WHERE  ( sims_smtp_code = a.sims_smtp_id )
                                                     ) )
            ) AS TotalDBHourMailCount ,
            ( SELECT    sims_smtp_max_mail_hour
              FROM      sims.sims_smtp AS sims_smtp_1
              WHERE     ( sims_smtp_code = a.sims_smtp_id )
            ) AS TotalSMTPHourMailCount ,
            ( SELECT    sims_email_cc
              FROM      sims.sims_email_sms_msg
              WHERE     sims_msg_sr_no = [sims_email_templ_no]
            ) sims_email_cc ,
            ( SELECT    sims_email_bcc
              FROM      sims.sims_email_sms_msg
              WHERE     sims_msg_sr_no = [sims_email_templ_no]
            ) sims_email_bcc
    FROM    sims.sims_appl_collaboration_details AS a;


GO
/****** Object:  View [dbo].[sims_email_daily_count]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

CREATE VIEW [dbo].[sims_email_daily_count]
AS
SELECT        sims_smtp_id, sims_email_flag,[sims_mod_code],[sims_appl_code],[sims_form_field],
                             (SELECT        COUNT(*) AS Expr1
                               FROM            sims.sims_email_transaction
                               WHERE        (DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details)))) AND (DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_6)))) AND (DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_5)))) AND (sims_sender_email_id =
                                                             (SELECT        sims_smtp_from_email
                                                               FROM            sims.sims_smtp AS sims_smtp_4
                                                               WHERE        (sims_smtp_code = a.sims_smtp_id)))) AS TotalDBDayMailCount,
                             (SELECT        sims_smtp_max_mail_day
                               FROM            sims.sims_smtp AS sims_smtp_3
                               WHERE        (sims_smtp_code = a.sims_smtp_id)) AS TotalSMTPDayMailCount,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            sims.sims_email_transaction AS sims_email_transaction_1
                               WHERE        (DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_4)))) AND (DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_3)))) AND (DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_2)))) AND (DATEPART(hh, sims_email_date) = DATEPART(hh, SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_1)))) AND (sims_sender_email_id =
                                                             (SELECT        sims_email_transaction_1.sims_sender_email_id
                                                               FROM            sims.sims_smtp AS sims_smtp_2
                                                               WHERE        (sims_smtp_code = a.sims_smtp_id)))) AS TotalDBHourMailCount,
                             (SELECT        sims_smtp_max_mail_hour
                               FROM            sims.sims_smtp AS sims_smtp_1
                               WHERE        (sims_smtp_code = a.sims_smtp_id)) AS TotalSMTPHourMailCount
FROM            sims.sims_appl_collaboration_details AS a





GO
/****** Object:  View [dbo].[sims_email_schedule_daily_count]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

CREATE VIEW [dbo].[sims_email_schedule_daily_count]
AS
SELECT        sims_smtp_id, sims_email_flag,[sims_mod_code],[sims_appl_code],[sims_form_field],
                             (SELECT        COUNT(*) AS Expr1
                               FROM            sims.sims_email_transaction
                               WHERE        (DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details)))) AND (DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_6)))) AND (DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_5)))) AND (sims_sender_email_id =
                                                             (SELECT        sims_smtp_username
                                                               FROM            sims.sims_smtp AS sims_smtp_4
                                                               WHERE        (sims_smtp_code = a.sims_smtp_id)))) AS TotalDBDayMailCount,
                             (SELECT        sims_smtp_max_mail_day
                               FROM            sims.sims_smtp AS sims_smtp_3
                               WHERE        (sims_smtp_code = a.sims_smtp_id)) AS TotalSMTPDayMailCount,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            sims.sims_email_transaction AS sims_email_transaction_1
                               WHERE        (DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_4)))) AND (DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_3)))) AND (DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_2)))) AND (DATEPART(hh, sims_email_date) = DATEPART(hh, SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_1)))) AND (sims_sender_email_id =
                                                             (SELECT        sims_smtp_username
                                                               FROM            sims.sims_smtp AS sims_smtp_2
                                                               WHERE        (sims_smtp_code = a.sims_smtp_id)))) AS TotalDBHourMailCount,
                             (SELECT        sims_smtp_max_mail_hour
                               FROM            sims.sims_smtp AS sims_smtp_1
                               WHERE        (sims_smtp_code = a.sims_smtp_id)) AS TotalSMTPHourMailCount,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            sims.sims_email_schedule
                               WHERE        (DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_7)))) AND (DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_6)))) AND (DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_5)))) AND (sims_sender_id =
                                                             (SELECT        sims_smtp_username
                                                               FROM            sims.sims_smtp AS sims_smtp_4
                                                               WHERE        (sims_smtp_code = a.sims_smtp_id)))) AS TotalDBDayMailCount_schedule,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            sims.sims_email_schedule AS sims_email_transaction_1
                               WHERE        (DATEPART(yy, sims_email_date) = YEAR(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_4)))) AND (DATEPART(mm, sims_email_date) = MONTH(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_3)))) AND (DATEPART(dd, sims_email_date) = DAY(SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_2)))) AND (DATEPART(hh, sims_email_date) = DATEPART(hh, SWITCHOFFSET(TodateTimeoffset(GETDATE(), DATEPART(TZ, 
                                                         sysdatetimeoffset())),
                                                             (SELECT        TOP (1) lic_time_zone
                                                               FROM            mogra.mogra_license_details AS mogra_license_details_1)))) AND (sims_sender_id =
                                                             (SELECT        sims_smtp_username
                                                               FROM            sims.sims_smtp AS sims_smtp_2
                                                               WHERE        (sims_smtp_code = a.sims_smtp_id)))) AS TotalDBHourMailCount_schedule
FROM            sims.sims_appl_collaboration_details AS a






GO
/****** Object:  View [dbo].[sims_student_data_STSS]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[sims_student_data_STSS] as 
select distinct a.sims_student_enroll_number, dbo.fnProperCase(ltrim(rtrim(concat(coalesce(sims_student_passport_first_name_en,''),' ',coalesce(sims_student_passport_middle_name_en,''),' ',coalesce(sims_student_passport_last_name_en,''))))) [sims_student_passport_name_en], sims_student_national_id
,sims_parent_number, dbo.fnProperCase(ltrim(rtrim(concat(coalesce(sims_parent_father_first_name,''),' ',coalesce(sims_parent_father_middle_name,''),' ',coalesce(sims_parent_father_last_name,''))))) [sims_parent_father_name], replace(coalesce(sims_parent_father_mobile,''),'-','') [sims_parent_father_mobile]
, dbo.fnProperCase(ltrim(rtrim(concat(coalesce(sims_parent_mother_first_name,''),' ',coalesce(sims_parent_mother_middle_name,''),' ',coalesce(sims_parent_mother_last_name,''))))) [sims_parent_mother_name], replace(coalesce(sims_parent_mother_mobile,''),'-','') [sims_parent_mother_mobile]
,iif(e.sims_allocation_status in ('A','1'),'Present','Left') sims_student_acadmeic_status
,a.sims_student_commence_date	, cast(d.sims_student_status_updation_date as date) sims_student_cancel_date
,sims_student_attribute2, sims_student_attribute3, sims_student_attribute8,sims_student_attribute12
from sims.sims_Student a inner join sims.sims_sibling b on a.sims_student_enroll_number=b.sims_sibling_student_enroll_number
inner join sims.sims_parent c on b.sims_sibling_parent_number=c.sims_parent_number
left join sims.sims_academic_year f on f.sims_academic_year_status='C'
left join sims.sims_student_section e on a.sims_student_enroll_number=e.sims_enroll_number and e.sims_cur_code=f.sims_cur_Code and e.sims_academic_year=f.sims_academic_year
left join sims.sims_student_history d on a.sims_student_enroll_number=d.sims_student_enroll_number and d.sims_student_academic_status='C'	


GO
/****** Object:  View [dbo].[sims_student_data_update_STSS]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[sims_student_data_update_STSS] as 
select a.sims_student_enroll_number, sims_student_attribute2, sims_student_attribute3, sims_student_attribute8,sims_student_attribute12
from sims.sims_Student a, sims.sims_sibling b, sims.sims_parent c
where a.sims_student_enroll_number=b.sims_sibling_student_enroll_number and b.sims_sibling_parent_number=c.sims_parent_number

GO
/****** Object:  View [dbo].[View_1]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[View_1]
AS
SELECT        sims.sims_transport_route_stop.sims_transport_route_stop_code, sims.sims_transport_route.sims_transport_route_name, sims.sims_transport_route_student.sims_transport_route_code AS Expr1, 
                         sims.sims_transport_route_student.sims_transport_pickup_stop_code, sims.sims_transport_route_student.sims_transport_drop_stop_code, 
                         sims.sims_transport_route_stop.sims_transport_route_stop_expected_time, sims.sims_transport_vehicle.sims_transport_vehicle_name_plate
FROM            sims.sims_transport_route INNER JOIN
                         sims.sims_transport_stop ON sims.sims_transport_route.sims_academic_year = sims.sims_transport_stop.sims_academic_year INNER JOIN
                         sims.sims_transport_route_student ON sims.sims_transport_route.sims_transport_route_code = sims.sims_transport_route_student.sims_transport_route_code INNER JOIN
                         sims.sims_transport_route_stop ON sims.sims_transport_route_student.sims_transport_pickup_stop_code = sims.sims_transport_route_stop.sims_transport_route_stop_code AND 
                         sims.sims_transport_route_student.sims_transport_drop_stop_code = sims.sims_transport_route_stop.sims_transport_route_stop_code AND 
                         sims.sims_transport_stop.sims_transport_stop_code = sims.sims_transport_route_stop.sims_transport_route_stop_code CROSS JOIN
                         sims.sims_student CROSS JOIN
                         sims.sims_transport_vehicle


GO
/****** Object:  View [dbo].[View_2]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

CREATE VIEW [dbo].[View_2]
AS
SELECT        sims.sims_workflow_detail.sims_workflow_description, sims.sims_workflow_details_employee.sims_workflow_employee_code, 
                         pays.pays_employee.em_first_name + ' ' + pays.pays_employee.em_middle_name + ' ' + pays.pays_employee.em_last_name AS Name, sims.sims_workflow_employee_transaction.sims_workflow_no
FROM            sims.sims_workflow_detail INNER JOIN
                         sims.sims_workflow_details_employee ON sims.sims_workflow_detail.sims_workflow_srno = sims.sims_workflow_details_employee.sims_workflow_srno INNER JOIN
                         sims.sims_workflow_employee_transaction ON sims.sims_workflow_details_employee.sims_workflow_det_empno = sims.sims_workflow_employee_transaction.sims_workflow_det_empno INNER JOIN
                         pays.pays_employee ON sims.sims_workflow_details_employee.sims_workflow_employee_code = pays.pays_employee.em_login_code
WHERE        (sims.sims_workflow_employee_transaction.sims_workflow_status = 'P')


GO
/****** Object:  View [dbo].[view_atte_not_marked]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[view_atte_not_marked] 
as

SELECT sims_attendance_date,
(select sims_grade_name_en from sims.sims_grade where sims_grade_code=a.sims_grade_code) as grade,
(Select sims_section_name_en from sims.sims_section where sims_section_code=a.sims_section_code) as section
FROM sims.sims_student_attendance a
--WHERE [sims_attendance_day_attendance_code] IN ('UM') And
--[sims_attendance_date] between '2015/01/09' and  '2015/01/23' 
--AND sims_grade_code='01'  AND sims_section_code='1'
--GROUP BY sims_attendance_date,sims_grade_code,sims_section_code 





GO
/****** Object:  View [fins].[fins_pdc_detail_realize]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [fins].[fins_pdc_detail_realize]
AS
SELECT        ROW_NUMBER() OVER(order by pc_comp_code) AS Number, pc_comp_code, pc_sl_ldgr_code, pc_sl_acno, pc_dept_no, pc_doc_type, pc_our_doc_no, pc_rec_serial, 
                         pc_bank_from, pc_bank_slno, pc_cheque_no, pc_calendar, pc_due_date, pc_amount, pc_pstng_no, pc_narrative, pc_bank_to, pc_ret_times, pc_discd, pc_ref_no, 
                         pc_resub_ref, pc_cur_date, pc_amd_flag, pc_submission_date, pc_resub_date, pc_cancel_date
FROM            fins.fins_pdc_cheques AS a



GO
/****** Object:  View [invs].[customers]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [invs].[customers]
 (cus_account_no, cus_name, cus_credit_limit, cus_balance, cus_stop_cr_ind, 
 cus_status, cus_address1, cus_address2, cus_address3
 )

	   as 
select 
       slma_ldgrctl_code+'/'+slma_acno,
       coad_pty_full_name,
       slma_cr_limit,
       slma_yob_amt + slma_outstg_dr_amt + slma_outstg_cr_amt,
       slma_stop_cr_ind,
       slma_status,
       coad_po_box,
       coad_line_1,
       coad_line_2
from   fins.fins_sblgr_masters, fins.fins_pty_address
where  slma_comp_code = '1'
 and   slma_ldgrctl_year = YEAR(GETDATE())
 and   slma_ldgrctl_code in ('01', '02', '03')
 and   coad_comp_code = slma_comp_code
 and   coad_addr_id   = slma_addr_id




GO
/****** Object:  View [invs].[item_movements]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [invs].[item_movements] (
   im_inv_no, 
   sup_code, 
   sg_name, 
   sal_type, 
   im_item_code, 
   im_trade_cat, 
   doc_date, 
   dt_code, 
   doc_no, 
   dd_line_no, 
   dep_code, 
   loc_from, 
   loc_to, 
   ic_account_no, 
   cus_account_no, 
   doc_special_name, 
   uom_code, 
   dd_qty, 
   dd_sell_price, 
   dd_sell_price_discounted, 
   dd_sell_price_final, 
   dd_sell_value_final, 
   dd_sell_cost_total, 
   sm_code, 
   doc_prov_no, 
   doc_order_ref_no, 
   dd_status, 
   grv_no)
as 
   /*generated by sql server migration assistant for oracle version 5.2.1259.*/
   select 
      b.im_inv_no, 
      c.sup_code, 
      c.sg_name, 
      a.sal_type, 
      c.im_item_code, 
      c.im_trade_cat, 
      a.doc_date, 
      a.dt_code, 
      a.doc_no, 
      cast(b.dd_line_no as numeric(38, 10)) as dd_line_no, 
      b.dep_code, 
      b.loc_code as loc_from, 
      null as loc_to, 
      a.ic_account_no, 
      a.cus_account_no, 
      a.doc_special_name, 
      c.uom_code, 
      case a.dt_code
         when 'do' then b.dd_qty * -1
         when 'in' then b.dd_qty * -1
         when 'ji' then b.dd_qty * -1
         else b.dd_qty
      end as dd_qty, 
      b.dd_sell_price, 
      b.dd_sell_price_discounted, 
      b.dd_sell_price_final, 
      case a.dt_code
         when 'do' then b.dd_sell_value_final * -1
         when 'in' then b.dd_sell_value_final * -1
         when 'ji' then b.dd_sell_value_final * -1
         else b.dd_sell_value_final
      end as dd_sell_value_final, 
      case a.dt_code
         when 'do' then b.dd_sell_cost_total * -1
         when 'in' then b.dd_sell_cost_total * -1
         when 'ji' then b.dd_sell_cost_total * -1
         else b.dd_sell_cost_total
      end as dd_sell_cost_total, 
      a.sm_code, 
      a.doc_prov_no, 
      a.doc_order_ref_no, 
      b.dd_status as dd_status, 
      null as grv_no
   from invs.sale_documents  as a, invs.sale_document_details  as b, invs.item_master  as c
   where 
      a.dt_code in ( 
      'cr', 
      'jr', 
      'do', 
      'dr', 
      'in', 
      'ji' ) and 
      a.doc_status in ( '2', '3' ) and 
      b.dep_code = a.dep_code and 
      b.doc_prov_no = a.doc_prov_no and 
      isnull(b.doc_type_have_previous, 'xx') != 
      case a.dt_code
         when 'in' then 'do'
         else '*^'
      end and 
      c.im_inv_no = b.im_inv_no
    union all
   select 
      b.im_inv_no, 
      c.sup_code, 
      c.sg_name, 
      null as sale_type, 
      c.im_item_code, 
      c.im_trade_cat, 
      a.adj_date, 
      a.dt_code, 
      a.adj_doc_no, 
      b.ad_line_no, 
      b.dep_code, 
      a.loc_code as loc_from, 
      a.loc_code_to as loc_to, 
      null as ic_account_no, 
      null as cus_account_no, 
      null as doc_special_name, 
      c.uom_code, 
      case a.dt_code
         when 'tr' then -1 * b.ad_qty
         else b.ad_qty
      end as dd_qty, 
      0 as dd_sell_price, 
      0 as dd_sell_price_discounted, 
      b.ad_value / case b.ad_qty
         when 0 then 1
         else case a.dt_code
            when 'tr' then -1 * b.ad_qty
            else b.ad_qty
         end
      end as dd_sell_price_final, 
      case a.dt_code
         when 'tr' then -1 * b.ad_value
         else b.ad_value
      end as dd_sell_value_final, 
      case a.dt_code
         when 'tr' then -1 * b.ad_value
         else b.ad_value
      end as dd_sell_cost_total, 
      null as sm_code, 
      0 as doc_prov_no, 
      null as doc_order_ref_no, 
      null as dd_status, 
      a.adj_grv_no as grv_no
   from invs.adjustments  as a, invs.adjustment_details  as b, invs.item_master  as c
   where 
      a.dt_code in ( 'ad', 'st', 'tr' ) and 
      b.dep_code = a.dep_code and 
      b.adj_doc_no = a.adj_doc_no and 
      c.im_inv_no = b.im_inv_no
    union all
   select 
      b.im_inv_no, 
      c.sup_code, 
      c.sg_name, 
      null as sale_type, 
      c.im_item_code, 
      c.im_trade_cat, 
      a.adj_date, 
      a.dt_code, 
      a.adj_doc_no, 
      b.ad_line_no, 
      b.dep_code, 
      a.loc_code_to as loc_from, 
      a.loc_code as loc_to, 
      null as ic_account_no, 
      null as cus_account_no, 
      null as doc_special_name, 
      c.uom_code, 
      b.ad_qty as dd_qty, 
      0 as dd_sell_price, 
      0 as dd_sell_price_discounted, 
      b.ad_value / case b.ad_qty
         when 0 then 1
         else b.ad_qty
      end as dd_sell_price_final, 
      b.ad_value as dd_sell_value_final, 
      b.ad_value as dd_sell_cost_total, 
      null as sm_code, 
      0 as doc_prov_no, 
      null as doc_order_ref_no, 
      null as dd_status, 
      a.adj_grv_no as grv_no
   from invs.adjustments  as a, invs.adjustment_details  as b, invs.item_master  as c
   where 
      a.dt_code in (  'tr' ) and 
      b.dep_code = a.dep_code and 
      b.adj_doc_no = a.adj_doc_no and 
      c.im_inv_no = b.im_inv_no
    union all
   select 
      b.im_inv_no, 
      c.sup_code, 
      c.sg_name, 
      null as sale_type, 
      c.im_item_code, 
      c.im_trade_cat, 
      a.cs_authorized_date, 
      'cs' as dt_code, 
      a.cs_no, 
      b.csd_line_no, 
      a.dep_code, 
      b.loc_code as loc_from, 
      null as loc_to, 
      null as ic_account_no, 
      null as cus_account_no, 
      null as doc_special_name, 
      c.uom_code, 
      isnull(b.csd_binned_qty, 0) as dd_qty, 
      b.csd_supl_unit_price, 
      0 as dd_sell_price_discounted, 
      b.csd_landed_cost / case isnull(b.csd_binned_qty, 0)
         when 0 then 1
         else isnull(b.csd_binned_qty, 0)
      end as dd_sell_price_final, 
      b.csd_landed_cost as dd_sell_value_final, 
      b.csd_landed_cost as dd_sell_cost_total, 
      null as sm_code, 
      a.cs_prov_no as doc_prov_no, 
      null as doc_order_ref_no, 
      null as dd_status, 
      null as grv_no
   from invs.costing_sheets  as a, invs.costing_sheet_details  as b, invs.item_master  as c
   where 
      a.cs_status in ( '5', '6' ) and 
      b.cs_prov_no = a.cs_prov_no and 
      c.im_inv_no = b.im_inv_no
    union all
   select 
      b.im_inv_no, 
      c.sup_code, 
      c.sg_name, 
      null as sale_type, 
      c.im_item_code, 
      c.im_trade_cat, 
      a.cs_authorized_date, 
      'cs' as dt_code, 
      a.cs_no, 
      b.csad_line_no, 
      a.dep_code, 
      b.loc_code as loc_from, 
      null as loc_to, 
      null as ic_account_no, 
      null as cus_account_no, 
      null as doc_special_name, 
      c.uom_code, 
      isnull(b.csad_binned_qty, 0) as dd_qty, 
      b.csad_supl_unit_price, 
      0 as dd_sell_price_discounted, 
      b.csad_landed_cost / case isnull(b.csad_binned_qty, 0)
         when 0 then 1
         else isnull(b.csad_binned_qty, 0)
      end as dd_sell_price_final, 
      b.csad_landed_cost as dd_sell_value_final, 
      b.csad_landed_cost as dd_sell_cost_total, 
      null as sm_code, 
      a.cs_prov_no as doc_prov_no, 
      null as doc_order_ref_no, 
      null as dd_status, 
      null as grv_no
   from invs.costing_sheets  as a, invs.costing_sheet_addl_details  as b, invs.item_master  as c
   where 
      a.cs_status in ( '5', '6' ) and 
      b.cs_prov_no = a.cs_prov_no and 
      c.im_inv_no = b.im_inv_no




GO
/****** Object:  View [pays].[pays_employee_list_report]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

-- View

CREATE VIEW [pays].[pays_employee_list_report]
AS
SELECT        em_login_code, em_dept_code, em_dept_effect_from, em_desg_code,
                             (SELECT     top 1   dg_desc
                               FROM            pays.pays_designation
                               WHERE        (dg_code = a.em_desg_code)) AS em_designation, em_dest_code,
                             (SELECT     top 1    ds_name
                               FROM            pays.pays_destination
                               WHERE        (ds_code = a.em_dest_code) AND (ds_nation_code = a.em_nation_code)) AS destination_name, em_nation_code,
                             (SELECT     top 1    sims_country_name_en
                               FROM            sims.sims_country
                               WHERE        (sims_country_code = a.em_nation_code)) AS emp_country, em_grade_code,
                             (SELECT    top 1     gr_desc
                               FROM            pays.pays_grade
                               WHERE        (gr_code = a.em_grade_code)) AS grade_name, em_grade_effect_from, em_staff_type,
                             (SELECT    top 1     sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Staff Type') AND (sims_appl_parameter = a.em_staff_type)) AS emp_staff_type, em_salutation, 
                         em_first_name + ' ' + em_middle_name + ' ' + em_last_name AS emp_name, em_name_ot, em_date_of_birth, em_sex,
                             (SELECT     top 1    sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_2
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Gender') AND (sims_appl_parameter = a.em_sex)) AS em_gender,
                             (SELECT     top 1    sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_1
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Marital Status') AND (sims_appl_parameter = a.em_marital_status)) AS emp_marital_status, 
                         em_religion_code,
                             (SELECT     top 1    sims_religion_name_en
                               FROM            sims.sims_religion
                               WHERE        (sims_religion_code = a.em_religion_code)) AS religion_code, em_ethnicity_code,
                             (SELECT  top 1       sims_ethnicity_name_en
                               FROM            sims.sims_ethnicity
                               WHERE        (sims_ethnicity_code = a.em_ethnicity_code)) AS emp_ethnicity, em_appartment_number, em_building_number, em_street_number, em_area_number, em_summary_address, 
                         em_summary_address_local_language, em_country_code,
                             (SELECT    top 1     sims_country_name_en
                               FROM            sims.sims_country AS sims_country_1
                               WHERE        (sims_country_code = a.em_country_code)) AS em_address_country, em_state,
                             (SELECT    top 1     sims_state_name_en
                               FROM            sims.sims_state
                               WHERE        (sims_country_code = a.em_country_code) AND (sims_state_code = a.em_state)) AS em_address_state, em_city,
                             (SELECT     top 1    sims_city_name_en
                               FROM            sims.sims_city
                               WHERE        (sims_state_code = a.em_state) AND (sims_city_code = a.em_city)) AS em_add_city, em_phone, em_mobile, em_email, em_fax, em_po_box, em_img, em_emergency_contact_number1, 
                         em_emergency_contact_name1, em_emergency_contact_number2, em_emergency_contact_name2, em_date_of_join,
                             (SELECT     top 1    sims_financial_year
                               FROM            sims.sims_financial_year
                               WHERE        (a.em_date_of_join BETWEEN sims_financial_year_start_date AND sims_financial_year_end_date)) AS year_of_join, em_gpf_ac_no, em_gosi_start_date, em_pan_no, en_labour_card_no, 
                         em_status, em_service_status,
                             (SELECT     top 1    sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_3
                               WHERE        (sims_mod_code = '004') AND (sims_appl_code = 'Per099') AND (sims_appl_form_field = 'Service Status') AND (sims_appl_parameter = a.em_service_status)) AS service_status,
                             (SELECT    top 1     sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_4
                               WHERE        (sims_mod_code = '003') AND (sims_appl_code = 'Sim010') AND (sims_appl_form_field = 'BloodGroup') AND (sims_appl_parameter = a.em_blood_group_code)) AS blood_group
FROM            pays.pays_employee AS a


GO
/****** Object:  View [pays].[pays_salary_details_v]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [pays].[pays_salary_details_v] (
   sd_number, 
   sd_company_code, 
   sd_pay_code, 
   sd_year_month, 
   sd_amount)
as 
   /*generated by sql server migration assistant for oracle version 5.2.1259.*/
   select 
      pays_salary_details.sd_number, 
      pays_salary_details.sd_company_code, 
      pays_salary_details.sd_pay_code, 
      pays_salary_details.sd_year_month, 
      pays_salary_details.sd_amount
   from pays.pays_salary_details
    union
   select 
      pays_suppl_payment.su_number, 
      pays_suppl_payment.su_company_code, 
      pays_suppl_payment.su_pay_code, 
      pays_suppl_payment.su_year_month, 
      pays_suppl_payment.su_amount
   from pays.pays_suppl_payment
    union
   select 
      pays_net_payment.no_number, 
      pays_net_payment.no_company_code, 
      '199', 
      pays_net_payment.no_year_month, 
      isnull(pays_net_payment.no_carry_forward, 0)
   from pays.pays_net_payment
    union
   select 
      pays_net_payment.no_number, 
      pays_net_payment.no_company_code, 
      '099', 
      pays_net_payment.no_year_month, 
      isnull(pays_net_payment.no_unrecovered_amt, 0)
   from pays.pays_net_payment
    union
   select 
      pays_salary_hist.sh_number, 
      pays_salary_hist.sh_company_code, 
      pays_salary_hist.sh_pay_code, 
      pays_salary_hist.sh_year_month, 
      pays_salary_hist.sh_amount
   from pays.pays_salary_hist













GO
/****** Object:  View [sims].[sims_admission_list_details]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- View



CREATE VIEW [sims].[sims_admission_list_details]
AS
select bs.*,
(select isnull(sims_student_passport_first_name_en,'')+' '+isnull(sims_student_passport_middle_name_en,'')+' '+isnull(sims_student_passport_last_name_en,'')  from sims.sims_student  where 
sims_student_enroll_number=bs.sims_admission_sibling_enroll_number) as 'student_name',
(select sims_grade_name_en from sims.sims_grade where sims_cur_code=ss.sims_cur_code and sims_academic_year=ss.sims_academic_year and sims_grade_code=ss.sims_grade_code) as Grade,
(select sims_section_name_en from sims.sims_section where sims_cur_code=ss.sims_cur_code and sims_academic_year=ss.sims_academic_year and sims_grade_code=ss.sims_grade_code and sims_section_code=ss.sims_section_code) as Section
from (
SELECT	--Admission no details--
        v.sims_sibling_student_enroll_number,
		a.sims_admission_number, a.sims_admission_application_number, a.sims_admission_date, 
		a.sims_admission_pros_application_number, a.sims_admission_pros_number,n.sims_student_enroll_number,
		--Admission School Details--
		a.sims_admission_school_code, b.lic_school_name, 
		a.sims_admission_cur_code, c.sims_cur_short_name_en, 
		a.sims_admission_tentative_joining_date as sims_admission_tentative_joining_date, a.sims_admission_academic_year,
		IIF(a.sims_admission_status='C',o.sims_grade_code,a.sims_admission_grade_code) as sims_admission_grade_code, 
		(select sims_grade_name_en from sims.sims_grade where sims_cur_code+sims_academic_year+sims_grade_code=IIF(a.sims_admission_status='C',o.sims_cur_code+o.sims_academic_year+o.sims_grade_code,a.sims_admission_cur_code+a.sims_admission_academic_year+a.sims_admission_grade_code)) as sims_grade_name_en, 
		IIF(a.sims_admission_status='C',o.sims_section_code,a.sims_admission_section_code) as sims_admission_section_code, 
	    (select sims_section_name_en from sims.sims_section where sims_cur_code+sims_academic_year+sims_grade_code+sims_section_code=IIF(a.sims_admission_status='C',o.sims_cur_code+o.sims_academic_year+o.sims_grade_code+o.sims_section_code,a.sims_admission_cur_code+a.sims_admission_academic_year+a.sims_admission_grade_code+a.sims_admission_section_code)) as section_name,  
		a.sims_admission_term_code, f.sims_term_desc_en, 
		--Personal Details of student--
		CONCAT(isnull(a.sims_admission_passport_first_name_en,''), ' ',isnull(a.sims_admission_passport_middle_name_en,''), ' ',isnull(a.sims_admission_passport_last_name_en,'')) AS student_name_en,
		CONCAT(isnull(a.sims_admission_passport_first_name_ot,''),' ', isnull(a.sims_admission_passport_middle_name_ot,''), ' ',isnull(a.sims_admission_passport_last_name_ot,'')) AS student_name_ot,
		a.sims_admission_dob, sims_admission_commencement_date, 
		a.sims_admission_gender, (SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
								WHERE sims_mod_code = '003' AND sims_appl_form_field = 'Gender' AND 
								sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_gender) AS gender, 
		a.sims_admission_birth_country_code, g.sims_country_name_en,
		a.sims_admission_nationality_code, h.sims_nationality_name_en,
		a.sims_admission_ethnicity_code, i.sims_ethnicity_name_en,
		a.sims_admission_religion_code, j.sims_religion_name_en, 
		--Passport Details--
		a.sims_admission_passport_number, a.sims_admission_passport_issue_date, a.sims_admission_passport_expiry_date, 
		a.sims_admission_passport_issuing_authority, sims_admission_passport_issue_place, 
		--Visa/National ID Information--
		a.sims_admission_visa_type, (SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
									WHERE sims_mod_code = '004' AND 
									sims_appl_form_field = 'Visa Type' AND 
									sims_appl_code = 'Per099' AND 
									sims_appl_parameter = a.sims_admission_visa_type ) AS visa_type,a.sims_admission_visa_number,
		a.sims_admission_visa_issuing_place, a.sims_admission_visa_issuing_authority, a.sims_admission_visa_issue_date,
		a.sims_admission_visa_expiry_date, a.sims_admission_national_id, a.sims_admission_national_id_issue_date, 
		a.sims_admission_national_id_expiry_date,
		--Sibling Information--
		a.sims_admission_sibling_status, a.sims_admission_parent_id, a.sims_admission_sibling_enroll_number, 
		a.sims_admission_sibling_name, a.sims_admission_sibling_dob, sims_admission_sibling_school_code, 
		k.lic_school_name AS lic_sibling_school_name,
		--Father Employee of school--
		a.sims_admission_employee_code, 
		--Language details--
		a.sims_admission_main_language_code, l.sims_language_name_en, 
		a.sims_admission_main_language_m, a.sims_admission_main_language_r, 
		a.sims_admission_main_language_s, a.sims_admission_main_language_w, 
		--Parent Details--
	    CASE WHEN NULLIF(IIF(a.sims_admission_parent_id = 'N/A','',sims_admission_parent_id),'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_father_salutation_code),' ',isnull(a.sims_admission_father_first_name,''),' ',isnull(a.sims_admission_father_middle_name,''),' ',isnull(a.sims_admission_father_last_name,'')) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_father_salutation_code),' ',isnull(m.sims_parent_father_first_name,a.sims_admission_father_first_name),' ',isnull(m.sims_parent_father_middle_name,a.sims_admission_father_middle_name), ' ',isnull(m.sims_parent_father_last_name,a.sims_admission_father_last_name)) END AS father_name,
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_mother_salutation_code),' ',isnull(a.sims_admission_mother_first_name,''),' ',isnull(a.sims_admission_mother_middle_name,''),' ',isnull(a.sims_admission_mother_last_name,'')) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_mother_salutation_code),' ',isnull(m.sims_parent_mother_first_name,a.sims_admission_mother_first_name),' ',isnull(m.sims_parent_mother_middle_name,a.sims_admission_mother_middle_name), ' ',isnull(m.sims_parent_mother_last_name,a.sims_admission_mother_last_name)) END AS mother_name,
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_admission_guardian_salutation_code),' ',isnull(a.sims_admission_guardian_first_name,''),' ',isnull(a.sims_admission_guardian_middle_name,''),' ',isnull(a.sims_admission_guardian_last_name,'')) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_guardian_salutation_code),' ',isnull(m.sims_parent_guardian_first_name,''),' ',isnull(m.sims_parent_guardian_middle_name,''), ' ',isnull(m.sims_parent_guardian_last_name,'')) END AS guardian_name, 
		a.sims_admission_primary_contact_pref, a.sims_admission_fee_payment_contact_pref, 
		a.sims_admission_transport_status, a.sims_admission_transport_desc, 
		--CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_father_mobile ELSE m.sims_parent_father_mobile END
		a.sims_admission_father_mobile
		 AS father_mobile, 
		--CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_mother_mobile ELSE m.sims_parent_mother_mobile END
		sims_admission_mother_mobile
		 AS mother_mobile, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_guardian_mobile ELSE m.sims_parent_guardian_mobile END AS guardian_mobile, 
		--CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_father_phone ELSE m.sims_parent_father_phone END 
		 a.sims_admission_father_phone
		AS father_phone, 
		
		--CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_mother_phone ELSE m.sims_parent_mother_phone END 
		sims_admission_mother_phone
		AS mother_phone, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_guardian_phone ELSE m.sims_parent_guardian_phone END AS guardian_phone, 

		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_father_email ELSE m.sims_parent_father_email END AS father_email, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_mother_email ELSE m.sims_parent_mother_email END AS mother_email, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_guardian_email ELSE m.sims_parent_guardian_email END AS guardian_email, 


		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN 
		CONCAT(a.sims_admission_father_appartment_number,' ',a.sims_admission_father_building_number,' ',a.sims_admission_father_street_number,' ',
		a.sims_admission_father_area_number,' ',a.sims_admission_father_city,' ',a.sims_admission_father_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_admission_father_country_code))
		ELSE CONCAT(m.sims_parent_father_appartment_number,' ',m.sims_parent_father_building_number,' ',m.sims_parent_father_street_number,' ',
		m.sims_parent_father_area_number,' ',m.sims_parent_father_city,' ',m.sims_parent_father_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_father_country_code)) END AS father_address,
				CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN 
		CONCAT(a.sims_admission_mother_appartment_number,' ',a.sims_admission_mother_building_number,' ',a.sims_admission_mother_street_number,' ',
		a.sims_admission_mother_area_number,' ',a.sims_admission_mother_city,' ',a.sims_admission_mother_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_admission_mother_country_code))
		ELSE CONCAT(m.sims_parent_mother_appartment_number,' ',m.sims_parent_mother_building_number,' ',m.sims_parent_mother_street_number,' ',
		m.sims_parent_mother_area_number,' ',m.sims_parent_mother_city,' ',m.sims_parent_mother_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_mother_country_code)) END AS mother_address,
				CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN 
		CONCAT(a.sims_admission_guardian_appartment_number,' ',a.sims_admission_guardian_building_number,' ',a.sims_admission_guardian_street_number,' ',
		a.sims_admission_guardian_area_number,' ',a.sims_admission_guardian_city,' ',a.sims_admission_guardian_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_admission_guardian_country_code))
		ELSE CONCAT(m.sims_parent_guardian_appartment_number,' ',m.sims_parent_guardian_building_number,' ',m.sims_parent_guardian_street_number,' ',
		m.sims_parent_guardian_area_number,' ',m.sims_parent_guardian_city,' ',m.sims_parent_guardian_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_guardian_country_code)) END AS guardian_address, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_father_po_box ELSE m.sims_parent_father_po_box END AS father_po_box, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_mother_po_box ELSE m.sims_parent_mother_po_box END AS mother_po_box, 
		CASE WHEN NULLIF(IIF(a.sims_admission_parent_id ='N/A','',sims_admission_parent_id),'') IS NULL THEN a.sims_admission_guardian_po_box ELSE m.sims_parent_guardian_po_box END AS guardian_po_box,
		--Previous School Details--
		a.sims_admission_current_school_name, a.sims_admission_current_school_enroll_number, 
		a.sims_admission_current_school_grade, a.sims_admission_current_school_cur, 
		a.sims_admission_current_school_from_date, a.sims_admission_current_school_to_date, 
		a.sims_admission_current_school_language, a.sims_admission_current_school_phone, 
		a.sims_admission_current_school_address, a.sims_admission_current_school_city,
		(SELECT sc.sims_country_name_en FROM sims.sims_country AS sc WHERE sc.sims_country_code = a.sims_admission_current_school_country_code) AS sims_admission_current_school_country_name, 
		--Legal Custoday Details--
		a.sims_admission_legal_custody, 
		--Health Details--
		a.sims_admission_health_card_number, a.sims_admission_health_card_issue_date, a.sims_admission_health_card_expiry_date, 
		a.sims_admission_blood_group_code, (select sims_appl_form_field_value1 from sims.sims_parameter where sims_mod_code = '003'  AND sims_appl_code = 'Sim010' AND
											sims_appl_form_field = 'BloodGroup' AND sims_appl_parameter = a.sims_admission_blood_group_code) AS blood_group, 
		a.sims_admission_health_restriction_status,a.sims_admission_health_restriction_desc, 
		a.sims_admission_disability_status, a.sims_admission_disability_desc, 
		a.sims_admission_medication_status, a.sims_admission_medication_desc, 
		a.sims_admission_health_other_status, a.sims_admission_health_other_desc, 
		a.sims_admission_health_hearing_status, a.sims_admission_health_hearing_desc, 
		a.sims_admission_health_vision_status, a.sims_admission_health_vision_desc, 
		--Other Details--
		a.sims_admission_gifted_status, a.sims_admission_gifted_desc, 
		a.sims_admission_music_status, a.sims_admission_music_desc, 
		a.sims_admission_sports_status, a.sims_admission_sports_desc, 
		a.sims_admission_language_support_status, a.sims_admission_language_support_desc,
		--Status--
		a.sims_admission_status,(SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
								 WHERE sims_mod_code = '003' AND sims_appl_code= 'Sim010' AND 
										sims_appl_form_field = 'Admission Status' 
										AND sims_appl_parameter = a.sims_admission_status)	AS sims_admission_status_value, 
		a.sims_student_health_respiratory_status,a.sims_student_health_respiratory_desc,a.sims_student_health_hay_fever_status,
		a.sims_student_health_hay_fever_desc, a.sims_student_health_epilepsy_status, a.sims_student_health_epilepsy_desc,
		a.sims_student_health_skin_status, a.sims_student_health_skin_desc, a.sims_student_health_diabetes_status, 
		a.sims_student_health_diabetes_desc,a.sims_student_health_surgery_status, a.sims_student_health_surgery_desc, 
		a.sims_student_attribute3, a.sims_student_attribute4, a.sims_admission_father_national_id, a.sims_admission_mother_national_id,
		a.sims_admission_guardian_national_id, a.sims_student_organizational_skills, a.sims_student_social_skills, a.sims_student_academic_standard, 
		a.sims_student_special_education, a.sims_student_attribute5 , a.sims_student_attribute6, a.sims_student_attribute7,
		a.sims_student_attribute8, a.sims_student_attribute9, a.sims_student_attribute10, a.sims_student_attribute11 as sims_student_attribute,
		a.sims_student_attribute12, a.sims_student_attribute1,a.sims_student_attribute2,a.sims_student_attribute11
		,(select sims_academic_year_description from sims.sims_academic_year where sims_academic_year=a.sims_admission_academic_year) as acad_year_desc

FROM	sims.sims_admission AS a LEFT OUTER JOIN mogra.mogra_license_details AS b ON 
		a.sims_admission_school_code = b.lic_school_code LEFT OUTER JOIN sims.sims_cur AS c ON 
		a.sims_admission_cur_code = c.sims_cur_code LEFT OUTER JOIN sims.sims_grade AS d ON 
		a.sims_admission_cur_code = d.sims_cur_code AND a.sims_admission_academic_year = d.sims_academic_year AND
		a.sims_admission_grade_code = d.sims_grade_code LEFT OUTER JOIN sims.sims_section AS e ON 
		a.sims_admission_cur_code = e.sims_cur_code AND a.sims_admission_academic_year = e.sims_academic_year AND 
		a.sims_admission_grade_code = e.sims_grade_code AND 
		a.sims_admission_section_code = e.sims_section_code	LEFT OUTER JOIN sims.sims_term AS f ON 
		a.sims_admission_cur_code = f.sims_cur_code AND a.sims_admission_academic_year = f.sims_academic_year AND 
		a.sims_admission_term_code = f.sims_term_code LEFT OUTER JOIN sims.sims_country AS g ON 
		a.sims_admission_birth_country_code = g.sims_country_code LEFT OUTER JOIN sims.sims_nationality AS h ON 
		a.sims_admission_nationality_code = h.sims_nationality_code LEFT OUTER JOIN sims.sims_ethnicity AS i ON 
		a.sims_admission_ethnicity_code = i.sims_ethnicity_code LEFT OUTER JOIN sims.sims_religion AS j ON 
		a.sims_admission_religion_code = j.sims_religion_code LEFT OUTER JOIN mogra.mogra_license_details AS k ON 
		a.sims_admission_sibling_school_code = k.lic_school_code LEFT OUTER JOIN sims.sims_language AS l ON
		a.sims_admission_main_language_code = l.sims_language_code LEFT OUTER JOIN sims.sims_parent AS m ON 
		a.sims_admission_parent_id = m.sims_parent_number left outer join sims.sims_student as n on
		a.sims_admission_number=n.[sims_student_admission_number] and n.sims_student_academic_status in ('1','A')
		left outer join  sims.sims_student_section as o on
		n.sims_student_enroll_number=o.sims_enroll_number and o.sims_cur_code=n.sims_student_cur_code and 
		o.sims_academic_year=a.sims_admission_academic_year and
		o.sims_allocation_status in ('1','A')
		LEFT OUTER JOIN sims.sims_sibling v ON 
		v.sims_sibling_parent_number=a.sims_admission_parent_id
		) bs left join sims.sims_student_section ss ON
		ss.sims_enroll_number=bs.sims_admission_sibling_enroll_number--bs.sims_sibling_student_enroll_number
		and ss.sims_academic_year=(select top 1 sims_academic_year from sims.sims_academic_year where sims_academic_year_status='C')
		and ss.sims_allocation_status in ('1','A')
		left join sims.sims_student sss ON 
		sss.sims_student_enroll_number=bs.sims_admission_sibling_enroll_number--bs.sims_sibling_student_enroll_number
		and sss.sims_student_academic_status in ('1','A')
		





























GO
/****** Object:  View [sims].[sims_circular_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE View [sims].[sims_circular_list]
AS
SELECT        a.sims_circular_number, a.sims_academic_year, a.sims_cur_code, a.sims_circular_date, a.sims_circular_publish_date, a.sims_circular_expiry_date, 
                         a.sims_circular_title, a.sims_circular_short_desc, a.sims_circular_desc, a.sims_circular_file_path1, a.sims_circular_file_path2, a.sims_circular_file_path3, 
                         a.sims_circular_type, a.sims_circular_category, a.sims_circular_created_user_code, a.sims_circular_display_order, b.sims_cur_short_name_en, 
                         c.sims_term_desc_en, SUBSTRING(dbo.GetMonthName(MONTH(a.sims_circular_publish_date)), 1, 3) AS publish_month, 
                         CASE WHEN a.sims_circular_expiry_date <= GetDate() OR
                         a.sims_circular_publish_date > GETDATE() THEN 'I' ELSE 'A' END AS circular_status,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_mod_code = '001') AND (sims_appl_code = 'Com017') AND (sims_appl_form_field = 'Circular Category') AND 
                                                         (sims_appl_parameter = a.sims_circular_category)) AS sims_circular_category_desc,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter AS sims_parameter_1
                               WHERE        (sims_mod_code = '001') AND (sims_appl_code = 'Com017') AND (sims_appl_form_field = 'Circular Type') AND 
                                                         (sims_appl_parameter = a.sims_circular_type)) AS sims_circular_type_desc, CASE WHEN
                             (SELECT        ISNULL(em_first_name, '') + ' ' + ISNULL(em_middle_name, '') + ' ' + ISNULL(em_last_name, '')
                               FROM            pays.pays_employee AS d
                               WHERE        d .em_login_code = a.sims_circular_created_user_code) IS NULL THEN
                             (SELECT        comn_user_alias
                               FROM            comn.comn_user
                               WHERE        comn_user_code = a.sims_circular_created_user_code) ELSE
                             (SELECT        ISNULL(em_first_name, '') + ' ' + ISNULL(em_middle_name, '') + ' ' + ISNULL(em_last_name, '')
                               FROM            pays.pays_employee AS d
                               WHERE        d .em_login_code = a.sims_circular_created_user_code) END AS sims_circular_created_by
FROM            sims.sims_circular AS a INNER JOIN
                         sims.sims_cur AS b ON a.sims_cur_code = b.sims_cur_code INNER JOIN
                         sims.sims_term AS c ON a.sims_cur_code = c.sims_cur_code AND a.sims_academic_year = c.sims_academic_year
WHERE        (a.sims_circular_publish_date BETWEEN c.sims_term_start_date AND c.sims_term_end_date)



GO
/****** Object:  View [sims].[sims_class_teacher_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [sims].[sims_class_teacher_list]
AS
SELECT        sims.sims_class_teacher.sims_academic_year, sims.sims_class_teacher.sims_cur_code, sims.sims_class_teacher.sims_grade_code,
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade
                               WHERE        (sims_grade_code = sims.sims_class_teacher.sims_grade_code)  and sims_academic_year= sims.sims_class_teacher.sims_academic_year) AS grade_name, 
							   
							   
							   sims.sims_class_teacher.sims_section_code,
                             (SELECT        sims_section_name_en
                               FROM            sims.sims_section
                               WHERE        (sims_grade_code = sims.sims_class_teacher.sims_grade_code) AND (sims_section_code = sims.sims_class_teacher.sims_section_code) and sims_academic_year= sims.sims_class_teacher.sims_academic_year) 
                         AS section_name, sims.sims_class_teacher.sims_class_teacher_code, sims.sims_teacher.sims_employee_code, 
                         [dbo].[fnProperCase](sims.sims_teacher.sims_teacher_name) as sims_teacher_name
 FROM            sims.sims_class_teacher INNER JOIN
                         sims.sims_teacher ON sims.sims_class_teacher.sims_class_teacher_code = sims.sims_teacher.sims_teacher_code
						 and sims.sims_teacher.sims_status='A'




GO
/****** Object:  View [sims].[sims_house_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [sims].[sims_house_list]
AS
SELECT        sims.sims_student.sims_student_enroll_number, 
                         isnull(sims.sims_student.sims_student_passport_first_name_en,'') + ' ' + isnull(sims.sims_student.sims_student_passport_middle_name_en,'') + ' ' + isnull(sims.sims_student.sims_student_passport_last_name_en,'')
                          AS StudentName, sims.sims_student.sims_student_house, ISNULL(sims.sims_house.sims_house_name, N'Not Assigned') AS house_name, 
                         ISNULL(sims.sims_house.sims_house_color, N'#FF00FF') AS color_code, sims.sims_student_section.sims_cur_code, 
                         sims.sims_student_section.sims_academic_year,
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade
                               WHERE        (sims_grade_code = sims.sims_student_section.sims_grade_code) and (sims_academic_year=sims.sims_student_section.sims_academic_year) and (sims_cur_code=sims.sims_student_section.sims_cur_code)) AS grade_name,
                             (SELECT        sims_section_name_en
                               FROM            sims.sims_section
                               WHERE        (sims_section_code = sims.sims_student_section.sims_section_code) and (sims_grade_code = sims.sims_student_section.sims_grade_code) and (sims_academic_year=sims.sims_student_section.sims_academic_year) and (sims_cur_code=sims.sims_student_section.sims_cur_code)) AS section_name,
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade AS sims_grade_1
                               WHERE        (sims_grade_code = sims.sims_student_section.sims_grade_code) and (sims_academic_year=sims.sims_student_section.sims_academic_year) and (sims_cur_code=sims.sims_student_section.sims_cur_code)) + ' ' +
                             (SELECT        sims_section_name_en
                               FROM            sims.sims_section AS sims_section_1
                               WHERE        (sims_section_code = sims.sims_student_section.sims_section_code) and (sims_grade_code = sims.sims_student_section.sims_grade_code) and (sims_academic_year=sims.sims_student_section.sims_academic_year) and (sims_cur_code=sims.sims_student_section.sims_cur_code)) AS Class, sims.sims_student_section.sims_grade_code, 
                         sims.sims_student_section.sims_section_code, sims.sims_student_section.sims_allocation_status
FROM            sims.sims_student_section INNER JOIN
                         sims.sims_student ON sims.sims_student_section.sims_enroll_number = sims.sims_student.sims_student_enroll_number LEFT OUTER JOIN
                         sims.sims_house ON sims.sims_student.sims_student_house = sims.sims_house.sims_house_code and sims.sims_student_section.sims_academic_year=sims.sims_house.sims_cur_year


				


GO
/****** Object:  View [sims].[sims_library_item_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [sims].[sims_library_item_list]
AS
SELECT        a.sims_library_item_number, a.sims_library_item_accession_number, a.sims_library_isbn_number, a.sims_library_bnn_number, a.sims_library_item_dewey_number, a.sims_library_item_name, 
                         a.sims_library_item_title, a.sims_library_item_desc, a.sims_library_item_category, b.sims_library_category_name, a.sims_library_item_subcategory, c.sims_library_subcategory_name, 
                         a.sims_library_item_catalogue_code, d.sims_library_catalogue_name, a.sims_library_item_cur_code, e.sims_cur_short_name_en, a.sims_library_item_cur_level_code, f.sims_cur_level_name_en, 
                         a.sims_library_item_grade_code, g.sims_grade_name_en, a.sims_library_item_language_code, h.sims_language_name_en, a.sims_library_item_remark, a.sims_library_item_author1, 
                         a.sims_library_item_author2, a.sims_library_item_author3, a.sims_library_item_author4, a.sims_library_item_author5, a.sims_library_item_age_from, a.sims_library_item_age_to, 
                         a.sims_library_item_edition_statement, a.sims_library_item_place_of_publication, a.sims_library_item_name_of_publisher, a.sims_library_item_date_of_publication, a.sims_library_item_extent_of_item, 
                         a.sims_library_item_accompanying_item, a.sims_library_item_printing_date, a.sims_library_item_bar_code, a.sims_library_item_qr_code, a.sims_library_item_storage_hint, 
                         a.sims_library_item_purchase_price, a.sims_library_item_keywords, a.sims_library_item_number_of_copies, a.sims_library_item_known_as, a.sims_library_item_book_style, i.sims_library_item_style_name, 
                         a.sims_library_item_status_code,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_mod_code = '014') AND (sims_appl_code = 'Sim121') AND (sims_appl_form_field = 'Library Item Status') AND (sims_appl_parameter = a.sims_library_item_status_code)) 
                         AS sims_library_item_status, a.sims_library_item_curcy_code, j.excg_curcy_desc
FROM            sims.sims_library_item_master AS a LEFT OUTER JOIN
                         sims.sims_library_category AS b ON a.sims_library_item_category = b.sims_library_category_code LEFT OUTER JOIN
                         sims.sims_library_subcategory AS c ON a.sims_library_item_subcategory = c.sims_library_subcategory_code LEFT OUTER JOIN
                         sims.sims_library_catelogue AS d ON a.sims_library_item_catalogue_code = d.sims_library_catalogue_code LEFT OUTER JOIN
                         sims.sims_cur AS e ON a.sims_library_item_cur_code = e.sims_cur_code LEFT OUTER JOIN
                         sims.sims_cur_level AS f ON a.sims_library_item_cur_level_code = f.sims_cur_level_code AND a.sims_library_item_cur_code = f.sims_cur_code LEFT OUTER JOIN
                         sims.sims_grade AS g ON a.sims_library_item_grade_code = g.sims_grade_code AND a.sims_library_item_cur_code = g.sims_cur_code AND g.sims_academic_year =
                             (SELECT        sims_academic_year
                               FROM            sims.sims_academic_year
                               WHERE        (sims_academic_year_status = 'C')) LEFT OUTER JOIN
                         sims.sims_language AS h ON a.sims_library_item_language_code = h.sims_language_code LEFT OUTER JOIN
                         sims.sims_library_item_style AS i ON a.sims_library_item_book_style = i.sims_library_item_style_code LEFT OUTER JOIN
                         fins.fins_currency_master AS j ON a.sims_library_item_curcy_code = j.excg_curcy_code



GO
/****** Object:  View [sims].[sims_prospect_list]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

CREATE VIEW [sims].[sims_prospect_list]
AS


select	--Prospect Number Details--
		a.sims_pros_number sims_admission_number, a.sims_pros_application_number, 
		a.sims_pros_date_created, '' AS sims_admission_pros_application_number ,'' AS sims_admission_pros_number,

		--Prospect School Details--
		a.sims_pros_school_code, b.lic_school_name, 
		a.sims_pros_cur_code, c.sims_cur_short_name_en, 
		a.sims_pros_tentative_joining_date , a.sims_pros_academic_year, 
		a.sims_pros_grade_code, d.sims_grade_name_en, 
		'' AS sims_admission_section_code, '' AS sims_section_name_en, 
		a.sims_pros_term_code, f.sims_term_desc_en, 

		--Personal Information--
		--Personal Details of student--
		CONCAT(a.sims_pros_passport_first_name_en, ' ',a.sims_pros_passport_middle_name_en, ' ',a.sims_pros_passport_last_name_en) AS student_name_en,
		CONCAT(a.sims_pros_passport_first_name_ot,' ', a.sims_pros_passport_middle_name_ot, ' ',a.sims_pros_passport_last_name_ot) AS student_name_ot,
		a.sims_pros_dob, a.sims_pros_tentative_joining_date AS sims_admission_commencement_date, 
		a.sims_pros_gender, (SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
								WHERE sims_mod_code = '003' AND sims_appl_form_field = 'Gender' AND 
								sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_pros_gender) AS gender, 
		a.sims_pros_birth_country_code, g.sims_country_name_en,
		a.sims_pros_nationality_code, h.sims_nationality_name_en,
		a.sims_pros_ethnicity_code, i.sims_ethnicity_name_en,
		a.sims_pros_religion_code, j.sims_religion_name_en,
		--Passport Details--
		a.sims_pros_passport_number, a.sims_pros_passport_issue_date, a.sims_pros_passport_expiry_date, 
		a.sims_pros_passport_issuing_authority, a.sims_pros_passport_issue_place,
		--Visa/National ID Information--
		a.sims_pros_visa_type, (SELECT sims_appl_form_field_value1 FROM sims.sims_parameter 
									WHERE sims_mod_code = '004' AND 
									sims_appl_form_field = 'Visa Type' AND 
									sims_appl_code = 'Per099' AND 
									sims_appl_parameter = a.sims_pros_visa_type) AS visa_type,a.sims_pros_visa_number,
		a.sims_pros_visa_issuing_place, a.sims_pros_visa_issuing_authority, a.sims_pros_visa_issue_date,
		a.sims_pros_visa_expiry_date, a.sims_pros_national_id, a.sims_pros_national_id_issue_date, 
		a.sims_pros_national_id_expiry_date,
		--Sibling Information--
		a.sims_pros_sibling_status, a.sims_pros_parent_id, a.sims_pros_sibling_enroll_number, 
		a.sims_pros_sibling_name, a.sims_pros_sibling_dob, sims_pros_sibling_school_code, 
		k.lic_school_name AS lic_sibling_school_name,
		--Father Employee of school--
		a.sims_parent_is_employement_comp_code, 
		--Language details--
		a.sims_pros_main_language_code, l.sims_language_name_en, 
		a.sims_pros_main_language_m, a.sims_pros_main_language_r, 
		a.sims_pros_main_language_s, a.sims_pros_main_language_w, 
	    --Parent Details--
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_pros_father_salutation_code),' ',a.sims_pros_father_first_name,' ',a.sims_pros_father_middle_name,' ',a.sims_pros_father_last_name) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_father_salutation_code),' ',m.sims_parent_father_first_name,' ',m.sims_parent_father_middle_name, ' ',m.sims_parent_father_last_name) END AS father_name,
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_pros_mother_salutation_code),' ',a.sims_pros_mother_first_name,' ',a.sims_pros_mother_middle_name,' ',a.sims_pros_mother_last_name) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_mother_salutation_code),' ',m.sims_parent_mother_first_name,' ',m.sims_parent_mother_middle_name, ' ',m.sims_parent_mother_last_name) END AS mother_name,
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN 
		CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = a.sims_pros_guardian_salutation_code),' ',a.sims_pros_guardian_first_name,' ',a.sims_pros_guardian_middle_name,' ',a.sims_pros_guardian_last_name) 
		ELSE  CONCAT((SELECT sims_appl_form_field_value1 FROM sims.sims_parameter WHERE sims_appl_form_field = 'Salutation' AND sims_appl_code = 'Sim010' AND sims_appl_parameter = m.sims_parent_guardian_salutation_code),' ',m.sims_parent_guardian_first_name,' ',m.sims_parent_guardian_middle_name, ' ',m.sims_parent_guardian_last_name) END AS guardian_name, 
		a.sims_pros_primary_contact_pref, a.sims_pros_fee_payment_contact_pref, 
		a.sims_pros_transport_status, a.sims_pros_transport_desc, 
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN a.sims_pros_father_mobile ELSE m.sims_parent_father_mobile END AS father_mobile, 
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN a.sims_pros_mother_mobile ELSE m.sims_parent_mother_mobile END AS mother_mobile, 
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN a.sims_pros_guardian_mobile ELSE m.sims_parent_guardian_mobile END AS guardian_mobile, 
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN a.sims_pros_father_phone ELSE m.sims_parent_father_phone END AS father_phone, 
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN a.sims_pros_mother_phone ELSE m.sims_parent_mother_phone END AS mother_phone, 
		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN a.sims_pros_guardian_phone ELSE m.sims_parent_guardian_phone END AS guardian_phone, 
	    CASE WHEN NULLIF (a.sims_pros_parent_id, '') IS NULL THEN a.sims_pros_father_email ELSE m.sims_parent_father_email END AS father_email,
	   CASE WHEN NULLIF (a.sims_pros_parent_id, '') IS NULL THEN a.sims_pros_mother_email ELSE  m.sims_parent_mother_email END AS mother_email,
	    CASE WHEN NULLIF (a.sims_pros_parent_id, '') IS NULL THEN a.sims_pros_guardian_email ELSE m.sims_parent_guardian_email END AS guardian_email,



		CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN 
		CONCAT(a.sims_pros_father_appartment_number,' ',a.sims_pros_father_building_number,' ',a.sims_pros_father_street_number,' ',
		a.sims_pros_father_area_number,' ',a.sims_pros_father_city,' ',a.sims_pros_father_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_pros_father_country_code))
		ELSE CONCAT(m.sims_parent_father_appartment_number,' ',m.sims_parent_father_building_number,' ',m.sims_parent_father_street_number,' ',
		m.sims_parent_father_area_number,' ',m.sims_parent_father_city,' ',m.sims_parent_father_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_father_country_code)) END AS father_address,
				CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN 
		CONCAT(a.sims_pros_mother_appartment_number,' ',a.sims_pros_mother_building_number,' ',a.sims_pros_mother_street_number,' ',
		a.sims_pros_mother_area_number,' ',a.sims_pros_mother_city,' ',a.sims_pros_mother_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_pros_mother_country_code))
		ELSE CONCAT(m.sims_parent_mother_appartment_number,' ',m.sims_parent_mother_building_number,' ',m.sims_parent_mother_street_number,' ',
		m.sims_parent_mother_area_number,' ',m.sims_parent_mother_city,' ',m.sims_parent_mother_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_mother_country_code)) END AS mother_address,
				CASE WHEN NULLIF(a.sims_pros_parent_id,'') IS NULL THEN 
		CONCAT(a.sims_pros_guardian_appartment_number,' ',a.sims_pros_guardian_building_number,' ',a.sims_pros_guardian_street_number,' ',
		a.sims_pros_guardian_area_number,' ',a.sims_pros_guardian_city,' ',a.sims_pros_guardian_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = a.sims_pros_guardian_country_code))
		ELSE CONCAT(m.sims_parent_guardian_appartment_number,' ',m.sims_parent_guardian_building_number,' ',m.sims_parent_guardian_street_number,' ',
		m.sims_parent_guardian_area_number,' ',m.sims_parent_guardian_city,' ',m.sims_parent_guardian_state,' ',(select sc.sims_country_name_en from sims.sims_country sc WHERE sc.sims_country_code = m.sims_parent_guardian_country_code)) END AS guardian_address,


		--Previous School Details--
		a.sims_pros_current_school_name, a.sims_pros_current_school_enroll_number, 
		a.sims_pros_current_school_grade, a.sims_pros_current_school_cur, 
		a.sims_pros_current_school_from_date, a.sims_pros_current_school_to_date, 
		a.sims_pros_current_school_language, a.sims_pros_current_school_phone, 
		a.sims_pros_current_school_address, a.sims_pros_current_school_city,
		(SELECT sc.sims_country_name_en FROM sims.sims_country AS sc WHERE sc.sims_country_code = a.sims_pros_current_school_country_code) AS sims_pros_current_school_country_name,

		--Legal Custoday Details--
		a.sims_pros_legal_custody, 
		--Health Details--
		a.sims_pros_health_card_number, a.sims_pros_health_card_issue_date, a.sims_pros_health_card_expiry_date, 
		a.sims_pros_blood_group_code, (select sims_appl_form_field_value1 from sims.sims_parameter where sims_mod_code = '003'  AND sims_appl_code = 'Sim010' AND
											sims_appl_form_field = 'BloodGroup' AND sims_appl_parameter = a.sims_pros_blood_group_code) AS blood_group, 
		a.sims_pros_health_restriction_status,a.sims_pros_health_restriction_desc, 
		a.sims_pros_disability_status, a.sims_pros_disability_desc, 
		a.sims_pros_medication_status, a.sims_pros_medication_desc, 
		a.sims_pros_health_other_status, a.sims_pros_health_other_desc, 
		a.sims_pros_health_hearing_status, a.sims_pros_health_hearing_desc, 
		a.sims_pros_health_vision_status, a.sims_pros_health_vision_desc, 
		--Other Details--
		a.sims_pros_gifted_status, a.sims_pros_gifted_desc, 
		a.sims_pros_music_status, a.sims_pros_music_desc, 
		a.sims_pros_sports_status, a.sims_pros_sports_desc, 
		a.sims_pros_language_support_status, a.sims_pros_language_support_desc,

		--Status--
		a.sims_pros_status,
		CASE WHEN a.sims_pros_status = 'A' THEN 'ACTIVE'
			 WHEN a.sims_pros_status = 'I' THEN 'INACTIVE'
			 WHEN a.sims_pros_status = 'P' THEN 'PROMTED FOR ADMISSION'
			 WHEN a.sims_pros_status = 'R' THEN 'REJECTED' END AS sims_admission_status_value,
		sims_pros_health_respiratory_status 
,sims_pros_health_respiratory_desc 
,sims_pros_health_hay_fever_status 
,sims_pros_health_hay_fever_desc 
,sims_pros_health_epilepsy_status 
,sims_pros_health_epilepsy_desc 
,sims_pros_health_skin_status 
,sims_pros_health_skin_desc 
,sims_pros_health_diabetes_status 
,sims_pros_health_diabetes_desc 
,sims_pros_health_surgery_status 
,sims_pros_health_surgery_desc 
,sims_pros_attribute3 
,sims_pros_attribute4 
,sims_pros_father_national_id 
,sims_pros_mother_national_id 
,sims_pros_guardian_national_id 
,sims_pros_organizational_skills 
,sims_pros_social_skills 
,sims_pros_academic_standard 
,sims_pros_special_education 
,sims_pros_attribute5 
,sims_pros_attribute6 
,sims_pros_attribute7 
,sims_pros_attribute8 
,sims_pros_attribute9 
,sims_pros_attribute10 
,sims_pros_attribute11 
,sims_pros_attribute12 
,sims_pros_attribute1 
,sims_pros_attribute2 

FROM	sims.sims_prospect AS a  INNER JOIN mogra.mogra_license_details AS b ON 
		a.sims_pros_school_code = b.lic_school_code INNER JOIN sims.sims_cur AS c ON 
		a.sims_pros_cur_code = c.sims_cur_code LEFT OUTER JOIN sims.sims_grade AS d ON 
		a.sims_pros_cur_code = d.sims_cur_code AND 
		a.sims_pros_academic_year = d.sims_academic_year AND 
		a.sims_pros_grade_code = d.sims_grade_code LEFT OUTER JOIN sims.sims_term AS f ON 
		a.sims_pros_academic_year = f.sims_academic_year AND 
		a.sims_pros_cur_code = f.sims_cur_code AND 
		a.sims_pros_term_code = f.sims_term_code LEFT OUTER JOIN sims.sims_country AS g ON 
		a.sims_pros_birth_country_code = g.sims_country_code LEFT OUTER JOIN sims.sims_nationality AS h ON 
		a.sims_pros_nationality_code = h.sims_nationality_code LEFT OUTER JOIN sims.sims_ethnicity AS i ON 
		a.sims_pros_ethnicity_code = i.sims_ethnicity_code LEFT OUTER JOIN sims.sims_religion AS j ON 
		a.sims_pros_religion_code = j.sims_religion_code LEFT OUTER JOIN mogra.mogra_license_details AS k ON
		a.sims_pros_sibling_school_code = k.lic_school_code LEFT OUTER JOIN sims.sims_language AS l ON 
		a.sims_pros_main_language_code = l.sims_language_code LEFT OUTER JOIN sims.sims_parent AS m ON 
		a.sims_pros_parent_id = m.sims_parent_number







GO
/****** Object:  View [sims].[student_history]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [sims].[student_history]
AS
SELECT        sims.sims_student.sims_student_enroll_number, sims.sims_student_section.sims_academic_year, sims.sims_student_section.sims_grade_code, 
                         sims.sims_student.sims_student_date, sims.sims_student.sims_student_commence_date, sims.sims_student_history.sims_student_status_updation_date,
                             (SELECT        sims_grade_name_en
                               FROM            sims.sims_grade
                               WHERE        (sims_grade_code = sims.sims_student_section.sims_grade_code) and sims_academic_year = sims.sims_student_section.sims_academic_year) AS grade_name, sims.sims_student_section.sims_section_code,
                             (SELECT        sims_section_name_en
                               FROM            sims.sims_section
                               WHERE        (sims_section_code = sims.sims_student_section.sims_section_code) and sims_academic_year = sims.sims_student_section.sims_academic_year and sims_grade_code = sims.sims_student_Section.sims_grade_code) AS section_name, sims.sims_student.sims_student_cur_code,
                             (SELECT        sims_cur_short_name_en
                               FROM            sims.sims_cur
                               WHERE        (sims_cur_code = sims.sims_student.sims_student_cur_code)) AS cur_shrt_name, 
                         sims.sims_student.sims_student_passport_middle_name_en + ' ' + sims.sims_student.sims_student_passport_first_name_en + ' ' + sims.sims_student.sims_student_passport_last_name_en
                          AS student_name,
                             (SELECT        sims_appl_form_field_value1
                               FROM            sims.sims_parameter
                               WHERE        (sims_appl_code = 'Sim010') AND (sims_appl_form_field = 'Gender') AND (sims_appl_parameter = sims.sims_student.sims_student_gender)) 
                         AS gender, sims.sims_student.sims_student_gender, sims.sims_student.sims_student_religion_code,
                             (SELECT        sims_religion_name_en
                               FROM            sims.sims_religion
                               WHERE        (sims_religion_code = sims.sims_student.sims_student_religion_code)) AS std_religion, sims.sims_student.sims_student_dob, 
                         sims.sims_student.sims_student_birth_country_code,
                             (SELECT        sims_country_name_en
                               FROM            sims.sims_country
                               WHERE        (sims_country_code = sims.sims_student.sims_student_birth_country_code)) AS country_name, 
                         sims.sims_student.sims_student_nationality_code,
                             (SELECT        sims_nationality_name_en
                               FROM            sims.sims_nationality
                               WHERE        (sims_nationality_code = sims.sims_student.sims_student_nationality_code)) AS nationality_name, sims.sims_student.sims_student_img, 
                         sims.sims_student.sims_student_academic_status, sims.sims_student_history.sims_student_remark, 
                         sims.sims_student_history.sims_student_academic_status AS academic_status_history
FROM            sims.sims_student_section INNER JOIN
                         sims.sims_student ON sims.sims_student_section.sims_enroll_number = sims.sims_student.sims_student_enroll_number RIGHT OUTER JOIN
                         sims.sims_student_history ON sims.sims_student.sims_student_enroll_number = sims.sims_student_history.sims_student_enroll_number



GO
/****** Object:  View [sims].[studentNationality]    Script Date: 07-08-2019 13:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- View

CREATE VIEW [sims].[studentNationality]
AS
SELECT        c.sims_cur_short_name_en AS Curriculum, b.sims_academic_year_description AS [Academic Year], d.sims_grade_name_en AS Grade, e.sims_section_name_en AS Section, a.sims_enroll_number AS [Enroll No],
                          ISNULL(f.sims_student_passport_first_name_en, N'') + ISNULL(f.sims_student_passport_middle_name_en, N'') + ISNULL(f.sims_student_passport_last_name_en, N'') AS [Student Name], 
                         ISNULL(h.sims_country_name_en, N'Not Available') AS Country,
				f.sims_student_gender as [Gender]
FROM            sims.sims_student_section AS a INNER JOIN
                         sims.sims_academic_year AS b ON a.sims_cur_code = b.sims_cur_code AND a.sims_academic_year = b.sims_academic_year INNER JOIN
                         sims.sims_cur AS c ON a.sims_cur_code = c.sims_cur_code INNER JOIN
                         sims.sims_grade AS d ON a.sims_cur_code = d.sims_cur_code AND a.sims_academic_year = d.sims_academic_year AND a.sims_grade_code = d.sims_grade_code INNER JOIN
                         sims.sims_section AS e ON a.sims_cur_code = e.sims_cur_code AND a.sims_academic_year = e.sims_academic_year AND a.sims_grade_code = e.sims_grade_code AND 
                         a.sims_section_code = e.sims_section_code INNER JOIN
                         sims.sims_student AS f ON a.sims_cur_code = f.sims_student_cur_code AND a.sims_enroll_number = f.sims_student_enroll_number LEFT OUTER JOIN
                         sims.sims_nationality AS g ON f.sims_student_nationality_code = g.sims_nationality_code LEFT OUTER JOIN
                         sims.sims_country AS h ON g.sims_country_code = h.sims_country_code
WHERE        (a.sims_allocation_status = 'A')


GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 314
               Bottom = 136
               Right = 579
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 617
               Bottom = 136
               Right = 840
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 284
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 138
               Left = 322
               Bottom = 234
               Right = 541
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   B' , @level0type=N'SCHEMA',@level0name=N'comn', @level1type=N'VIEW',@level1name=N'comn_user_audit_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'egin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'comn', @level1type=N'VIEW',@level1name=N'comn_user_audit_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'comn', @level1type=N'VIEW',@level1name=N'comn_user_audit_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pays_employee (pays)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'pays_employee_report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'pays_employee_report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[6] 2[35] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'sims_collab_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'sims_collab_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'sims_email_daily_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'sims_email_daily_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'sims_email_schedule_daily_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'sims_email_schedule_daily_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[59] 4[14] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_student (sims)"
            Begin Extent = 
               Top = 1
               Left = 742
               Bottom = 131
               Right = 1062
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_transport_route (sims)"
            Begin Extent = 
               Top = 0
               Left = 440
               Bottom = 130
               Right = 723
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "sims_transport_route_stop (sims)"
            Begin Extent = 
               Top = 160
               Left = 636
               Bottom = 290
               Right = 944
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "sims_transport_stop (sims)"
            Begin Extent = 
               Top = 146
               Left = 20
               Bottom = 276
               Right = 269
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_transport_route_student (sims)"
            Begin Extent = 
               Top = 0
               Left = 0
               Bottom = 130
               Right = 323
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "sims_transport_vehicle (sims)"
            Begin Extent = 
               Top = 204
               Left = 294
               Bottom = 334
               Right = 631
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefau' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'lts = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[23] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_workflow_detail (sims)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 269
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "sims_workflow_details_employee (sims)"
            Begin Extent = 
               Top = 6
               Left = 307
               Bottom = 136
               Right = 562
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "sims_workflow_employee_transaction (sims)"
            Begin Extent = 
               Top = 6
               Left = 600
               Bottom = 136
               Right = 841
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pays_employee (pays)"
            Begin Extent = 
               Top = 152
               Left = 35
               Bottom = 282
               Right = 325
            End
            DisplayFlags = 280
            TopColumn = 15
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2685
         Width = 1500
         Width = 1650
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'        Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[13] 4[3] 2[39] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 27
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'fins', @level1type=N'VIEW',@level1name=N'fins_pdc_detail_realize'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'fins', @level1type=N'VIEW',@level1name=N'fins_pdc_detail_realize'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 328
            End
            DisplayFlags = 280
            TopColumn = 42
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 56
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
  ' , @level0type=N'SCHEMA',@level0name=N'pays', @level1type=N'VIEW',@level1name=N'pays_employee_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'pays', @level1type=N'VIEW',@level1name=N'pays_employee_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'pays', @level1type=N'VIEW',@level1name=N'pays_employee_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 297
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 335
               Bottom = 135
               Right = 555
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 593
               Bottom = 135
               Right = 792
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_circular_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_circular_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_class_teacher (sims)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 256
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_teacher (sims)"
            Begin Extent = 
               Top = 6
               Left = 294
               Bottom = 135
               Right = 515
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_class_teacher_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_class_teacher_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_fee_ledger'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_fee_ledger'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -958
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 319
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 357
               Bottom = 136
               Right = 564
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 602
               Bottom = 136
               Right = 822
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 362
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 138
               Left = 400
               Bottom = 268
               Right = 801
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 310
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = ' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_gradebook_category_assignment_student_marks_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_gradebook_category_assignment_student_marks_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_gradebook_category_assignment_student_marks_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_student_section (sims)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_student (sims)"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 136
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_house (sims)"
            Begin Extent = 
               Top = 6
               Left = 640
               Bottom = 136
               Right = 826
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_house_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_house_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 332
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 370
               Bottom = 101
               Right = 607
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 645
               Bottom = 101
               Right = 901
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 102
               Left = 370
               Bottom = 214
               Right = 614
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 102
               Left = 652
               Bottom = 231
               Right = 872
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 216
               Left = 293
               Bottom = 345
               Right = 494
            End
            DisplayFlags = 280
            TopColumn = 0
         End
 ' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_item_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'        Begin Table = "h"
            Begin Extent = 
               Top = 234
               Left = 532
               Bottom = 363
               Right = 752
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 365
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 348
               Left = 320
               Bottom = 477
               Right = 514
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_item_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_item_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 311
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 349
               Bottom = 135
               Right = 750
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 233
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 138
               Left = 295
               Bottom = 267
               Right = 579
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 138
               Left = 617
               Bottom = 267
               Right = 907
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_membership_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_membership_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_membership_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_library_transaction_detail (sims)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 335
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_library_transaction (sims)"
            Begin Extent = 
               Top = 0
               Left = 637
               Bottom = 130
               Right = 910
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_library_item_master (sims)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 332
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_library_membership_list (sims)"
            Begin Extent = 
               Top = 138
               Left = 370
               Bottom = 267
               Right = 621
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupB' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_transaction_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'y = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_transaction_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_library_transaction_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 416
               Bottom = 136
               Right = 656
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 694
               Bottom = 136
               Right = 909
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 363
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 138
               Left = 401
               Bottom = 268
               Right = 642
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 138
               Left = 680
               Bottom = 268
               Right = 897
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "h"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
  ' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_report_card_category_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Begin Table = "i"
            Begin Extent = 
               Top = 270
               Left = 330
               Bottom = 400
               Right = 597
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 270
               Left = 635
               Bottom = 400
               Right = 860
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "k"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 263
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 32
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_report_card_category_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_report_card_category_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 416
               Bottom = 136
               Right = 656
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 694
               Bottom = 136
               Right = 909
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_report_card_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_report_card_details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "st"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 358
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 396
               Bottom = 102
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ss"
            Begin Extent = 
               Top = 102
               Left = 396
               Bottom = 232
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_student_parent_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'sims_student_parent_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -2110
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 377
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 415
               Bottom = 135
               Right = 680
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 718
               Bottom = 135
               Right = 940
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 99
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
   ' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_attendance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_attendance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_attendance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_student_section (sims)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_student (sims)"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 135
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_student_history (sims)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 343
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_history'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_history'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sims_student (sims)"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 135
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_student_section (sims)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_student_parent_list (sims)"
            Begin Extent = 
               Top = 6
               Left = 640
               Bottom = 135
               Right = 880
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sims_parent (sims)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 439
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 112
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
    ' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'student_list'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 136
               Right = 542
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 580
               Bottom = 136
               Right = 800
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 838
               Bottom = 136
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 306
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 138
               Left = 344
               Bottom = 268
               Right = 664
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 138
               Left = 702
               Bottom = 268
               Right = 929
            End
            DisplayFlags = 280
            TopColumn = 0
         End
  ' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'studentNationality'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Begin Table = "h"
            Begin Extent = 
               Top = 6
               Left = 1077
               Bottom = 136
               Right = 1292
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 11520
         Alias = 1515
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'studentNationality'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'sims', @level1type=N'VIEW',@level1name=N'studentNationality'
GO
