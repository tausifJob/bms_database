USE [BMS_ASD]
GO
/****** Object:  User [BMS_USR_ADS]    Script Date: 07-08-2019 13:55:01 ******/
CREATE USER [BMS_USR_ADS] FOR LOGIN [BMS_USR_ADS] WITH DEFAULT_SCHEMA=[BMS_USR_ADS]
GO
/****** Object:  User [BMS_USR_ASD]    Script Date: 07-08-2019 13:55:01 ******/
CREATE USER [BMS_USR_ASD] FOR LOGIN [BMS_USR_ASD] WITH DEFAULT_SCHEMA=[BMS_USR_ASD]
GO
/****** Object:  User [BMS_USR_BRS]    Script Date: 07-08-2019 13:55:01 ******/
CREATE USER [BMS_USR_BRS] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[BMS_USR_BRS]
GO
/****** Object:  User [BMS_USR_NISS]    Script Date: 07-08-2019 13:55:01 ******/
CREATE USER [BMS_USR_NISS] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[BMS_USR_NISS]
GO
/****** Object:  User [BMS_USR_SISO]    Script Date: 07-08-2019 13:55:02 ******/
CREATE USER [BMS_USR_SISO] FOR LOGIN [BMS_USR_SISO] WITH DEFAULT_SCHEMA=[BMS_USR_SISO]
GO
/****** Object:  User [BRS_USR_lems]    Script Date: 07-08-2019 13:55:02 ******/
CREATE USER [BRS_USR_lems] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [mograsis_admin]    Script Date: 07-08-2019 13:55:02 ******/
CREATE USER [mograsis_admin] FOR LOGIN [mograsis_admin] WITH DEFAULT_SCHEMA=[mograsis_admin]
GO
/****** Object:  User [SQL_USR_AMITY]    Script Date: 07-08-2019 13:55:02 ******/
CREATE USER [SQL_USR_AMITY] FOR LOGIN [SQL_USR_AMITY] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SQL_USR_LEMS]    Script Date: 07-08-2019 13:55:02 ******/
CREATE USER [SQL_USR_LEMS] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BMS_USR_ADS]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [BMS_USR_ADS]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BMS_USR_ADS]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [BMS_USR_ADS]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BMS_USR_ASD]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [BMS_USR_ASD]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BMS_USR_ASD]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [BMS_USR_ASD]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BMS_USR_BRS]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [BMS_USR_BRS]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BMS_USR_BRS]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [BMS_USR_BRS]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BMS_USR_NISS]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [BMS_USR_NISS]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BMS_USR_NISS]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [BMS_USR_NISS]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BMS_USR_SISO]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [BMS_USR_SISO]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BMS_USR_SISO]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [BMS_USR_SISO]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BRS_USR_lems]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [BRS_USR_lems]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BRS_USR_lems]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [BRS_USR_lems]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [mograsis_admin]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [mograsis_admin]
GO
ALTER ROLE [db_datareader] ADD MEMBER [mograsis_admin]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [mograsis_admin]
GO
/****** Object:  Schema [apps]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [apps]
GO
/****** Object:  Schema [BMS_USR_ADS]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [BMS_USR_ADS]
GO
/****** Object:  Schema [BMS_USR_ASD]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [BMS_USR_ASD]
GO
/****** Object:  Schema [BMS_USR_BRS]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [BMS_USR_BRS]
GO
/****** Object:  Schema [BMS_USR_NISS]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [BMS_USR_NISS]
GO
/****** Object:  Schema [BMS_USR_SISO]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [BMS_USR_SISO]
GO
/****** Object:  Schema [comn]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [comn]
GO
/****** Object:  Schema [crm]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [crm]
GO
/****** Object:  Schema [fins]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [fins]
GO
/****** Object:  Schema [invs]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [invs]
GO
/****** Object:  Schema [mdl]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [mdl]
GO
/****** Object:  Schema [mogra]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [mogra]
GO
/****** Object:  Schema [mograsis_admin]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [mograsis_admin]
GO
/****** Object:  Schema [pays]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [pays]
GO
/****** Object:  Schema [purs]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [purs]
GO
/****** Object:  Schema [sims]    Script Date: 07-08-2019 13:55:02 ******/
CREATE SCHEMA [sims]
GO
/****** Object:  UserDefinedFunction [dbo].[FindInWeek]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- User Defined Function

	 CREATE Function [dbo].[FindInWeek] (@givendate datetime)-- select [dbo].[FindInWeek]('01-01-2014')
returns varchar(15) 
as
begin
declare @firstDayOfMonth varchar(20),
@findWeek int,@weeks varchar(30)
set @firstDayOfMonth = cast(month(@givenDate) as varchar(2))+'/'+'1'
+'/'+ cast(year(@givenDate) as varchar(4))
set @findWeek= datepart(wk, @givendate) - datepart(wk, @firstDayOfMonth) + 1
set @weeks=case @findWeek
when 1 then 'First'
when 2 then 'Second'
when 3 then 'Third'
when 4 then 'Fourth'
when 5 then 'Fifth'
when 6 then 'Sixth'
else 'Seventh' end
return @weeks+' Week'
end

GO
/****** Object:  UserDefinedFunction [dbo].[fn_dates]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_dates](@from AS DATETIME, @to AS DATETIME)
  RETURNS @Dates TABLE(dt DATETIME NOT NULL PRIMARY KEY)
AS
BEGIN
  DECLARE @rc AS INT
  SET @rc = 1

  INSERT INTO @Dates VALUES(@from)

  WHILE @from + @rc * 2 - 1 <= @to
  BEGIN
    INSERT INTO @Dates
      SELECT dt + @rc FROM @Dates

    SET @rc = @rc * 2
  END

  INSERT INTO @Dates
    SELECT dt + @rc FROM @Dates
    WHERE dt + @rc <= @to

  RETURN
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_parsehtml]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_parsehtml] 
(
@htmldesc varchar(max)
) 
returns varchar(max)
as
begin
declare @first int, @last int,@len int 
set @first = CHARINDEX('<',@htmldesc) 
set @last = CHARINDEX('>',@htmldesc,CHARINDEX('<',@htmldesc)) 
set @len = (@last - @first) + 1 
while @first > 0 AND @last > 0 AND @len > 0 
begin 
---Stuff function is used to insert string at given position and delete number of characters specified from original string
set @htmldesc = STUFF(@htmldesc,@first,@len,'')  
SET @first = CHARINDEX('<',@htmldesc) 
set @last = CHARINDEX('>',@htmldesc,CHARINDEX('<',@htmldesc)) 
set @len = (@last - @first) + 1 
end 
return LTRIM(RTRIM(@htmldesc)) 
end 

GO
/****** Object:  UserDefinedFunction [dbo].[fn_RemoveHTMLFromText]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--use Production
--select * from sims.sims_circular



CREATE FUNCTION [dbo].[fn_RemoveHTMLFromText] (@inputString nvarchar(max))
RETURNS nvarchar(MAX)

AS
BEGIN

  /*Variables to store source fielde temporarily and to remove tags one by one*/
  DECLARE @replaceHTML nvarchar(2000), @counter int, @outputString nvarchar(max)
  set @counter = 0
  SET @outputString = @inputString

  /*This was extra case which I've added later to remove no-break space*/
  SET @outputString = REPLACE(@outputString, '&nbsp;', '')

  /*This loop searches for tags beginning with "<" and ending with ">" */
  WHILE (CHARINDEX('<', @outputString,1)>0 AND CHARINDEX('>', @outputString,1)>0)
  BEGIN
    SET @counter = @counter + 1

    /*
    Some math here... looking for tags and taking substring storing result into temporarily variable, for example "</span>" 
   */
   SELECT @replaceHTML = SUBSTRING(@outputString, CHARINDEX('<', @outputString,1), CHARINDEX('>',   @outputString,1)-CHARINDEX('<', @outputString,1)+1)

   /* Replace the tag that we stored in previous step */
   SET @outputString = REPLACE(@outputString, @replaceHTML, '') 

   /* Let's clear our variable just in case... */
   SET @replaceHTML = ''

   /* Let's set up maximum number of tags just for fun breaking the loop after 15 tags */
  if @counter >15
      RETURN(@outputString);

  END

  RETURN(@outputString);

END 
 


GO
/****** Object:  UserDefinedFunction [dbo].[fnAccessCriteriaCheck]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fnAccessCriteriaCheck]
    (
      @sims_appl_code NVARCHAR(6) ,
      @sims_academic_year NVARCHAR(4) ,
      @comn_user_code NVARCHAR(10)
    )
RETURNS @List TABLE
    (
	  sims_academic_year NVARCHAR(4),
      sims_grade_code NVARCHAR(2) ,
      sims_section_code NVARCHAR(4)
    )
    BEGIN
        DECLARE @opr NVARCHAR(1) = 'S';

        IF ( UPPER(@opr) = 'S' )
            BEGIN
                IF ( ( SELECT   sp_read
                       FROM     comn.comn_master_sp
                       WHERE    sp_name = 'fnAccessCriteriaCheck'
                     ) = 'Y' )
                    BEGIN
                        INSERT  INTO @List
                                SELECT DISTINCT
										sims_academic_year,
                                        sims_grade_code ,
                                        sims_section_code
                                FROM    sims.sims_access_criteria a ,
                                        sims.sims_access_application b ,
                                        sims.sims_access_application_mapping c ,
                                        sims.sims_access_class d
                                WHERE   a.sims_access_code = b.sims_access_code
                                        AND b.sims_access_code = c.sims_access_code
                                        AND b.sims_access_code = d.sims_access_code
                                        AND a.sims_access_status IN ( 'A', 'Y',
                                                              '1' )
                                        AND b.sims_status IN ( 'A', 'Y', '1' )
                                        AND d.sims_status IN ( 'A', 'Y', '1' )
                                        AND sims_appl_code = @sims_appl_code
                                        AND sims_academic_year = @sims_academic_year
                                        AND sims_access_application_mapping_user_code = @comn_user_code;
                    END;
            END;
        ELSE
            BEGIN
	
                EXECUTE NotifyError 'I', 'FN', '[dbo].[fnAccessCriteriaCheck]',
                    '',
                    'Tbl comn_master_sp does not contain record for sp [fnAccessCriteriaCheck] or not activated it',
                    '', '', 'Fn';
            END;

        RETURN;
    END;


GO
/****** Object:  UserDefinedFunction [dbo].[fnAllDaysBetDates]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shrutika>
-- Create date: <29-1-2018>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnAllDaysBetDates] (@from_date date, @to_date date)

RETURNS nvarchar(max)
AS
BEGIN
	
	DECLARE 
	
	 @AllDays nvarchar(max),
	 @TOTALCount INT

    SET @from_date = DATEADD(DAY,-1,@from_date)
    Select  @TOTALCount= DATEDIFF(DD,@from_date,@to_date);


    ;WITH d AS 
            (
              SELECT top (@TOTALCount) AllDays = DATEADD(DAY, ROW_NUMBER() 
                OVER (ORDER BY object_id), REPLACE(@from_date,'-',''))
              FROM sys.all_objects
            )
		
        SELECT @AllDays=stuff((select ','+(cast(Day(AllDays) as nvarchar)+cast(Month(AllDays) as nvarchar)) From d  order by AllDays for xml path(''),type).value('.','nvarchar(max)'),1,1,'')
			
	RETURN @AllDays

END


GO
/****** Object:  UserDefinedFunction [dbo].[fnCalculateAge]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnCalculateAge]--select [dbo].[fnCalculateAge]('01-01-2014','01-01-2015')
(
	@from_date date,
	@to_date date 
)
RETURNS nvarchar(50)
AS 
BEGIN 
	DECLARE @age nvarchar(50), @tmpdate date, @years int, @months int, @days int
	IF(@to_date >  @from_date)
	BEGIN
		SELECT @tmpdate = @from_date

		SELECT @years = DATEDIFF(yy, @tmpdate, @to_date) - CASE WHEN (MONTH(@from_date) > MONTH(@to_date)) OR (MONTH(@from_date) = MONTH(@to_date) AND DAY(@from_date) > DAY(@to_date)) THEN 1 ELSE 0 END
		SELECT @tmpdate = DATEADD(yy, @years, @tmpdate)
		SELECT @months = DATEDIFF(m, @tmpdate, @to_date) - CASE WHEN DAY(@from_date) > DAY(@to_date) THEN 1 ELSE 0 END
		SELECT @tmpdate = DATEADD(m, @months, @tmpdate)
		SELECT @days = DATEDIFF(d, @tmpdate, @to_date)
		IF(@years = 0 AND @months = 0)
					SELECT @age = CAST(@days AS nvarchar(2)) + ' Days'
		ELSE IF(@years = 0 AND @days = 0)
					SELECT @age = CAST(@months AS nvarchar(2)) + ' Months '
		ElSE IF(@months = 0 AND @days = 0)
					SELECT @age = CAST(@years AS nvarchar(5)) + ' Years '
		ELSE IF(@years = 0)
					SELECT @age = CAST(@months AS nvarchar(2)) + ' Months ' + CAST(@days AS nvarchar(2)) + ' Days'
		ELSE IF(@months = 0)
					SELECT @age = CAST(@years AS nvarchar(5)) + ' Years ' + CAST(@days AS nvarchar(2)) + ' Days'
		ElSE IF(@days = 0)
					SELECT @age = CAST(@years AS nvarchar(5)) + ' Years ' + CAST(@months AS nvarchar(2)) + ' Months '
		ElSE 
					SELECT @age = CAST(@years AS nvarchar(5)) + ' Years ' + CAST(@months AS nvarchar(2)) + ' Months ' + CAST(@days AS nvarchar(2)) + ' Days'
	END
	ElSE 
	SELECT @age = 'Please select appropriate date'
	return @age
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnCalculateStudentYear]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[fnCalculateStudentYear]--select [dbo].[fnCalculateAge]('01-01-2014','01-01-2015')
(
	@from_date date,
	@to_date date 
)
RETURNS nvarchar(50)
AS 
BEGIN 
	DECLARE @age nvarchar(50), @tmpdate date, @years int, @months int, @days int
	IF(@to_date >  @from_date)
	BEGIN
		SELECT @tmpdate = @from_date

		SELECT @years = DATEDIFF(yy, @tmpdate, @to_date) - CASE WHEN (MONTH(@from_date) > MONTH(@to_date)) OR (MONTH(@from_date) = MONTH(@to_date) AND DAY(@from_date) > DAY(@to_date)) THEN 1 ELSE 0 END
		SELECT @tmpdate = DATEADD(yy, @years, @tmpdate)
		SELECT @months = DATEDIFF(m, @tmpdate, @to_date) - CASE WHEN DAY(@from_date) > DAY(@to_date) THEN 1 ELSE 0 END
		SELECT @tmpdate = DATEADD(m, @months, @tmpdate)
		SELECT @days = DATEDIFF(d, @tmpdate, @to_date)
		
		
		IF(@months = 0 AND @days = 0)
					SELECT @age = CAST(@years AS nvarchar(5)) + ' Years '

		ELSE IF(@years = 0 AND @days = 0)
					SELECT @age = CAST(@months AS nvarchar(2)) + ' Months '
		
		ElSE 
					--SELECT @age = CAST(@years AS nvarchar(5)) + CAST(@months AS nvarchar(2))
					SELECT @age = CAST(@years*12+@months AS nvarchar(5))


		--IF(@months = 0 AND @days = 0)
		--			SELECT @age = CAST(@years AS nvarchar(5)) + ' Years '
		
		--ElSE 
		--			SELECT @age = CAST(@years AS nvarchar(5))
		
		--IF(@age>0)
		--BEGIN
		--	CASE WHEN	@age <=5 and  @age>=1	THEN 
		--					'Age Gruop 1-5'
		--		 WHEN	@age >=6 and  @age<=10	THEN 
		--		 			'Age Gruop 6-10'
		--		 WHEN	@age >=11 and  @age<=15 THEN 
		--		 			'Age Gruop 11-15'
		--		 WHEN	@age >=16 and  @age<=20 THEN 
		--					'Age Gruop 16-20'
		--	END as	'Group 1'
		--END
			
		--	SET @age='Group 1'
				
	END

	ElSE SELECT @age = 'Please select appropriate date'
		return @age

END

GO
/****** Object:  UserDefinedFunction [dbo].[fnConvert_TitleCase]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnConvert_TitleCase] (@InputString VARCHAR(4000) )
RETURNS VARCHAR(4000)
AS
BEGIN
DECLARE @Index INT
DECLARE @Char CHAR(1)
DECLARE @OutputString VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 2
SET @OutputString = STUFF(@OutputString, 1, 1,UPPER(SUBSTRING(@InputString,1,1)))

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char = SUBSTRING(@InputString, @Index, 1)
    IF @Char IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&','''','(')
    IF @Index + 1 <= LEN(@InputString)
BEGIN
    IF @Char != ''''
    OR
    UPPER(SUBSTRING(@InputString, @Index + 1, 1)) != 'S'
    SET @OutputString =
    STUFF(@OutputString, @Index + 1, 1,UPPER(SUBSTRING(@InputString, @Index + 1, 1)))
END
    SET @Index = @Index + 1
END

RETURN ISNULL(@OutputString,'')
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnDateName]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[fnDateName]--select [dbo].[fnCalculateAge]('01-01-2014','01-01-2015')
(
	@from_date date
)
RETURNS nvarchar(200)
AS 
BEGIN 
		DECLARE @date_name nvarchar(200), 
				@a int
				
				SELECT @a = day(@from_date)
				
				IF(@a = 1)
				SELECT @date_name = 'First ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 2)
				SELECT @date_name = 'Second ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 3)
				SELECT @date_name = 'Third ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 4)
				SELECT @date_name = 'Fourth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 5)
				SELECT @date_name = 'Fifth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 6)
				SELECT @date_name = 'Sixth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 7)
				SELECT @date_name = 'Seventh ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 8)
				SELECT @date_name = 'Eighth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 9)
				SELECT @date_name = 'Ninth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 10)
				SELECT @date_name = 'Tenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 11)
				SELECT @date_name = 'Eleventh ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 12)
				SELECT @date_name = 'Twelth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 13)
				SELECT @date_name = 'Thirteenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 14)
				SELECT @date_name = 'Fourteenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 15)
				SELECT @date_name = 'Fifteenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 16)
				SELECT @date_name = 'Sixteenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 17)
				SELECT @date_name = 'Seventeenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 18)
				SELECT @date_name = 'Eighteenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 19)
				SELECT @date_name = 'Nineteenth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 20)
				SELECT @date_name = 'Twentieth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 21)
				SELECT @date_name = 'Twenty First ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 22)
				SELECT @date_name = 'Twenty Second ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 23)
				SELECT @date_name = 'Twenty Third ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 24)
				SELECT @date_name = 'Twenty Fourth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 25)
				SELECT @date_name = 'Twenty Fifth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))	
				IF(@a = 26)
				SELECT @date_name = 'Twenty Sixth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 27)
				SELECT @date_name = 'Twenty Seventh ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 28)
				SELECT @date_name = 'Twenty Eighth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 29)
				SELECT @date_name = 'Twenty Ninth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 30)
				SELECT @date_name = 'Thirtieth ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))
				IF(@a = 31)
				SELECT @date_name = 'Thiry First ' + DATENAME(MONTH,@from_date) + ' ' + dbo.fnNumToWords(DATEPART(year,@from_date))

		RETURN @date_name
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnMax2AvgOutof3]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE function [dbo].[fnMax2AvgOutof3] (@a as nvarchar(7),@b as nvarchar(7),@c as nvarchar(7))
returns nvarchar(7)
as
BEGIN

declare
--@a nvarchar(7)=null,
--@b nvarchar(7)=null,
--@c nvarchar(7)=null,
@max1 nvarchar(7)=null,
@max2 nvarchar(7)=null,
@max numeric(8,2)=null


set @max1=(select 
case 
when(@a>@b and @a>@c)then
(select @a)
when(@b>@a and @b>@c)then
(select @b)
else
@c
end as max1)
--print @max1


set @max2=(select 
case 
when((@a>@b and @a<@c) or (@a<@b and @a>@c) and @a!=@max1)then
(select @a)
when((@b>@a and @b<@c) or (@b<@a and @b>@c) and @b!=@max1)then
(select @b)
else
@c
end as max2)
--print @max2

set @max=(cast(@max1 as numeric(8,2))+ cast(@max2 as numeric(8,2)))/2
   return @max
  
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnNumToWords]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fnNumToWords] (

	@Number Numeric (38, 0) -- Input number with as many as 18 digits

) RETURNS VARCHAR(8000) 
/*
* Converts a integer number as large as 34 digits into the 
* equivalent words.  The first letter is capitalized.
*
* Attribution: Based on NumberToWords by Srinivas Sampath
*        as revised by Nick Barclay
*
* Example:
select dbo.udf_Num_ToWords (1234567890) + CHAR(10)
      +  dbo.udf_Num_ToWords (0) + CHAR(10)
      +  dbo.udf_Num_ToWords (123) + CHAR(10)
select dbo.udf_Num_ToWords(76543210987654321098765432109876543210)
 
DECLARE @i numeric (38,0)
SET @i = 0
WHILE @I <= 1000 BEGIN 
    PRINT convert (char(5), @i)  
            + convert(varchar(255), dbo.udf_Num_ToWords(@i)) 
    SET @I  = @i + 1 
END
*
* Published as the T-SQL UDF of the Week Vol 2 #9 2/17/03
****************************************************************/
AS BEGIN

DECLARE @inputNumber VARCHAR(38)
DECLARE @NumbersTable TABLE (number CHAR(2), word VARCHAR(10))
DECLARE @outputString VARCHAR(8000)
DECLARE @length INT
DECLARE @counter INT
DECLARE @loops INT
DECLARE @position INT
DECLARE @chunk CHAR(3) -- for chunks of 3 numbers
DECLARE @tensones CHAR(2)
DECLARE @hundreds CHAR(1)
DECLARE @tens CHAR(1)
DECLARE @ones CHAR(1)

IF @Number = 0 Return 'Zero'

-- initialize the variables
SELECT @inputNumber = CONVERT(varchar(38), @Number)
     , @outputString = ''
     , @counter = 1
SELECT @length   = LEN(@inputNumber)
     , @position = LEN(@inputNumber) - 2
     , @loops    = LEN(@inputNumber)/3

-- make sure there is an extra loop added for the remaining numbers
IF LEN(@inputNumber) % 3 <> 0 SET @loops = @loops + 1

-- insert data for the numbers and words
INSERT INTO @NumbersTable   SELECT '00', ''
    UNION ALL SELECT '01', 'one'      UNION ALL SELECT '02', 'two'
    UNION ALL SELECT '03', 'three'    UNION ALL SELECT '04', 'four'
    UNION ALL SELECT '05', 'five'     UNION ALL SELECT '06', 'six'
    UNION ALL SELECT '07', 'seven'    UNION ALL SELECT '08', 'eight'
    UNION ALL SELECT '09', 'nine'     UNION ALL SELECT '10', 'ten'
    UNION ALL SELECT '11', 'eleven'   UNION ALL SELECT '12', 'twelve'
    UNION ALL SELECT '13', 'thirteen' UNION ALL SELECT '14', 'fourteen'
    UNION ALL SELECT '15', 'fifteen'  UNION ALL SELECT '16', 'sixteen'
    UNION ALL SELECT '17', 'seventeen' UNION ALL SELECT '18', 'eighteen'
    UNION ALL SELECT '19', 'nineteen' UNION ALL SELECT '20', 'twenty'
    UNION ALL SELECT '30', 'thirty'   UNION ALL SELECT '40', 'forty'
    UNION ALL SELECT '50', 'fifty'    UNION ALL SELECT '60', 'sixty'
    UNION ALL SELECT '70', 'seventy'  UNION ALL SELECT '80', 'eighty'
    UNION ALL SELECT '90', 'ninety'   

WHILE @counter <= @loops BEGIN

	-- get chunks of 3 numbers at a time, padded with leading zeros
	SET @chunk = RIGHT('000' + SUBSTRING(@inputNumber, @position, 3), 3)

	IF @chunk <> '000' BEGIN
		SELECT @tensones = SUBSTRING(@chunk, 2, 2)
		     , @hundreds = SUBSTRING(@chunk, 1, 1)
		     , @tens = SUBSTRING(@chunk, 2, 1)
		     , @ones = SUBSTRING(@chunk, 3, 1)

		-- If twenty or less, use the word directly from @NumbersTable
		IF CONVERT(INT, @tensones) <= 20 OR @Ones='0' BEGIN
			SET @outputString = (SELECT word 
                                      FROM @NumbersTable 
                                      WHERE @tensones = number)
                   + CASE @counter WHEN 1 THEN '' -- No name
                       WHEN 2 THEN ' thousand ' WHEN 3 THEN ' million '
                       WHEN 4 THEN ' billion '  WHEN 5 THEN ' trillion '
                       WHEN 6 THEN ' quadrillion ' WHEN 7 THEN ' quintillion '
                       WHEN 8 THEN ' sextillion '  WHEN 9 THEN ' septillion '
                       WHEN 10 THEN ' octillion '  WHEN 11 THEN ' nonillion '
                       WHEN 12 THEN ' decillion '  WHEN 13 THEN ' undecillion '
                       ELSE '' END
                               + @outputString
		    END
		 ELSE BEGIN -- break down the ones and the tens separately

             SET @outputString = ' ' 
                            + (SELECT word 
                                    FROM @NumbersTable 
                                    WHERE @tens + '0' = number)
					         + '-'
                             + (SELECT word 
                                    FROM @NumbersTable 
                                    WHERE '0'+ @ones = number)
                   + CASE @counter WHEN 1 THEN '' -- No name
                       WHEN 2 THEN ' thousand ' WHEN 3 THEN ' million '
                       WHEN 4 THEN ' billion '  WHEN 5 THEN ' trillion '
                       WHEN 6 THEN ' quadrillion ' WHEN 7 THEN ' quintillion '
                       WHEN 8 THEN ' sextillion '  WHEN 9 THEN ' septillion '
                       WHEN 10 THEN ' octillion '  WHEN 11 THEN ' nonillion '
                       WHEN 12 THEN ' decillion '   WHEN 13 THEN ' undecillion '
                       ELSE '' END
                            + @outputString
		END

		-- now get the hundreds
		IF @hundreds <> '0' BEGIN
			SET @outputString  = (SELECT word 
                                      FROM @NumbersTable 
                                      WHERE '0' + @hundreds = number)
					            + ' hundred ' 
                                + @outputString
		END
	END

	SELECT @counter = @counter + 1
	     , @position = @position - 3

END

-- Remove any double spaces
SET @outputString = LTRIM(RTRIM(REPLACE(@outputString, '  ', ' ')))
SET @outputstring = UPPER(LEFT(@outputstring, 1)) + SUBSTRING(@outputstring, 2, 8000)


RETURN @outputString -- return the result
END

GO
/****** Object:  UserDefinedFunction [dbo].[fnProperCase]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fnProperCase](@Text as varchar(8000))
returns varchar(8000)
as
begin
   declare @Reset bit;
   declare @Ret varchar(8000);
   declare @i int;
   declare @c char(1);
   DECLARE @opr NVARCHAR(1) = 'S';

   IF (UPPER(@opr) = 'S')
	BEGIN
		IF (
				(
					SELECT sp_read
					FROM comn.comn_master_sp
					WHERE sp_name = 'fnProperCase'
					) = 'Y'
				)
		BEGIN
	
   select @Reset = 1, @i=1, @Ret = '';
   
   while (@i <= len(@Text))
   	select @c= substring(@Text,@i,1),
               @Ret = @Ret + case when @Reset=1 then UPPER(@c) else LOWER(@c) end,
--Check with this line @Reset = case when @c<>' ' then 0 else 1 end,
	           @Reset = case when @c like '[a-zA-Z]' then 0 else 1 end,
               @i = @i +1
   return @Ret
   END
	ELSE
	BEGIN
	
		EXECUTE NotifyError 'I'
			,'FN'
			,'[dbo].[fnProperCase]'
			,''
			,'Tbl comn_master_sp does not contain record for sp [fnProperCase] or not activated it'
			,''
			,''
			,'Fn'
	END
	End
	RETURN @Ret
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnSplit]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnSplit] (
	@sInputList NVARCHAR(4000) -- List of delimited items
	,@sDelimiter NVARCHAR(4000) = ',' -- delimiter that separates items
	)
RETURNS @List TABLE (item NVARCHAR(4000))

BEGIN
	DECLARE @opr NVARCHAR(1) = 'S';

	IF (UPPER(@opr) = 'S')
	BEGIN
		IF (
				(
					SELECT sp_read
					FROM comn.comn_master_sp
					WHERE sp_name = 'fnSplit'
					) = 'Y'
				)
		BEGIN
			DECLARE @sItem NVARCHAR(4000)

			WHILE CHARINDEX(@sDelimiter, @sInputList, 0) <> 0
			BEGIN
				SELECT @sItem = RTRIM(LTRIM(SUBSTRING(@sInputList, 1, CHARINDEX(@sDelimiter, @sInputList, 0) - 1)))
					,@sInputList = RTRIM(LTRIM(SUBSTRING(@sInputList, CHARINDEX(@sDelimiter, @sInputList, 0) + LEN(@sDelimiter), LEN(@sInputList))))

				IF LEN(@sItem) > 0
					INSERT INTO @List
					SELECT @sItem
			END

			IF LEN(@sInputList) > 0
				INSERT INTO @List
				SELECT @sInputList -- Put the last item in
		END
	END
	ELSE
	BEGIN
	
		EXECUTE NotifyError 'I'
			,'FN'
			,'[dbo].[fnSplit]'
			,''
			,'Tbl comn_master_sp does not contain record for sp [fnSplit] or not activated it'
			,''
			,''
			,'Fn'
	END

	RETURN
END




GO
/****** Object:  UserDefinedFunction [dbo].[fnTimeDiff]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnTimeDiff]
(
	-- Add the parameters for the function here
	@Time1 time,
	@Time2 time
)
RETURNS time
AS
BEGIN
		DECLARE @hh int
		DECLARE @mm int
		DECLARE @ss int
		DECLARE @rTime time
		SET @ss=abs(DATEDIFF(SECOND, CONVERT(DATETIME,@Time1),CONVERT(DATETIME,@Time2)))
		
		SET @mm=(@ss/60)
		SET @ss=@ss-(@mm*60)
		SET @hh=(@mm/60)
		SET @mm=@mm-(@hh*60)
		SET @rTime=(CONVERT(time,str(@hh)+':'+str(@mm)+':'+str(@ss),114))
	
		RETURN @rTime
END



GO
/****** Object:  UserDefinedFunction [dbo].[func_seq_lib]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION  [dbo].[func_seq_lib]()
RETURNS @List TABLE (item NVARCHAR(4000))
AS
BEGIN
	DECLARE @a nvarchar(10)
	SET @a=(SELECT  CONCAT(comn_character_sequence,RIGHT('00000' + CONVERT(VARCHAR(5),comn_number_sequence),5)) FROM comn.comn_sequence where comn_appl_code='sim126' and  comn_cur_code='01' and comn_sequence_code='LM')
	exec dbo.temp_seq_lib
	--SELECT @a as a
	RETURN;
END


GO
/****** Object:  UserDefinedFunction [dbo].[GetCountForComment]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM dbo.GetCountForComment('1018','2014-09-09')
CREATE FUNCTION [dbo].[GetCountForComment] 
(	
	-- Add the parameters for the function here
	@enroll nvarchar(7),
	@date nvarchar(12)
)
RETURNS @cnt1 TABLE (item int)
AS
BEGIN
	-- Add the SELECT statement with parameter references here
	DECLARE  @cnt int
	SET @cnt=(SELECT COUNT(sims_attendance_day_comment) FROM sims.sims_student_attendance WHERE sims_enrollment_number=@enroll AND sims_attendance_date=@date)
	INSERT into @cnt1
		SELECT @cnt
	
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetMonthName]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMonthName]
(
	-- Add the parameters for the function here
	@MonthNumber nvarchar(2)
)
RETURNS nvarchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @monthName nvarchar(20)

	IF(@MonthNumber='1' OR @MonthNumber='01')
		SET @monthName='January'
	ELSE IF(@MonthNumber='2' OR @MonthNumber='02')
		SET @monthName='February'
	ELSE IF(@MonthNumber='3' OR @MonthNumber='03')
		SET @monthName='March'
	ELSE IF(@MonthNumber='4' OR @MonthNumber='04')
		SET @monthName='April'
	ELSE IF(@MonthNumber='5' OR @MonthNumber='05')
		SET @monthName='May'
	ELSE IF(@MonthNumber='6' OR @MonthNumber='06')
	SET @monthName='June'
	ELSE IF(@MonthNumber='7' OR @MonthNumber='07')
	SET @monthName='July'
	ELSE IF(@MonthNumber='8' OR @MonthNumber='08')
	SET @monthName='August'
	ELSE IF(@MonthNumber='9' OR @MonthNumber='09')
	SET @monthName='September'
	ELSE IF(@MonthNumber='10')
	SET @monthName='October'
	ELSE IF(@MonthNumber='11')
	SET @monthName='November'
	ELSE IF(@MonthNumber='12')
	SET @monthName='December'

	RETURN @monthName

END

GO
/****** Object:  UserDefinedFunction [dbo].[GetMonthNameReceipt]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMonthNameReceipt]
(
	-- Add the parameters for the function here
	@MonthNumber nvarchar(2)
)
RETURNS nvarchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @monthName nvarchar(20)

	IF(@MonthNumber='1' OR @MonthNumber='01')
		SET @monthName='Installment - 5'
	ELSE IF(@MonthNumber='2' OR @MonthNumber='02')
		SET @monthName='Installment - 6'
	ELSE IF(@MonthNumber='3' OR @MonthNumber='03')
		SET @monthName='Installment - 7'
	ELSE IF(@MonthNumber='4' OR @MonthNumber='04')
		SET @monthName='Installment - 8'
	ELSE IF(@MonthNumber='5' OR @MonthNumber='05')
		SET @monthName='Installment - 9'
	ELSE IF(@MonthNumber='6' OR @MonthNumber='06')
	SET @monthName='Installment - 10'
	ELSE IF(@MonthNumber='7' OR @MonthNumber='07')
	SET @monthName='Installment - 11'
	ELSE IF(@MonthNumber='8' OR @MonthNumber='08')
	SET @monthName='Installment - 12'
	ELSE IF(@MonthNumber='9' OR @MonthNumber='09')
	SET @monthName='Installment - 1'
	ELSE IF(@MonthNumber='10')
	SET @monthName='Installment - 2'
	ELSE IF(@MonthNumber='11')
	SET @monthName='Installment - 3'
	ELSE IF(@MonthNumber='12')
	SET @monthName='Installment - 4'

	RETURN @monthName

END




GO
/****** Object:  UserDefinedFunction [dbo].[PROPER_CASE]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[PROPER_CASE]
(
      @String VARCHAR(MAX)  -- Variable for string
)
RETURNS varchar(MAX)
BEGIN
     Declare @Xml XML
     Declare @ProperCase Varchar(Max)
     Declare @delimiter Varchar(5)
     Set @delimiter=' '
     SET @Xml = cast(('<A>'+replace(@String,@delimiter,'</A><A>')+'</A>') AS XML)
 
     ;With CTE AS (SELECT A.value('.', 'varchar(max)') as [Column]
      FROM @Xml.nodes('A') AS FN(A) )
      Select @ProperCase =Stuff((Select ' ' + UPPER(LEFT([Column],1))
      + LOWER(SUBSTRING([Column], 2 ,LEN([Column]))) from CTE
      for xml path('') ),1,1,'')
 
RETURN (@ProperCase)
END

--print dbo.PROPER_CASE('ABCD')
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Split](@String varchar(8000), @Delimiter char(1))
returns @temptable
TABLE (nameIndex int identity(1,1),items varchar(8000))
as
begin
	declare @idx int
	declare
	@slice varchar(8000)

	select @idx = 1
	if len(@String)<1 or @String
	is null return

	while @idx!= 0
	begin
	set @idx =
	charindex(@Delimiter,@String)
	if @idx!=0
	set @slice = left(@String,@idx
	- 1)
	else
	set @slice = @String

	if(len(@slice)>0)
	insert
	into @temptable(Items) values(@slice)

	set @String =
	right(@String,len(@String) - @idx)
	if len(@String) = 0 break
	end

return
end
GO
/****** Object:  UserDefinedFunction [dbo].[StripHTML]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[StripHTML] (
@HTMLText nvarchar(MAX)
)
RETURNS nvarchar(MAX)
AS
BEGIN
DECLARE @Start  int
DECLARE @End    int
DECLARE @Length int

set @HTMLText = replace(@htmlText, '<br>',CHAR(13) + CHAR(10))
set @HTMLText = replace(@htmlText, '<br/>',CHAR(13) + CHAR(10))
set @HTMLText = replace(@htmlText, '<br />',CHAR(13) + CHAR(10))
set @HTMLText = replace(@htmlText, '<li>','- ')
set @HTMLText = replace(@htmlText, '</li>',CHAR(13) + CHAR(10))

set @HTMLText = replace(@htmlText, '&rsquo;' collate Latin1_General_CS_AS, ''''  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&quot;' collate Latin1_General_CS_AS, '"'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&amp;' collate Latin1_General_CS_AS, '&'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&euro;' collate Latin1_General_CS_AS, '€'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&lt;' collate Latin1_General_CS_AS, '<'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&gt;' collate Latin1_General_CS_AS, '>'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&oelig;' collate Latin1_General_CS_AS, 'oe'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&nbsp;' collate Latin1_General_CS_AS, ' '  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&copy;' collate Latin1_General_CS_AS, '©'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&laquo;' collate Latin1_General_CS_AS, '«'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&reg;' collate Latin1_General_CS_AS, '®'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&plusmn;' collate Latin1_General_CS_AS, '±'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&sup2;' collate Latin1_General_CS_AS, '²'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&sup3;' collate Latin1_General_CS_AS, '³'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&micro;' collate Latin1_General_CS_AS, 'µ'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&middot;' collate Latin1_General_CS_AS, '·'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ordm;' collate Latin1_General_CS_AS, 'º'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&raquo;' collate Latin1_General_CS_AS, '»'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&frac14;' collate Latin1_General_CS_AS, '¼'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&frac12;' collate Latin1_General_CS_AS, '½'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&frac34;' collate Latin1_General_CS_AS, '¾'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&Aelig' collate Latin1_General_CS_AS, 'Æ'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&Ccedil;' collate Latin1_General_CS_AS, 'Ç'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&Egrave;' collate Latin1_General_CS_AS, 'È'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&Eacute;' collate Latin1_General_CS_AS, 'É'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&Ecirc;' collate Latin1_General_CS_AS, 'Ê'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&Ouml;' collate Latin1_General_CS_AS, 'Ö'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&agrave;' collate Latin1_General_CS_AS, 'à'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&acirc;' collate Latin1_General_CS_AS, 'â'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&auml;' collate Latin1_General_CS_AS, 'ä'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&aelig;' collate Latin1_General_CS_AS, 'æ'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ccedil;' collate Latin1_General_CS_AS, 'ç'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&egrave;' collate Latin1_General_CS_AS, 'è'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&eacute;' collate Latin1_General_CS_AS, 'é'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ecirc;' collate Latin1_General_CS_AS, 'ê'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&euml;' collate Latin1_General_CS_AS, 'ë'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&icirc;' collate Latin1_General_CS_AS, 'î'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ocirc;' collate Latin1_General_CS_AS, 'ô'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ouml;' collate Latin1_General_CS_AS, 'ö'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&divide;' collate Latin1_General_CS_AS, '÷'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&oslash;' collate Latin1_General_CS_AS, 'ø'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ugrave;' collate Latin1_General_CS_AS, 'ù'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&uacute;' collate Latin1_General_CS_AS, 'ú'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&ucirc;' collate Latin1_General_CS_AS, 'û'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&uuml;' collate Latin1_General_CS_AS, 'ü'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&quot;' collate Latin1_General_CS_AS, '"'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&amp;' collate Latin1_General_CS_AS, '&'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&lsaquo;' collate Latin1_General_CS_AS, '<'  collate Latin1_General_CS_AS)
set @HTMLText = replace(@htmlText, '&rsaquo;' collate Latin1_General_CS_AS, '>'  collate Latin1_General_CS_AS)


-- Remove anything between <STYLE> tags
SET @Start = CHARINDEX('<STYLE', @HTMLText)
SET @End = CHARINDEX('</STYLE>', @HTMLText, CHARINDEX('<', @HTMLText)) + 7
SET @Length = (@End - @Start) + 1

WHILE (@Start > 0 AND @End > 0 AND @Length > 0) BEGIN
SET @HTMLText = STUFF(@HTMLText, @Start, @Length, '')
SET @Start = CHARINDEX('<STYLE', @HTMLText)
SET @End = CHARINDEX('</STYLE>', @HTMLText, CHARINDEX('</STYLE>', @HTMLText)) + 7
SET @Length = (@End - @Start) + 1
END

-- Remove anything between <whatever> tags
SET @Start = CHARINDEX('<', @HTMLText)
SET @End = CHARINDEX('>', @HTMLText, CHARINDEX('<', @HTMLText))
SET @Length = (@End - @Start) + 1

WHILE (@Start > 0 AND @End > 0 AND @Length > 0) BEGIN
SET @HTMLText = STUFF(@HTMLText, @Start, @Length, '')
SET @Start = CHARINDEX('<', @HTMLText)
SET @End = CHARINDEX('>', @HTMLText, CHARINDEX('<', @HTMLText))
SET @Length = (@End - @Start) + 1
END

RETURN LTRIM(RTRIM(@HTMLText))

END

GO
/****** Object:  UserDefinedFunction [dbo].[udf_GetNumeric]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_GetNumeric]
(@strAlphaNumeric VARCHAR(max))
RETURNS VARCHAR(max)
AS
BEGIN
DECLARE @intAlpha INT
SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric)
BEGIN
WHILE @intAlpha > 0
BEGIN
SET @strAlphaNumeric = STUFF(@strAlphaNumeric, @intAlpha, 1, '' )
SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric )
END
END
RETURN ISNULL(@strAlphaNumeric,0)
END

GO
/****** Object:  UserDefinedFunction [dbo].[udfTrim]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udfTrim] 
(
	@StringToClean as nvarchar(4000)
)
RETURNS nvarchar(4000)
AS
BEGIN	
	--Replace all non printing whitespace characers with Characer 32 whitespace
	--NULL
	Set @StringToClean = Replace(@StringToClean,CHAR(0),CHAR(32));
	--Horizontal Tab
	Set @StringToClean = Replace(@StringToClean,CHAR(9),CHAR(32));
	--Line Feed
	Set @StringToClean = Replace(@StringToClean,CHAR(10),CHAR(32));
	--Vertical Tab
	Set @StringToClean = Replace(@StringToClean,CHAR(11),CHAR(32));
	--Form Feed
	Set @StringToClean = Replace(@StringToClean,CHAR(12),CHAR(32));
	--Carriage Return
	Set @StringToClean = Replace(@StringToClean,CHAR(13),CHAR(32));
	--Column Break
	Set @StringToClean = Replace(@StringToClean,CHAR(14),CHAR(32));
	--Non-breaking space
	Set @StringToClean = Replace(@StringToClean,CHAR(160),CHAR(32));
 
	Set @StringToClean = LTRIM(RTRIM(@StringToClean));
	Return @StringToClean
END

GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GeneratePassword]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_GeneratePassword] ( @PasswordLength INT )
RETURNS VARCHAR(20)
AS
BEGIN
		DECLARE @Password     VARCHAR(20)
		DECLARE @ValidCharacters   VARCHAR(100)
		DECLARE @PasswordIndex    INT
		DECLARE @CharacterLocation   INT

		SET @ValidCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890@$&%'
		SET @PasswordIndex = 1
		SET @Password = ''
		WHILE @PasswordIndex <= @PasswordLength
		BEGIN
		SELECT @CharacterLocation = ABS(CAST(CAST([NewID] AS VARBINARY) AS INT)) % 
		LEN(@ValidCharacters) + 1
		FROM [dbo].[RandomNewID]
		SET @Password = @Password + SUBSTRING(@ValidCharacters, @CharacterLocation, 1)
		SET @PasswordIndex = @PasswordIndex + 1
		END
		RETURN @Password
END

--SELECT [dbo].[ufn_GeneratePassword] ( 8 ) AS [Password8], [dbo].[ufn_GeneratePassword] ( 10 ) AS [Password10],    [dbo].[ufn_GeneratePassword] ( 12 ) AS [Password12]
GO
/****** Object:  UserDefinedFunction [dbo].[usp_ClearHTMLTags]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    /**************************************************************************** 
    Name of Author  :   Vishal Jharwade 
    Purpose         :   The Purpose of this function is to clean the html tags from the data. 
    ***************************************************************************************/ 
    CREATE FUNCTION [dbo].[usp_ClearHTMLTags] 
    (@String NVARCHAR(MAX)) 
     
    RETURNS NVARCHAR(MAX) 
    AS 
    BEGIN 
        DECLARE @Start INT, 
                @End INT, 
                @Length INT 
         
        WHILE CHARINDEX('<', @String) > 0 AND CHARINDEX('>', @String, CHARINDEX('<', @String)) > 0 
        BEGIN 
            SELECT  @Start  = CHARINDEX('<', @String),  
                    @End    = CHARINDEX('>', @String, CHARINDEX('<', @String)) 
            SELECT @Length = (@End - @Start) + 1 
             
            IF @Length > 0 
            BEGIN 
                SELECT @String = STUFF(@String, @Start, @Length, '') 
             END 
         END 
         
        RETURN @String 
    END 

GO
/****** Object:  UserDefinedFunction [dbo].[RemoveSpecialChars]    Script Date: 07-08-2019 13:55:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[RemoveSpecialChars] (@s varchar(256)) returns varchar(256)
   with schemabinding
begin
   if @s is null
      return null
   declare @s2 varchar(256)
   set @s2 = ''
   declare @l int
   set @l = len(@s)
   declare @p int
   set @p = 1
   while @p <= @l begin
      declare @c int
      set @c = ascii(substring(@s, @p, 1))
      if @c between 48 and 57 or @c between 65 and 90 or @c between 97 and 122
         set @s2 = @s2 + char(@c)
      set @p = @p + 1
      end
   if len(@s2) = 0
      return null
   return @s2
   end
GO
